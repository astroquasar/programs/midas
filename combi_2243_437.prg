!
! MIDAS procedure  COMBI_2243_437		                03.11.00 
!								(C)SVCLM
!
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter output table:"
!
!                    Scale by median:
!
copy/tab {P1} {P3}
comp/tab {P3} :flux1 = :flux 
comp/tab {P3} :sigma1 = :sigma 
!
copy/tt {P2} :flux {P3} :flux2
copy/tt {P2} :sigma {P3} :sigma2
!

!
!                         Coadd flux and sky:
!
comp/tab {P3} :sigma = sqrt(1/(1/:sigma1**2+1/:sigma2**2))
comp/tab {P3} :flux = (:flux1/:sigma1**2+:flux2/:sigma2**2)*:sigma**2


