!
! Procedure MERGE.PRG                                (c) SVCLM
!                                                     Oct 1995
! merge all orders weighting with inverse variances 
! (tables contain columns :wave :flux :sigma :sky)
!
DEFINE/PAR P1 ?         TABLE         "Enter root name:"
DEFINE/PAR P2 ?         TABLE         "Enter order1,order2:"
DEFINE/PAR P3 ?         TABLE         "Enter output table:"
DEFINE/PAR P4 ?         TABLE         "Enter delta1,delta2 (AA):"

define/local ORDER/i/1/2 {P2}
define/local N/i/1/1 1

set/format I4

copy/tab 437_{ORDER(1)} table1


DO N = {ORDER(1)}+1 {ORDER(2)}
!
write/out Now working on order 'N'
!write/out Now working on order r'N'
@@ /crater/prg_linux/echellemerg table1 437_{N} testout {P4}
copy/tab testout table1
!
ENDDO
copy/tab testout {P3}