!
! MIDAS Procedure ECHELLEFITNEW (SANTIAGO)                      (C) SVCLM
!      for magE                                        2010
! Fits polynoms to fitted parameters of optimal extraction
! of Echelle orders (echelleopt.prg)

! Method: parameters POS1 and SIGMA in each order are fitted with
! third order polynoms. 

DEFINE/PARAMETER P1 ?      C   "Enter input table:"
DEFINE/PARAMETER P2 ?      C   "Enter output table:"
DEFINE/PARAMETER P3 ?      C   "Enter poly degree:"
DEFINE/LOCAL N/I/1/1 0                ! Loop variable for orders
DEFINE/LOCAL I/I/1/1 0                ! Loop variable for regression
DEFINE/LOCAL IMAX/I/1/1 6            ! max Zahl der Iterationen
DEFINE/LOCAL KAPPA/I/1/1 3
DEFINE/LOCAL SIGMA/R/1/1 0
DEFINE/LOCAL SELROW/I/1/1 0
DEFINE/LOCAL sigstart/R/1/1 2.2
DEFINE/LOCAL posstart/R/1/1 1.1
DEFINE/LOCAL XLIM/R/1/2 500.,1800.  ! x-region to consider in fits


copy/tab {P1} test1

@@ /musica2/prg_linux/median test1 testa :x :pos1 5
copy/tt testa :median test1 :pos1
@@ /musica2/prg_linux/median test1 testa :x :delta 5
copy/tt testa :median test1 :delta
@@ /musica2/prg_linux/median test1 testa :x :sigma 5
copy/tt testa :median test1 :sigma

SELECT/TAB test1 :order .EQ. 1
COPY/TAB test1 {P2}
DELETE/ROW {P2} 1..
SELECT/TAB test1 ALL

DO N = 1 {order.tbl,NBORDI}


!selektion der orders
WRITE/OUT "Order" {N}
SELECT/TAB test1 (sequence .LE. {N}*{test1.tbl,NPIXX}).AND.(sequence .GT. ({N}-1)*{test1.tbl,NPIXX})
!Fit der Position, Schleife
COPY/TAB test1 testa
COPY/TAB test1 testb
SELECT/TABLE testa :pos1 .ne. 'posstart' 
select/tab testa SELECT .AND. ((:x .GE. 'XLIM(1)') .AND. (:x .LE. 'XLIM(2)'))

stat/tab testa :pos1
COMPUTE/KEY sigma = M$ABS(outputr(4))
@@ /musica2/prg_linux/median testa aux :x :pos1 1
!                                 skip outliers:
COMPUTE/KEY sigma = kappa * sigma
READ/KEY  sigma
SELECT/TAB testa SELECT .AND. ABS(:pos1 - {aux.tbl,MEDIAN}) .LE. 'sigma'

DO I = 1 IMAX
COMPUTE/KEY selrow = outputi(1)

REGRESSION/POLYNOMIAL testa  :pos1 :x {P3} 
SAVE/REGRESSION testa COEF
COMPUTE/REGRESSION testa :fit = COEF
COMPUTE/TABLE testa :diff = :pos1 - :fit
SELECT/TABLE testa :pos1 .ne. 'posstart'

stat/tab testa :pos1
COMPUTE/KEY sigma = M$ABS(outputr(4))
@@ /musica2/prg_linux/median testa aux :x :pos1 1
!                                 skip outliers:
COMPUTE/KEY sigma = kappa * sigma
READ/KEY  sigma

SELECT/TAB testa :pos1 .ne. 'posstart' .AND. ABS(:diff) .LE. 'sigma'
select/tab testa SELECT .AND. ((:x .GE. 'XLIM(1)') .AND. (:x .LE. 'XLIM(2)'))
IF OUTPUTI(1) .EQ. selrow .OR. I .EQ. IMAX THEN
REGRESSION/POLYNOMIAL testa :pos1 :x {P3}
SAVE/REGRESSION testa COEF
SELECT/TABLE testa all
COMPUTE/REGRESSION testa :fit = COEF
COPY/TT testa :fit testb :pos1

ENDIF
ENDDO

!!! delta median per order:
@@ /musica2/prg_linux/median testa aux :x :delta 1
comp/tab testa :delta = {aux.tbl,median}
COPY/TT testa :delta testb :delta
!!!

!!! sigma median per order:
@@ /musica2/prg_linux/median testa aux :x :sigma 1
comp/tab testa :sigma = {aux.tbl,median}
COPY/TT testa :sigma testb :sigma
!!!

WRITE/OUT Fits for pos1, sigma and delta done




!
MERGE/TAB {P2} testb testout
COPY/TAB testout {P2}
SELECT/TABLE {P2} all
!!GOTO nextorder

ENDDO
!nextorder:

!set/graph stype=1
!plot/tab {P1} :x :sigma
!plot/tab {P1} :x :pos1
!set/graph stype=0
!overplot/tab {P2} :x :sigma
!overplot/tab {P2} :x :pos1

!ENDDO

SELECT/TABLE {P2} all
SELECT/TABLE test1 all





