!
! MIDAS procedure  FIT_CONT1		                09.02.00 
!								(C)SVCLM
! Steps to fit continuum 
!
WRITE/OUT Procedure FIT_CONT
DEFINE/PARAMETER P1 ?      C   "Enter order:"
DEFINE/PARAMETER P2 ?      C   "Enter limits:"
!
define/local N/i/1/1 {P1}
define/local x/r/1/2 {P2}
define/local ROOT/c/1/15 nothing
define/local ff/r/1/1 10000.
define/local ratio/r/1/1 0.001
set/format I4
!
!goto there

set/graph xaxis={x(1)},{x(2)}
plot/tab r{N} :wave :fluxA
set/graph color=2
overplot/tab r{N} :wave :fitA
set/graph color=4 stype=5
overplot/tab tableA_r{N} :x_axis :y_axis
set/graph color=3
overplot/ident tableA_r{N} :x_axis :seq
set/graph color=1 stype=0 xaxis=
