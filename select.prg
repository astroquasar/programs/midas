!
! MIDAS procedure  SELECT		 	                30.07.97 
!								(C)SVCLM
!
! Select in table P1 intervals given in table P2 (cursor)
! Compute 1. and 2. moments of distribution
!
WRITE/OUT Procedure select
DEFINE/PARAMETER P1 ?      C   "Enter input table:"
DEFINE/PARAMETER P2 ?      C   "Enter table with data points:"
DEFINE/PARAMETER P3 ?      C   "Enter ouput table:"
!
define/local I/i/1/1 1                       !Loop for points in {P2}
define/local ISUM/i/1/1 1                       !Loop for points in {P2}

define/local N/i/1/1 {{P2}.tbl,TBLCONTR(4)}  !Number of points in {P2}
comp/key N = 'N' - 1


!select/tab test .NOT.select
select/tab {P1} (:wave .GE.{{P2}.tbl,:X_AXIS,@1} .AND. :wave .LT. {{P2}.tbl,:X_AXIS,@2})

DO I = 3 N 2                                  !select intervals:
COMP/KEY ISUM = {I}+1
select/tab {P1} select .OR. (:wave .GE.{{P2}.tbl,:X_AXIS,@{I}} .AND. :wave .LT. {{P2}.tbl,:X_AXIS,@{ISUM}})
ENDDO
copy/tab {P1} {P3}
select/tab {P1} all
