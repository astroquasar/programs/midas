!
!
!  Procedure echelleopt.prg                   (c) SVCLM May 1995
!
!  Optimal extraction of MIKE echelle orders
!-------------------------------------------------------
!
DEFINE/PAR P1 ? IMAGE    "Enter input image with object:"
DEFINE/PAR P2 ? TABLE    "Enter table with tilt angles:"
!
select/tab {P2} ALL
!
!set/echelle slit=20 
!
WRITE/KEY IN_A/C/1/60     {P1}
WRITE/KEY IN_B/C/1/60     {ORDTAB}
WRITE/KEY IN_PAR/C/1/60   {P2}
! WRITE/KEY OUT_PAR/C/1/60  dummy.tbl
WRITE/KEY OUT_A/C/1/60    ext
WRITE/KEY OUT_B/C/1/60    profile1    !!! this will be the rectified 2-d spec
WRITE/KEY INPUTC/C/1/60   COEFF
WRITE/KEY INPUTR/R/1/3    {SLIT},0.,{OFFSET}
WRITE/KEY INPUTI/I/1/2    1,{NBORDI}
WRITE/KEY display/I/1/3      1,13,1000 !!! compute from order n to m
WRITE/KEY tabtest/C/1/60      example.dat  !!! file with example pixel
!
!
!To create table with positions of test slit at x=display(2), order=display(1):
!create/tab example 2 35 example.dat
!load/table example :lab001 :lab002
!
!----------Organization of fit parameters:------------!
!                                                     !
! METHOD DOUBLE:                 METHOD SINGLE:       !
! -------------                  -------------        !
!  LISTA(6) = 3    !common sigma = A(3)               !
!  LISTA(5) = 2    !pos1         = A(2)               !
!  LISTA(1) = 1    !int1         = A(1)               !
!  LISTA(4) = 5    !pos2             --               !
!  LISTA(2) = 4    !int2             --               !
!  LISTA(3) = 6    !sky0         = A(4)               !
!                                                     !
!-----------------------------------------------------!
!
! 
!
!  WRITE/KEY LISTA/I/1/6    1,6,3,2,5,4  !int1,sky0,sigma,pos1,pos2,int2
!
RUN /home/mmaureir/Escritorio/reduccion/procs/echelle_mike  !for HE 2149
!RUN /musica2/LCOmar04/red_laura/CTQ305/echelle_mike  !for HE 2149

!
!MFIT(1): First MFIT(1) parameters will be actually fitted
!MFIT(2): Max number of allowed "hot pixels" to perform a fit.







