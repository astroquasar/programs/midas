DEFINE/LOCAL N/I/1/2 1
SET/FORMAT I4

do N = 1 35

!blue
copy/tab CXOMP_3_{N} ref_tab
!
rebin/tt ref_tab :wave,:flux CXOMP_4_{N} :wave_hvac,:flux LIN 0.,1. PIX
rebin/tt ref_tab :wave,:sigma CXOMP_4_{N} :wave_hvac,:sigma LIN 0.,1. PIX
!!comp/tab ref_tab :wave = :wave + 1.  !corrije parametro 6 de rebin/tt
copy/tab ref_tab CXOMP_4_{N}
!
rebin/tt ref_tab :wave,:flux CXOMP_3_{N} :wave_hvac,:flux LIN 0.,1. PIX
rebin/tt ref_tab :wave,:sigma CXOMP_3_{N} :wave_hvac,:sigma LIN 0.,1. PIX
copy/tab ref_tab CXOMP_3_{N}
!

enddo
