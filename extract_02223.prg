!! note: offset=-2 and posparameter = 2 in maketable


DEFINE/PARAMETER P1 ?      C   "Enter ID (2 characters):"
DEFINE/PARAMETER P2 ?      C   "Enter exp. time:"

write/key ID/c/1/2       {P1}
write/key time/r/1/1       {P2}



set/echelle slit=18 offset=0 nbordi=35
@@ ../../../procs/echellevar  02223_b{ID}_rect                !!create variances
comp 02223_b{ID}_rect_var = var + 1.9*1.9                      
write/ima 02223_b{ID}_rect_var [@83,@520:@889,@522] 10E8 ALL 
                  !! This corrects 3 bad lines in blue CCD.
@@ ../../../procs/maketable 02223_b{ID}_rect -4 0  1,-10,3 y  !!create startopt
comp/tab startopt :int2=0.
@@ ../../../procs/echelleopt 02223_b{ID}_rect 02223_b{ID}_rect_var startopt fitopt d 4
@@ ../../../procs/echellefitnew  fitopt startopt1 2 300,1200 1,35
@@ ../../../procs/echelleopt 02223_b{ID}_rect 02223_b{ID}_rect_var startopt1 fitopt1 d 2
COMP/TABL fitopt1 :flux=:int1*:sigma*1.77/{time}
COMP/TABL fitopt1 :stdev=:error1*:sigma*1.77/{time}
copy/tab fitopt1 02223{ID}_fitopt1
