!
! MIDAS procedure  normalize		                25.03.06 
!								(C)SVCLM
! normalize orders
!
WRITE/OUT Procedure NORMALIZE
DEFINE/PARAMETER P1 ?      C   "Enter root name:"
DEFINE/PARAMETER P2 ?      C   "Enter order1,order2:"
!
define/local N/i/1/1 1
define/local ORDER/i/1/2 {P2}
set/format I4
!
DO N = {ORDER(1)} {ORDER(2)}
!
write/out Now working on order 'N'
comp/tab {P1}{N} :normflux = :flux/:fit
comp/tab {P1}{N} :stdev = :sigma/:fit
!
ENDDO
