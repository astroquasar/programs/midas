!
! MIDAS procedure  ECHELLEADD_4		                25.03.06 
!								(C)SVCLM
! same as ecehlleadd_3 but for 4 spectra instead of 3.
! Coadd orders. Flux
! is weighted with inverse of variances.
!
WRITE/OUT Procedure ECHELLEADD
DEFINE/PARAMETER P1 ?      C   "Enter root name 1:"
DEFINE/PARAMETER P2 ?      C   "Enter root name 2:"
DEFINE/PARAMETER P3 ?      C   "Enter root name 3:"
DEFINE/PARAMETER P4 ?      C   "Enter root name 4:"
DEFINE/PARAMETER P5 ?      C   "Enter output root name:"
DEFINE/PARAMETER P6 ?      C   "Enter order1,order2:"
!
define/local N/i/1/1 1
define/local ORDER/i/1/2 {P6}
set/format I4
!
DO N = {ORDER(1)} {ORDER(2)}
!
write/out Now working on order 'N'
@@ /Users/Vincent/Documents/Science/Research/fitlyman/procs/combi_4 {P1}{N} {P2}{N} {P3}{N} {P4}{N} {P5}{N}
!
ENDDO
