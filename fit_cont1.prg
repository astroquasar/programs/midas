!
! MIDAS procedure  FIT_CONT1		                09.02.00 
!								(C)SVCLM
! Steps to fit continuum 
!
WRITE/OUT Procedure FIT_CONT
DEFINE/PARAMETER P1 ?      C   "Enter order:"
!
define/local N/i/1/1 {P1}
define/local ROOT/c/1/15 437_
define/local ff/r/1/1 10000.
define/local ratio/r/1/1 0.001
set/format I4
!
!goto there

!plot/tab r{N} :wave :fluxA
!get/gcurs table_{ROOT(12:14)}_{N} 
@@ /crater/prg_linux/fitspline1 r{N} tableA_{N} ? ? 0,0
set/graph color=2
comp/tab {ROOT}{N} :fitA = :fit
overplot/tab {ROOT}{N} :wave :fitA
set/graph color=4 stype=5
overplot/tab tableA_{N} :x_axis :y_axis
comp/tab tableA_{N} :seq=seq
name/col tableA_{N} :seq ? F3.0
set/graph color=3
overplot/ident tableA_{N} :x_axis :seq
comp/tab {ROOT}{N} :normfluxA=:fluxA/:fitA
comp/tab {ROOT}{N} :stdevA=:sigmaA/:fitA
set/graph color=1 stype=0 xaxis=
