!
!PROCEDURE ECHELLECAL.  (C) SEBASTIAN LOPEZ APR.95
!Wavelength Calibration in EMMI Echelle Frames. Session is already init.
!(SAVE/ECHELLE Sesion name afterwards!)
!
!
define/parameter p1 ? I "Enter frame with Th-Ar spectrum:"
!
!
copy/tab order_1x2 order
set/echelle nbordi=33 slit=10 offset=1.5
set/echelle wlcmtd=restart wlc={p1} width2=2 thres2=2  tol=0.2 dc=4 seamtd=gravity
set/echelle WLCITER=1,0.3,20,5.0,4.0  WLCNITER = 5,10 LINCAT=thar.tbl

!
cal/echelle done
!
!
!LINES USED (EEV):  (frames divided by 3-5)
!
!3839.700  order 121
!!!!!3868.530  order 120
!3324.753  order 140
!!!!!!3119.526  order 149


!** Identification loop **
! Initial accuracy          : 1.0 pixel
! Next neighbour parameter  : 0.1
! Maximal error             : 1.0 pixel
! Initial error             : 2.0 pixel
! Iteration error           : 1.0 std dev.

!** Rejection loop **
! Tolerance                 : 0.5
