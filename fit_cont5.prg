!
! MIDAS procedure  FIT_CONT2		                29.07.01 
!								(C)SVCLM
! Compare A continuum with B.
!
!WRITE/OUT Procedure FIT_CONT
!DEFINE/PARAMETER P1 ?      C   "Enter order:"
!
!define/local N/i/1/1 {P1}
define/local ROOT/c/1/11 nothing
define/local wave1/r/1/1 1.
define/local wave2/r/1/1 1.
define/local wave3/r/1/1 1.
define/local wave4/r/1/1 1.
define/local x1/r/1/1 1.
define/local x2/r/1/1 1.
define/local y1/r/1/1 1.
define/local y2/r/1/1 1.
define/local m/r/1/1 1.
define/local n/r/1/1 1.
set/format I4
!

write/out "define 2 featureless regions"
get/gcurs aux
comp/key wave1 = {aux.tbl,:x_axis,@1} 
comp/key wave2 = {aux.tbl,:x_axis,@2} 
comp/key wave3 = {aux.tbl,:x_axis,@3} 
comp/key wave4 = {aux.tbl,:x_axis,@4} 

select/tab test :wave.ge.{wave1}.and.:wave.le.{wave2}
stat/tab test :normfluxB
comp/key y1 =  {OUTPUTR(3)}
comp/key x1 =  ({wave1} + {wave2})/2.

select/tab test :wave.ge.{wave3}.and.:wave.le.{wave4}
stat/tab test :normfluxB
comp/key y2 =  {OUTPUTR(3)}
comp/key x2 =  ({wave3} + {wave4})/2.

comp/key m =  ({y2} - {y1})/({x2}-{x1})
comp/key n =  {y1} - {m} * {x1}

write/out m = {m}  n =  {n}

comp/tab tabletest :y_axis = :y_axis * (:x_axis * {m} + {n})

select/tab test all
@@ /home/slopez/he1104/prg/fitspline1 test tabletest ? ? 0,0
set/graph color=4
overplot/tab test :wave :fit
set/graph color=1 