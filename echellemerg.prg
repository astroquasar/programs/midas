!
! Procedure ECHELLEMERG.PRG                          (c) SVCLM
!                                                     Oct 1995
! merge two orders weighting with inverse variances 
! (tables contain columns :wave :flux :sigma :sky)
!
DEFINE/PAR P1 ?         TABLE         "Enter table 1:"
DEFINE/PAR P2 ?         TABLE         "Enter table 2:"
DEFINE/PAR P3 ?         TABLE         "Enter output table:"
DEFINE/PAR P4 ?         TABLE         "Enter delta:"

!define/local tol/r/1/1 {{P1}.tbl,step}   !Tolerance for wavelength
define/local tol/r/1/1 0.07
define/local pix/r/1/1 0.04562375
define/local c1/c/1/10 :stdevA !:ns   !column names (sigma)
define/local c2/c/1/10 :normfluxA !:n    !flux
define/local i1/i/1/1 1000 !   !last pix in test1
define/local i2/i/1/1 1000 !   !first pix in test2
define/local wave1/r/1/1 1000. !   !last wave in test1
define/local wave2/r/1/1 1000. !   !first wave in test2
define/local delta/r/1/2 {P4} ! delta 1 and 2, for left and right

!define/local c3/c/1/10 :sky   

write/out delta1,delta2 = {delta(1)} {delta(2)}

comp/key tol = 0.02
!comp/key tol = 'tol'/5.

copy/tab {P1} test1
copy/tab {P2} test2
!comp/key i1 = {test1.tbl,TBLCONTR(4)} - {P4}/pix
comp/key wave1 = {test1.tbl,:wave,@{test1.tbl,TBLCONTR(4)}} - {delta(2)}
!comp/key i2 = {P4}/{pix}
comp/key wave2 = {test2.tbl,:wave,@1} + {delta(1)}
!del/row test1 {i1}..{test1.tbl,TBLCONTR(4)}}
!del/row test2 1..{i2}
select/tab test1 :wave.le.{wave1}
select/tab test2 :wave.ge.{wave2}
copy/tab test1 aux1
copy/tab test2 aux2


!join/table test1 :wave test2 :wave test 'tol'
join/table aux1 :wave aux2 :wave test 'tol'


comp/tab test :wave = :wave_1

!comp weigthed flux in overlap region:

comp/tab test 'c1' = sqrt(1/(1/'c1'_1**2 + 1/'c1'_2**2))
comp/tab test 'c2' = ('c2'_1/'c1'_1**2 + 'c2'_2/'c1'_2**2)*'c1'**2

write/key c1/c/1/10 :stdevB 
write/key c2/c/1/10 :normfluxB 
!write/out 'c1' 'c2'
comp/tab test 'c1' = sqrt(1/(1/'c1'_1**2 + 1/'c1'_2**2))
comp/tab test 'c2' = ('c2'_1/'c1'_1**2 + 'c2'_2/'c1'_2**2)*'c1'**2


select/tab test 'c2'.ne.*
copy/tab test aux3

select/tab test1 :wave .LT. {test.tbl,:wave,@1} 
copy/tab test1 test3

select/tab test2 :wave .GT. {test.tbl,:wave,@{test.tbl,TBLCONTR(4)}} 
copy/tab test2 test4

merge/tab test3 aux3 test4 {P3}
!select/tab {P1} all
!select/tab {P2} all



