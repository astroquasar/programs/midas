!  Procedure delete.prg                   (c) wt 2006
!
!proceso que agrega puntos al fitspline

set/format i1

DEFINE/PARAMETER P1 ?	      C   "tabla de datos"
DEFINE/PARAMETER P2 ?	      C   "tabla puntos"
DEFINE/LOCAL T/c/1/100 {P1}
DEFINE/LOCAL M/c/1/100 {P2}
DEFINE/LOCAL dir_prog/c/1/100 /Users/vincent/prog/midas


get/gcurs {M} a
comp/tab {M} :n=seq
@@ {dir_prog}/fitspline1 {T} {M} ? ? 0,0 0.15

name/col table_{P1} :n ? F5.0

set/graph color=1 stype=0
plot/tab {P1} :wave :flux
set/graph color=2 stype=5
overplot/tab table_{P1} :x_axis :y_axis
set/graph color=3 stype=0
overplot/tab {P1} :wave :fit
set/graph color=4
overplot/ident table_{P1} :x_axis :n


FIN: