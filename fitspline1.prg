! MIDAS procedure  FITSPLINE1		 			23.01.95 
!								(C)SVCLM
DEFINE/PARAMETER P1 ?           C   "Enter table with data:"
DEFINE/PARAMETER P2 ?           C   "Enter table with cursor values:"
DEFINE/PARAMETER P3 x_axis      C   "Enter indep. column:"
DEFINE/PARAMETER P4 y_axis      C   "Enter dep. column:"
DEFINE/PARAMETER P5 0,0         C   "Enter derivatives at boundaries:"
DEFINE/PARAMETER P6 2.0         C   "Enter wavelength bin (AA)"
!
write/key TABDATA/c/1/50    {P1}
write/key TABCUR/c/1/50     {P2}
write/key indcol/c/1/50    {P3}
write/key depcol/c/1/50       {P4}
write/key DERIV/r/1/2     {P5}
write/key tolerance/r/1/1     {P6}
!
run /Users/vincent/prog/midas/fitspline1




