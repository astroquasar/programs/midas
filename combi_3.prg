!
! MIDAS procedure  COMBI1_3		 	                03.11.00 
!								(C)SVCLM
! same as combi_2243 but for 3 spectra instead of 4
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1_3, adding 3 exposures
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter input table 3:"
DEFINE/PARAMETER P4 ?      C   "Enter output table:"
!
!         !!!!!!!!!!!  A:          
!
copy/tab {P1} {P4}
comp/tab {P4} :flux1 = :fluxA 
comp/tab {P4} :sigma1 = :sigmaA 
!
copy/tt {P2} :fluxA {P4} :flux2
copy/tt {P2} :sigmaA {P4} :sigma2
!
copy/tt {P3} :fluxA {P4} :flux3
copy/tt {P3} :sigmaA {P4} :sigma3
!
!                         Coadd flux and sky:
!
comp/tab {P4} :sigmaA = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2))
comp/tab {P4} :fluxA = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2)*:sigmaA**2

!         !!!!!!!!!!!  B:
          
comp/tab {P4} :flux1 = :fluxB
comp/tab {P4} :sigma1 = :sigmaB 
!
copy/tt {P2} :fluxB {P4} :flux2
copy/tt {P2} :sigmaB {P4} :sigma2
!
copy/tt {P3} :fluxB {P4} :flux3
copy/tt {P3} :sigmaB {P4} :sigma3
!
!                         Coadd flux and sky:
!
comp/tab {P4} :sigmaB = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2))
comp/tab {P4} :fluxB = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2)*:sigmaB**2

del/col {P4} :flux1,:flux2,:flux3,:sigma1,:sigma2,:sigma3
