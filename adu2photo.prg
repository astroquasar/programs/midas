DEFINE/LOCAL CATAL/I/1/1 0

DEFINE/PAR P1 ? IMAGE "Enter catalog:"

cat_loop:

store/frame IN_A {P1} 1 finito
write/out {IN_A} "ADU --> e-"

!!BLUE binning=2x3 fast read-out!!!!!:
!g=0.47 e/ADU
!phi=2.6 e
!bias=1350.

!!BLUE binning=2x3 slow read-out!!!!!:
!g=0.47 e/ADU
!phi=1.9 e
!bias=1180.

!binning=1x1 fast read-out Mage!!!!:
!g=0.82 e/DN
!phi=3.1 e 
!bias=


!!! BLUE SEPT 29
!comp/ima test = {IN_A} - 1180.
!comp/ima {IN_A} = test*0.47
!write/desc {IN_A} start 1,1
!write/desc {IN_A} step 1,1

!!! red SEPT 29
!comp/ima test = {IN_A} - 970.
!comp/ima {IN_A} = test*0.95
!write/desc {IN_A} start 1,1
!write/desc {IN_A} step 1,1

!Mage
comp/ima test= {IN_A} - 1370 
comp/ima {IN_A}=test*0.82
write/desc {IN_A} start 1,1
write/desc {IN_A} step 1,1




goto cat_loop

finito:
