!
! MIDAS procedure  ECHELLEADD_2243a		                03.11.00 
!								(C)SVCLM
! same as ecehlleadd_2243 but for 7 spectra instead of 4.
! Coadd orders. Flux
! is weighted with inverse of variances.
!
WRITE/OUT Procedure ECHELLEADD
DEFINE/PARAMETER P1 ?      C   "Enter root name 1:"
DEFINE/PARAMETER P2 ?      C   "Enter root name 2:"
DEFINE/PARAMETER P3 ?      C   "Enter root name 3:"
DEFINE/PARAMETER P4 ?      C   "Enter root name 4:"
DEFINE/PARAMETER P5 ?      C   "Enter root name 5:"
DEFINE/PARAMETER P6 ?      C   "Enter root name 6:"
DEFINE/PARAMETER P7 ?      C   "Enter root name 7:"
DEFINE/PARAMETER P8 ?      C   "Enter output root name:"
!DEFINE/PARAMETER P9 ?      C   "Enter order1,order2:"
!
define/local N/i/1/1 1
!define/local ORDER/i/1/2 {P9}
set/format I4
!
!DO N = {ORDER(1)} {ORDER(2)}
DO N = 1 31
!
write/out Now working on order 'N'
@@ /home/slopez/he1104/prg/combi_7 {P1}{N} {P2}{N} {P3}{N} {P4}{N} {P5}{N} {P6}{N} {P7}{N}  {P8}{N}
!
ENDDO
