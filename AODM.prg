assign/graph g,0

!!! NEW gives W_r = rest-frame W

DEFINE/PAR P1 ? IMAGE "Enter redshift:"
DEFINE/PAR P2 ? IMAGE "Enter low vel:"
DEFINE/PAR P3 ? IMAGE "Enter high vel:"

!define/local tablein/c/1/41 spSpec-52731-1214-199
define/local tablein/c/1/41 GRB060607
define/local fluxcol/c/1/41 :normflux
define/local sigmacol/c/1/41 :stdev
define/local z/r/1/1        {P1}

!define/local dlambda/r/1/1  0.06756496   !step in lambda blue MIKE BLUE
!define/local dlambda/r/1/1  0.13595712180000   !step in lambda MIKE RED
!define/local dlambda/r/1/1   0.79210362200000  !step in lambda SDSS rest
!define/local dlambda/r/1/1  0.043034220000000   !step in lambda UVES
define/local dlambda/r/1/1  0.3993500E-01   !step in lambda UVES GRB


!define/local wave0/r/1/1    2576.877  !!MnII
!define/local f/r/1/1        0.3508
!define/local wave0/r/1/1    2594.499  !!MnII
!define/local f/r/1/1        0.2710
!define/local wave0/r/1/1    2852.9642  !!MgI
!define/local f/r/1/1        1.8100
!define/local wave0/r/1/1    2586.650  !!FeII
!define/local f/r/1/1        0.0691
!write/key wave0/r/1/1    2796.352  !!MgII
!write/key f/r/1/1        0.6123
!write/key wave0/r/1/1    2803.5310  !!MgII
!write/key f/r/1/1        0.3140
!define/local wave0/r/1/1    2600.1729  !!FeII
!define/local f/r/1/1        0.2390
!define/local wave0/r/1/1    2374.4612  !!FeII
!define/local f/r/1/1        0.0313
define/local wave0/r/1/1    1808.0126 !SiII
define/local f/r/1/1        0.00218
!define/local wave0/r/1/1    1548.1949  !!CIV
!define/local f/r/1/1        0.1908


define/local dv/r/1/1 1.            !step in v
define/local N/r/1/1 1.            !Col. den.
define/local dN/r/1/1 1.            !error in col. den.
define/local W_2796/r/1/1 1.            !Obs eq. width
define/local W_2852/r/1/1 1.            !Obs eq. width
define/local W_2600/r/1/1 1.            !Obs eq. width
define/local errorW2796/r/1/1 1.            !Obs eq. width
define/local errorW2852/r/1/1 1.            !Obs eq. width
define/local errorW2600/r/1/1 1.            !Obs eq. width

comp/key z = 'z' + 1
COMP/TABL {tablein} :v = (:wave/('wave0'*'z')-1)*299790
COMP/key dv = {dlambda}/('wave0'*'z')*299790
comp/tab {tablein} :Na=LN(1/{fluxcol})*3.767E+14/('wave0'*'f')
comp/tab {tablein} :dNa2=({sigmacol}*3.767E+14/('wave0'*'f'*{fluxcol}))**2
comp/tab {tablein} :aux=1.-{fluxcol}
comp/tab {tablein} :aux1={sigmacol}**2

set/format f8.5
set/graph xformat=
plot/axes -300,300 0,1.3 -100,-60,10,10   
overplot/tab {tablein} :v {fluxcol}
select/tab {tablein} :v.GE.{P2}.AND.:v.LE.{P3}  !! test

set/graph color=2
label/graph "{wave0}" 0,1.2
overplot/tab {tablein} :v {fluxcol}
set/graph color=1


stat/tab {tablein} :Na
comp/key N = 'OUTPUTI(2)'*'OUTPUTR(3)'*'dv'
stat/tab {tablein} :dNa2
comp/key dN = M$SQRT('OUTPUTI(2)'*'OUTPUTR(3)'*'dv')
stat/tab {tablein} :aux
comp/key W_2796 = ('OUTPUTI(2)'*'OUTPUTR(3)'*{dlambda})/'z'
stat/tab {tablein} :aux1
comp/key errorW2796 = M$SQRT('OUTPUTI(2)'*'OUTPUTR(3)')*{dlambda}/'z'
select/tab {tablein} all




!write/out N = 'N'    dN = 'dN'    W = 'W_2796' +/- 'errorW2796'


comp/key dN = M$LOG10({N}+{dN})
comp/key N = M$LOG10({N})
comp/key dN = {dN} - {N}
write/out logN = 'N'+/-'dN'   W = 'W_2796' +/- 'errorW2796'

goto there

!------------------------------------------------------------------------------------------


comp/key wave0 =     2852.9642  !!MgI
comp/key f =        1.8100


COMP/TABL {tablein} :v = (:wave/('wave0'*'z')-1)*299790
COMP/key dv = {dlambda}/('wave0'*'z')*299790
comp/tab {tablein} :Na=LN(1/{fluxcol})*3.767E+14/('wave0'*'f')
comp/tab {tablein} :dNa2=({sigmacol}*3.767E+14/('wave0'*'f'*{fluxcol}))**2
comp/tab {tablein} :aux=1.-{fluxcol}
comp/tab {tablein} :aux1={sigmacol}**2

set/graph xformat=none
overplot/axes -1300,1300 0,1.3 -100,-60,10,70   
overplot/tab {tablein} :v {fluxcol}
select/tab {tablein} :v.GE.{P2}..AND.:v.LE.{P3}  !! test
set/graph color=2
label/graph "{wave0}" 0,1.2
overplot/tab {tablein} :v {fluxcol}
set/graph color=1


stat/tab {tablein} :Na
comp/key N = 'OUTPUTI(2)'*'OUTPUTR(3)'*'dv'
stat/tab {tablein} :dNa2
comp/key dN = M$SQRT('OUTPUTI(2)'*'OUTPUTR(3)'*'dv')
stat/tab {tablein} :aux
comp/key W_2852 = ('OUTPUTI(2)'*'OUTPUTR(3)'*{dlambda})
stat/tab {tablein} :aux1**2
comp/key errorW2852 = M$SQRT('OUTPUTI(2)'*'OUTPUTR(3)')*{dlambda}
select/tab {tablein} all

write/out N = 'N'    dN = 'dN'    W +/-dW= 'W_2852' 'errorW2852'


comp/key dN = M$LOG10({N}+{dN})
comp/key N = M$LOG10({N})
comp/key dN = {dN} - {N}
write/out logN = 'N'+/-'dN'   W = 'W_2852'


!-----------------------------------------------------------

there1:

comp/key wave0 =    2600.1729  !!FeII
comp/key f =        0.2390


COMP/TABL {tablein} :v = (:wave/('wave0'*'z')-1)*299790
COMP/key dv = {dlambda}/('wave0'*'z')*299790
comp/tab {tablein} :Na=LN(1/{fluxcol})*3.767E+14/('wave0'*'f')
comp/tab {tablein} :dNa2=({sigmacol}*3.767E+14/('wave0'*'f'*{fluxcol}))**2
comp/tab {tablein} :aux=1.-{fluxcol}
comp/tab {tablein} :aux1={sigmacol}**2

set/graph xformat=none
overplot/axes -1300,1300 0,1.3 -100,-60,10,130   
overplot/tab {tablein} :v {fluxcol}
select/tab {tablein} :v.GE.{P2}..AND.:v.LE.{P3}  !! test
set/graph color=2
label/graph "{wave0}" 0,1.2
overplot/tab {tablein} :v {fluxcol}
set/graph color=1


stat/tab {tablein} :Na
comp/key N = 'OUTPUTI(2)'*'OUTPUTR(3)'*'dv'
stat/tab {tablein} :dNa2
comp/key dN = M$SQRT('OUTPUTI(2)'*'OUTPUTR(3)'*'dv')
stat/tab {tablein} :aux
comp/key W_2600 = ('OUTPUTI(2)'*'OUTPUTR(3)'*{dlambda})
stat/tab {tablein} :aux1**2
comp/key errorW2600 = M$SQRT('OUTPUTI(2)'*'OUTPUTR(3)')*{dlambda}
select/tab {tablein} all

write/out N = 'N'    dN = 'dN'    W = 'W_2600'


comp/key dN = M$LOG10({N}+{dN})
comp/key N = M$LOG10({N})
comp/key dN = {dN} - {N}
write/out logN = 'N'+/-'dN' 


set/format F4.2
!write/out 'W_2796'+/-'errorW2796' "   "   'W_2852'+/-'errorW2852' "   " 'W_2600'+/-'errorW2600'

there:

!write/out 'W_2796' "     "   'W_2852' "     " 'W_2600'



