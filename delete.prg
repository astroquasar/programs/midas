!  Procedure delete.prg                   (c) wt 2006
!
!proceso que elimina puntos del fitspline

set/format i1

DEFINE/PARAMETER P1 ?	      C   "tabla de datos"
DEFINE/PARAMETER P2 ?	      C   "tabla de puntos"
DEFINE/LOCAL T/c/1/100 {P1}
DEFINE/LOCAL M/c/1/100 {P2}
DEFINE/LOCAL dir_progs/c/1/100  /Users/vincent/prog/midas

comp/tab table_{P1} :n=seq
name/col table_{P1} :n ? F5.0
set/graph color=1 stype=0
plot/tab {P1} :wave :flux
set/graph color=2 stype=5
overplot/tab table_{P1} :x_axis :y_axis
set/graph color=3 stype=0
overplot/tab {P1} :wave :fit
set/graph color=4
overplot/ident table_{P1} :x_axis :n



!LOOP:
DEFINE/PARAMETER P3 ?         C   "ingrese los puntos a borrar separados por coma: "



del/key points
write/key points/c/1/50    {P3}
delete/row {M} {points}
write/out "deleted points" {points}
del/col {T} :fit
@@ {dir_progs}/fitspline1 {T} {M} :x_axis :y_axis 0,0 0.15


comp/tab table_{P1} :n=seq
name/col table_{P1} :n ? F5.0
set/graph color=1 stype=0
plot/tab {P1} :wave :flux
set/graph color=2 stype=5
overplot/tab table_{P1} :x_axis :y_axis
set/graph color=3 stype=0
overplot/tab {P1} :wave :fit
set/graph color=4
overplot/ident table_{P1} :x_axis :n



FIN:
