C
C PROGRAM MAKETABLE.FOR                (c) SVCLM May 1995
C
C
C.PURPOSE
C
C  COMPUTES START VALUES FOR PARAMETERS IN COLUMN ":column" (INTENSITY
C  OF GAUSS "int1","int2" OR PARAMETER 0 OF SKY POLYNOM "sky0") IN 
C  TABLE "startopt" 
C  FOR OPTIMAL EXTRACTION WITH PROGRAM "ECHELLEOPT.FOR" ("ECHELLEOPT.PRG"). 
C
C.ALGORITHM
C  POSITIONS ALONG THE ORDERS ARE DEFINED BY THE REGRESSION COEFFICIENTS
C  IN TABLE "ORDER". SLIT AND OFFSET DEFINES INITIAL POSITION OF
C  GAUSS OR SKY WINDOW. SLIT=1 FOR int1 OR int2. EXTRACTION METHOD IS AVERAGE.
C
C.INPUT
C  NAME OF COLUMN WITH INTENSITY (:int1 or :int2 or :sky0)
C  ECHELLE KEYWORDS OFFSET AND SLIT
C
C.OUTPUT
C  IN TABLE "startopt" COLUMNS :x :int1 or :int2 or :sky0
C
C The connection file contains:
C  IN_A   input image with object 
C  IN_B   input table with orders 
C  OUT_A  image with extracted orders
C------------------------------------------------------------------
C
      PROGRAM MAKETABLE
      IMPLICIT NONE
      INTEGER NDIM
      PARAMETER (NDIM=2)
      INTEGER MADRID, NAC, NAR, IMNO
      INTEGER I,IAV,INDX,IORD,IORD1
      INTEGER IORD2,ISTAT,NA,NAXISA,NAXISB
      INTEGER NCOL,NPAR,NROW,NSORT,PNTRA
      INTEGER PNTRB,STATUS,TID,INO
      INTEGER NPIXA(3),NPIXB(3),IPAR(10),KUN,KNUL
C
      DOUBLE PRECISION STEPA(3),STEPB(3), STARTA(3),STARTB(3)
      REAL  RPAR(10)
      REAL  CUTS(6)
      REAL XMIN,XMAX,XMIN1,XMAX1
      REAL PARAMS(12)
C
      DOUBLE PRECISION DPAR(100)
C
      CHARACTER*72   IDENA,IDENB
      CHARACTER*64   CUNITA,CUNITB,DESNAM
      CHARACTER*60   FRMIN,FRMOUT,INA1,INA2
      CHARACTER*60   TABLE,NAME,TYPE,COLUMN
C
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      COMMON /VMR/MADRID(1)
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
C
      DATA CUNITB/
     +     'FLUX            PIXEL                           ORDER'/
C
C  initialize system
C
      CALL STSPRO('MAKETABLE')
      CALL STKRDC('IN_A',1,1,60,I,INA1,KUN,KNUL,STATUS)
      CALL STKRDC('IN_B',1,1,60,I,TABLE,KUN,KNUL,STATUS)
      CALL STKRDC('OUT_A',1,1,60,I,INA2,KUN,KNUL,STATUS)
      CALL STKRDC('INPUTC',1,1,60,I,NAME,KUN,KNUL,STATUS)
      CALL STKRDR('INPUTR',1,3,I,RPAR,KUN,KNUL,STATUS)
      CALL STKRDI('INPUTI',1,2,I,IPAR,KUN,KNUL,STATUS)
      CALL STKRDC('IN_COL',1,1,60,I,COLUMN,KUN,KNUL,STATUS)
      IORD1  = IPAR(1)
      IORD2  = IPAR(2)
      FRMIN = INA1
      FRMOUT = INA2
C
C  input parameters in table
C
      CALL TBTOPN(TABLE,F_U_MODE,TID,STATUS)
      CALL TBIGET(TID,NCOL,NROW,NSORT,NAC,NAR,STATUS)
C
C  map input FRM
C
      CALL STIGET(FRMIN,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,
     .            NDIM,NAXISA,NPIXA,STARTA,STEPA,IDENA,
     +            CUNITA,PNTRA,IMNO,STATUS)
      PARAMS(1) = RPAR(1)/ABS(STEPA(2))
      PARAMS(2) = RPAR(2)
      PARAMS(3) = RPAR(3)/ABS(STEPA(2))
C
C  read coeffs
C
      NAME   = NAME(1:59)//' '
      INDX   = INDEX(NAME,' ') - 1
      DESNAM = NAME(1:INDX)//'C'
      CALL STDRDC(TID,DESNAM,1,17,4,IAV,TYPE,KUN,KNUL,ISTAT)
      TYPE   = 'MULT'
      IF (TYPE.EQ.'MULT') THEN
          DESNAM = NAME(1:INDX)//'I'
          CALL STDRDI(TID,DESNAM,4,4,IAV,IPAR,KUN,KNUL,ISTAT)
          NA     = (IPAR(3)+1)* (IPAR(4)+1)
          DESNAM = NAME(1:INDX)//'R'
          CALL STDRDR(TID,DESNAM,1,4,IAV,PARAMS(4),KUN,KNUL,ISTAT)
          DESNAM = NAME(1:INDX)//'D'
          CALL STDRDD(TID,DESNAM,1,NA,IAV,DPAR,KUN,KNUL,ISTAT)
          CALL TBTCLO(TID,STATUS)
C
C ... map output FRM
C
          IF (IORD1.EQ.IORD2 .AND. IORD1.EQ.0) THEN
              IORD1  = PARAMS(6)
              IORD2  = PARAMS(7)
          END IF
          STARTB(1) = STARTA(1)
          STARTB(2) = IORD1
          STEPB(1)  = STEPA(1)
          STEPB(2)  = 1.
          IDENB     = IDENA
          NAXISB    = 2
          NPIXB(1)  = NPIXA(1)
          NPIXB(2)  = IORD2 - IORD1 + 1
          CALL STIPUT(FRMOUT,D_R4_FORMAT,F_O_MODE,F_IMA_TYPE,
     +                NAXISB,NPIXB,STARTB,
     +                STEPB,IDENB,CUNITB,PNTRB,INO,STATUS) 
C
C  extract order
C
          NPAR      = 10
          PARAMS(8) = IPAR(3)
          PARAMS(9) = IPAR(4)
          XMIN      = 1.E30
          XMAX      = -XMIN
          DO 10 I = IORD1,IORD2
        write(*,*) "order",I
              PARAMS(10) = I
              IORD       = I - IORD1 + 1


              CALL EXTR1M(MADRID(PNTRA),NPIXA(1),NPIXA(2),
     +                    MADRID(PNTRB),NPIXB(1),NPIXB(2),IORD,
     +                    NPAR,PARAMS,NA,DPAR,XMIN1,XMAX1,COLUMN)

              XMIN   = AMIN1(XMIN,XMIN1)
              XMAX   = AMAX1(XMAX,XMAX1)
   10     CONTINUE
      ELSE
          CALL STTPUT(' Error in dispersion coefficients ',ISTAT)
          GO TO 20
      END IF
C
C  descriptor handling
C
      CUTS(1) = XMIN
      CUTS(2) = XMAX
      CUTS(3) = XMIN
      CUTS(4) = XMAX
      CUTS(5) = 0.
      CUTS(6) = 0.
      CALL DSCUPT(IMNO,INO,' ',STATUS)
      CALL STDWRR(INO,'LHCUTS',CUTS,1,6,KUN,STATUS)
C
C  free data
C
C      CALL STFCLO(INO,STATUS)
   20 CALL STSEPI
      END

      SUBROUTINE EXTR1M(IN,NPIXA1,NPIXA2,OUT,NB1,NB2,INDX,NPAR,PAR,ND,
     +                  DPAR,XMIN,XMAX,COLUMN)
C
C       EXTRACTION ROUTINE - BILINEAR INTERPOLATION
C
C   ARGUMENTS
C IN     REAL*4  INPUT IMAGE
C NPIXA1 INTG*4  NO. OF SAMPLES
C NPIXA2 INTG*4  NO. OF LINES
C OUT    REAL*4  EXTRACTED ORDERS
C NB1    INTG*4  NO. OF SAMPLES
C NB2    INTG*4  NO. OF ORDERS
C INDX   INTG*4  SEQUENTIAL NO. OF THE ORDER
C NPAR   INTG*4  NO. OF EXTRACTION PARAMETERS
C PAR    REAL*4  EXTRACTION PARAMETERS
C ND     INTG*4  NO. OF COEFFS.
C DPAR   REAL*8  COEFFS. TO DEFINE ORDER POSITION
C XMIN   REAL*4  MINIMUM VALUE
C XMAX   REAL*4  MAXIMUMVALUE
C
      IMPLICIT NONE
      INTEGER  NB1, NB2, NPAR, NPIXA1, NPIXA2, ND
      REAL OUT(NB1,NB2),PAR(NPAR)
      REAL IN(NPIXA1,NPIXA2)
      REAL XMIN,XMAX
      DOUBLE PRECISION DPAR(ND)
      DOUBLE PRECISION XVAL,YVAL,AX
      DOUBLE PRECISION WIDTH,SKEW,HWIDTH,OFFSET,DC
      DOUBLE PRECISION YV,WW(50)
      DOUBLE PRECISION SKWDX,SKWDY,SKWOF,SUM
      DOUBLE PRECISION DEGREE,DD,DSIN,DCOS
      INTEGER          NWW, IWIDTH, IYLIM, NX, N1, N2, K, L, IX, INDX
      INTEGER          IX1, IP, L1, K1, IW
      LOGICAL IF1D,NULL
      INTEGER          LOSLIT, HISLIT
      DOUBLE PRECISION YLOW, YHIGH, WSUM

      INTEGER ROW,TABID,STATUS,COLNUM,COLSKY0,COLX
      CHARACTER*60 COLUMN
      REAL sky0,ABSINT
C
C     DEGREE = RAD / DEGREE
C
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
      DATA DEGREE/0.017453292519943295769237D+0/
C

      NWW    = 50

C     Open table with start parameters:
 
      CALL TBTOPN('startopt.tbl',F_IO_MODE,TABID,STATUS)
      CALL TBLSER(TABID,COLUMN,COLNUM,STATUS)
      CALL TBLSER(TABID,'sky0',COLSKY0,STATUS)
      CALL TBLSER(TABID,'x',COLX,STATUS)
C
C ... define extraction constants
C
      WIDTH  = PAR(1)
      IWIDTH = WIDTH
      HWIDTH = 0.5*WIDTH
      SKEW   = PAR(2)
      SKWDX  = 0.0
      SKWDY  = 1.0
      SKWOF  = 0.0
      XMIN   = 0.0
      XMAX   = XMIN
C      OFFSET = PAR(3) - HWIDTH + 0.5
      OFFSET = PAR(3)
      IF (SKEW.NE.0.0) THEN       
          DD      = SKEW*DEGREE
          SKWDX  = -DSIN(DD)      !   DX PER PIXEL
          SKWDY  = DCOS(DD)       !   DY PER PIXEL
          SKWOF  = -SKWDX*HWIDTH  !   2D INTERPOLATION
          IF1D   = .FALSE.
      END IF
      AX     = 1.D0 + SKWOF
      IYLIM  = NPIXA2 - IWIDTH
      NX     = SKWOF
      N1     = NX
      N2     = NPIXA1 - NX
C
C ... get ordinate values
C
      K      = PAR(8)
      L      = PAR(9)
      YV     = PAR(10)
C
C ... loop over the samples
C
      DO 10 IX = 1,N1
          OUT(IX,INDX) = 0.
   10 CONTINUE

      DO 100 IX = N2 + 1,NPIXA1
          OUT(IX,INDX) = 0.
  100 CONTINUE

          DO 50 IX1 = N1 + 1,N2

              ROW = (INDX-1)*NPIXA1 + IX1
              IP     = 0
              DC     = 1.D0
              YVAL   = 0.D0
              XVAL   = AX

C Computes position of the center of the slit from the coefficients of
C the bivariate polynomial stored in table order.

              DO 30 L1 = 0,L
                  IP     = IP + 1
                  WW(IP) = DC
                  YVAL   = YVAL + WW(IP)*DPAR(IP)
                  DO 20 K1 = 1,K
                      IP     = IP + 1
                      WW(IP) = WW(IP-1)*XVAL
                      YVAL   = YVAL + WW(IP)*DPAR(IP)
   20             CONTINUE
                  DC     = DC*YV
   30         CONTINUE
C
C Corrects the central position for the offset
C OFFSET does not take into account HWIDTH 
C
              YVAL   = YVAL + OFFSET
C
              YLOW   = YVAL - HWIDTH
              YHIGH  = YVAL + HWIDTH
              LOSLIT = INT(YLOW)
              HISLIT = INT(YHIGH)

c              IF (LOSLIT.LT.1)      LOSLIT = 1
c              IF (HISLIT.GT.NPIXA2) HISLIT = NPIXA2
               IF ((LOSLIT.LT.1) .OR. (HISLIT.GT.NPIXA2)) THEN
                  CALL TBRDEL(TABID,ROW,STATUS)
                  GOTO 45
               ENDIF

              SUM    = 0.D0
              WSUM   = 0.D0
              IX     = INT(XVAL)
C
	              IF (LOSLIT.EQ.HISLIT) THEN
                  SUM = (YHIGH-YLOW)*IN(IX,LOSLIT)
C
              ELSE
C
              DO 40 IW = LOSLIT,HISLIT

                 IF (IW.EQ.LOSLIT) THEN
                     SUM = SUM + IN(IX,IW)*(LOSLIT+1.-YLOW)

                 ELSE IF (IW.EQ.HISLIT) THEN
                     SUM = SUM + IN(IX,IW)*(YHIGH-HISLIT)

                 ELSE
                     SUM = SUM + IN(IX,IW)

                 ENDIF


   40         CONTINUE

C
              ENDIF
C
C ******* INDX = ORDER NUMBER ********* IX1 = X PIXEL
C
              OUT(IX1,INDX) = SUM
C 
C OUT(IX1,INDX) and sky0 in table "startopt.tbl":
C 

              CALL TBERDR(TABID,ROW,COLSKY0,sky0,NULL,STATUS)
              CALL TBEWRR(TABID,ROW,COLNUM,OUT(IX1,INDX),STATUS)
              CALL TBEWRI(TABID,ROW,COLX,IX1,STATUS)            
45        CONTINUE
              XMIN   = AMIN1(XMIN,OUT(IX1,INDX))
              XMAX   = AMAX1(XMAX,OUT(IX1,INDX))
              AX     = AX + 1.D0
   50     CONTINUE

      CALL TBTCLO(TABID,STATUS)

      RETURN

      END






