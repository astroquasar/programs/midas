!
! MIDAS procedure  FIT_CONT3		                29.07.01 
!								(C)SVCLM
! Correct tableA_000{N} to fit B.
!
WRITE/OUT Procedure FIT_CONT
DEFINE/PARAMETER P1 ?      C   "Enter order:"
!
define/local N/i/1/1 {P1}
define/local N1/i/1/1 {tableA_{N}.tbl,TBLCONTR(4)}
define/local I/i/1/1 1
define/local wave/r/1/1 1.
define/local wave1/r/1/1 1.
define/local wave2/r/1/1 1.
define/local y/r/1/1 1.
set/format I4
!
!goto there

comp/tab test :normfluxB=:fluxB/:fitB
set/graph color=3

do I = 1 {N1}
comp/key wave = {tabletest.tbl,:x_axis,@{I}} 
comp/key y = {tabletest.tbl,:y_axis,@{I}} 
comp/key wave1 = {wave} - 0.5
comp/key wave2 = {wave} + 0.5
select/tab test :wave.ge.{wave1}.and.:wave.le.{wave2}
stat/tab test :normfluxB
if {OUTPUTR(1)} .ge. 0.92 then
!
 comp/key y = {y}*{OUTPUTR(3)}
 write/tab tabletest :y_axis @{I} {y}
!
endif
enddo

select/tab test all
@@ /home/slopez/he1104/prg/fitspline1 test tabletest ? ? 0,0

overplot/tab test :wave :fit
set/graph color=1 
