C
C PROGRAM ECHELLEOPTDELTA.FOR                (c) SVCLM May 1995
C
C
C.PURPOSE
C
C OPTIMAL EXTRACTION OF ECHELLE ORDERS. DELTA CONSTANT.
C
C ALGORITHM
C
C PERFORMS OPTIMAL EXTRACTION OF ECHELLE ORDERS BY FITTING GAUSSIANS TO THE
C SPATIAL PROFILE (L-M-METHOD). COSMICS CAN BE CLIPPED IF CERTAIN 
C PARAMETERS (E.G. POSITION, WIDTH) ARE HELD FIXED 
C (FOR A WAVELENGTH CHANNEL) IN A SECOND RUN OF THE ROUTINE.
C SLIT AND OFFSET DEFINES SPATIAL RANGE. INPUT TABLE "STARTOPT" WITH
C START PARAMETERS
C
C.INPUT
C  TABLE "STARTOPT" FROM PROCEDURE "MAKETABLE.PRG"
C  FRAME WITH FLAT FIELDED OBJECT                          -----> sufix A 
c  FRAME WITH VARIANCES                                    -----> sufix C
C  PARAMETER TO BE REBINED AND MERGED (DEFAULT "INT1")
C
C.OUTPUT
C  TABLE WITH NEW PARAMETERS
C  FRAME WITH EXTRACTED ORDERS                             -----> sufix B
C  FRAME WITH FITTED PROFILE                               -----> sufix D
C
C The connection file contains:
C  IN_A   input image with object 
C  OUT_A  image with extracted orders
C  OUT_B  image with fitted profile
C------------------------------------------------------------------
C
      PROGRAM ECHELLEOPT
      IMPLICIT NONE
      INTEGER NDIM
      PARAMETER (NDIM=2)
C...For Marquardt:
      INTEGER LISTA(6),MFIT(2)
C
      INTEGER MADRID, NAC, NAR, IMNO
      INTEGER I,IAV,INDX,IORD,IORD1
      INTEGER IORD2,ISTAT,NA,NAXISA,NAXISB,NAXISC,NAXISD
      INTEGER NCOL,NPAR,NROW,NSORT,PNTRA,PNTRB,PNTRC,PNTRD
      INTEGER STATUS,TID,INOB,INOD
      INTEGER NPIXA(3),NPIXB(3),NPIXC(3),NPIXD(3),IPAR(10),KUN,KNUL
C
      DOUBLE PRECISION STEPA(3),STEPB(3),STEPC(3),STEPD(4)
      DOUBLE PRECISION STARTA(3),STARTB(3),STARTC(3),STARTD(4)
      REAL  RPAR(10)
      REAL  CUTS(6)
      REAL XMIN,XMAX,XMIN1,XMAX1
      REAL PARAMS(12)
C
      DOUBLE PRECISION DPAR(100)
C
      CHARACTER*72   IDENA,IDENB,IDENC,IDEND
      CHARACTER*64   CUNITA,CUNITB,CUNITC,CUNITD,DESNAM
      CHARACTER*60   FRMIN,FRMINVAR,FRMOUT,FRMFIT,INA1,INA2,INA3,INA4
      CHARACTER*60   TABLE,INPAR,OUTPAR,NAME,TYPE
C
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      COMMON /VMR/MADRID(1)
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
C
      DATA CUNITB/
     +     'FLUX            PIXEL                           ORDER'/
C
C  initialize system
C
      CALL STSPRO('ECHELLEOPT')
      CALL STKRDC('IN_A',1,1,60,I,INA1,KUN,KNUL,STATUS)
      CALL STKRDC('IN_VAR',1,1,60,I,INA3,KUN,KNUL,STATUS)
      CALL STKRDC('IN_B',1,1,60,I,TABLE,KUN,KNUL,STATUS)
      CALL STKRDC('IN_PAR',1,1,60,I,INPAR,KUN,KNUL,STATUS)
      CALL STKRDC('OUT_PAR',1,1,60,I,OUTPAR,KUN,KNUL,STATUS)
      CALL STKRDC('OUT_A',1,1,60,I,INA2,KUN,KNUL,STATUS)
      CALL STKRDC('OUT_B',1,1,60,I,INA4,KUN,KNUL,STATUS)
      CALL STKRDC('INPUTC',1,1,60,I,NAME,KUN,KNUL,STATUS)
      CALL STKRDR('INPUTR',1,3,I,RPAR,KUN,KNUL,STATUS)
      CALL STKRDI('INPUTI',1,2,I,IPAR,KUN,KNUL,STATUS)
      CALL STKRDI('LISTA',1,6,I,LISTA,KUN,KNUL,STATUS)
      CALL STKRDI('MFIT',1,2,I,MFIT,KUN,KNUL,STATUS)
      IORD1  = IPAR(1)
      IORD2  = IPAR(2)
      FRMIN = INA1
      FRMOUT = INA2
      FRMINVAR = INA3
      FRMFIT = INA4
C
C  input parameters in table
C
      CALL TBTOPN(TABLE,F_U_MODE,TID,STATUS)
      CALL TBIGET(TID,NCOL,NROW,NSORT,NAC,NAR,STATUS)
C
C  map input FRM with object
C
      CALL STIGET(FRMIN,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,
     +            NDIM,NAXISA,NPIXA,STARTA,STEPA,IDENA,
     +            CUNITA,PNTRA,IMNO,STATUS)
      PARAMS(1) = RPAR(1)/ABS(STEPA(2))
      PARAMS(2) = RPAR(2)
      PARAMS(3) = RPAR(3)/ABS(STEPA(2))
C
C  map input FRM with variances
C
      CALL STIGET(FRMINVAR,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,
     +           NDIM,NAXISC,NPIXC,STARTC,STEPC,IDENC,
     +            CUNITC,PNTRC,IMNO,STATUS)
C 
C  read coeffs
C
      NAME   = NAME(1:59)//' '
      INDX   = INDEX(NAME,' ') - 1
      DESNAM = NAME(1:INDX)//'C'
      CALL STDRDC(TID,DESNAM,1,17,4,IAV,TYPE,KUN,KNUL,ISTAT)
      TYPE   = 'MULT'
      IF (TYPE.EQ.'MULT') THEN
          DESNAM = NAME(1:INDX)//'I'
          CALL STDRDI(TID,DESNAM,4,4,IAV,IPAR,KUN,KNUL,ISTAT)
          NA     = (IPAR(3)+1)* (IPAR(4)+1)
          DESNAM = NAME(1:INDX)//'R'
          CALL STDRDR(TID,DESNAM,1,4,IAV,PARAMS(4),KUN,KNUL,ISTAT)
          DESNAM = NAME(1:INDX)//'D'
          CALL STDRDD(TID,DESNAM,1,NA,IAV,DPAR,KUN,KNUL,ISTAT)
          CALL TBTCLO(TID,STATUS)

C
C ... map output FRM with orders
C
          IF (IORD1.EQ.IORD2 .AND. IORD1.EQ.0) THEN
              IORD1  = PARAMS(6)
              IORD2  = PARAMS(7)
          END IF
          STARTB(1) = STARTA(1)
          STARTB(2) = IORD1
          STEPB(1)  = STEPA(1)
          STEPB(2)  = 1.
          IDENB     = IDENA
          NAXISB    = 2
          NPIXB(1)  = NPIXA(1)
          NPIXB(2)  = IORD2 - IORD1 + 1
          CALL STIPUT(FRMOUT,D_R4_FORMAT,F_O_MODE,F_IMA_TYPE,
     +                NAXISB,NPIXB,STARTB,
     +                STEPB,IDENB,CUNITB,PNTRB,INOB,STATUS) 

C
C ... map output FRM with fitted profile
C
          STARTD(1) = STARTA(1)
          STARTD(2) = STARTA(2)
          STEPD(1)  = STEPA(1)
          STEPD(2)  = STEPA(2)
          IDEND     = IDENA
          NAXISD    = NAXISA
          NPIXD(1)  = NPIXA(1)
          NPIXD(2)  = NPIXA(2)
          CALL STIPUT(FRMFIT,D_R4_FORMAT,F_O_MODE,F_IMA_TYPE,
     +                NAXISD,NPIXD,STARTD,
     +                STEPD,IDEND,CUNITD,PNTRD,INOD,STATUS) 


C
C  extract order
C
          NPAR      = 10
          PARAMS(8) = IPAR(3)
          PARAMS(9) = IPAR(4)
          XMIN      = 1.E30
          XMAX      = -XMIN
          DO 10 I = IORD1,IORD2
c          DO 10 I = 20,21


              PARAMS(10) = I
              IORD       = I - IORD1 + 1

              write(*,1010) I
1010          FORMAT('order ',i2)
              CALL EXTR1M(MADRID(PNTRA),MADRID(PNTRC),MADRID(PNTRD),
     +                    NPIXA(1),NPIXA(2),MADRID(PNTRB),NPIXB(1),
     +                    NPIXB(2),IORD,NPAR,PARAMS,NA,DPAR,XMIN1,XMAX1,
     +                    INPAR,OUTPAR,LISTA,MFIT)

              XMIN   = AMIN1(XMIN,XMIN1)
              XMAX   = AMAX1(XMAX,XMAX1)

   10     CONTINUE
      ELSE
          CALL STTPUT(' Error in dispersion coefficients ',ISTAT)
          GO TO 20
      END IF

C
C  descriptor handling
C
      CUTS(1) = XMIN
      CUTS(2) = XMAX
      CUTS(3) = XMIN
      CUTS(4) = XMAX
      CUTS(5) = 0.
      CUTS(6) = 0.
c      CALL DSCUPT(IMNO,INO,' ',STATUS)
      CALL STDWRR(INOB,'LHCUTS',CUTS,1,6,KUN,STATUS)
      CALL STDWRR(INOD,'LHCUTS',CUTS,1,6,KUN,STATUS)
C
C  free data
C
C      CALL STFCLO(INO,STATUS)
   20 CALL STSEPI
      END

      SUBROUTINE EXTR1M(IN,INVAR,OUTFIT,NPIXA1,NPIXA2,OUT,NB1,NB2,
     +                  INDX,NPAR,PAR,ND,DPAR,XMIN,XMAX,
     +                  INPAR,OUTPAR,LISTA,MFIT)
C
C       EXTRACTION ROUTINE - BILINEAR INTERPOLATION
C
C   ARGUMENTS
C IN     REAL*4  INPUT IMAGE WITH OBJECT
C INVAR  REAL*4  INPUT VARIANCES
C NPIXA1 INTG*4  NO. OF SAMPLES
C NPIXA2 INTG*4  NO. OF LINES
C OUT    REAL*4  EXTRACTED ORDERS
C OUTFIT REAL*4  FITTED PROFILE
C NB1    INTG*4  NO. OF SAMPLES
C NB2    INTG*4  NO. OF ORDERS
C INDX   INTG*4  SEQUENTIAL NO. OF THE ORDER
C NPAR   INTG*4  NO. OF EXTRACTION PARAMETERS
C PAR    REAL*4  EXTRACTION PARAMETERS
C ND     INTG*4  NO. OF COEFFS.
C DPAR   REAL*8  COEFFS. TO DEFINE ORDER POSITION
C XMIN   REAL*4  MINIMUM VALUE
C XMAX   REAL*4  MAXIMUMVALUE
C

      IMPLICIT NONE

      INTEGER  NB1, NB2, NPAR, NPIXA1, NPIXA2, ND
      REAL OUT(NB1,NB2),PAR(NPAR)
      REAL IN(NPIXA1,NPIXA2)
      REAL XMIN,XMAX
      DOUBLE PRECISION DPAR(ND)
      DOUBLE PRECISION XVAL,YVAL,AX
      DOUBLE PRECISION WIDTH,SKEW,HWIDTH,OFFSET,DC
      DOUBLE PRECISION YV,WW(50)
      DOUBLE PRECISION SKWDX,SKWDY,SKWOF,SUM
      DOUBLE PRECISION DEGREE,DD,DSIN,DCOS
      INTEGER          NWW, IWIDTH, IYLIM, NX, N1, N2, J, K, L, IX, INDX
      INTEGER          IX1, IP, L1, K1, IW
      LOGICAL IF1D,NULL
      INTEGER          LOSLIT, HISLIT
      DOUBLE PRECISION YLOW, YHIGH, WSUM


      REAL sky0
C
C For MARQUARDT:
C
      INTEGER NDATA,N,MFIT(2),MA,NCA,SUMSIG
      INTEGER LISTA(6)
      REAL X(300),Y(300),SIG(300),A(6)
      REAL ERR1,ERR2,CHISQ,CHISQOLD,ALAMBDA,ALAOLD
      REAL INVAR(NPIXA1,NPIXA2)
      REAL OUTFIT(NPIXA1,NPIXA2)
      REAL COVAR(6,6),ALPHA(6,6),DELTA,YFIT,YPIX,DYDA(6)
      CHARACTER*60 FUNCS
              common delta    
C
C For Tables:
C
      CHARACTER*60 INPAR,OUTPAR
      INTEGER NC,NR,NSC,ACOL,AROW
      INTEGER ROW,TABID,TABOUTID,STATUS
      INTEGER COLINT1,COLINT2,COLPOS1,COLDELTA,COLSIGMA,COLSKY0
      INTEGER COLERR1,COLERR2,COLPIX
C
C
C     DEGREE = RAD / DEGREE
C

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
      DATA DEGREE/0.017453292519943295769237D+0/

C

      NWW    = 50

C     Open table with start parameters:
 
      CALL TBTOPN(INPAR,F_IO_MODE,TABID,STATUS)
      CALL TBLSER(TABID,'int1',COLINT1,STATUS)
      CALL TBLSER(TABID,'int2',COLINT2,STATUS)
      CALL TBLSER(TABID,'sky0',COLSKY0,STATUS)
      CALL TBLSER(TABID,'pos1',COLPOS1,STATUS)
      CALL TBLSER(TABID,'delta',COLDELTA,STATUS)
      CALL TBLSER(TABID,'sigma',COLSIGMA,STATUS)
      CALL TBLSER(TABID,'error1',COLERR1,STATUS)
      CALL TBLSER(TABID,'error2',COLERR2,STATUS)
      CALL TBLSER(TABID,'pix',COLPIX,STATUS)


C     Open table with fit parameters (same column numbers!):

      CALL TBTOPN(OUTPAR,F_IO_MODE,TABOUTID,STATUS)

C
C ... define extraction constants
C
      WIDTH  = PAR(1)
      IWIDTH = WIDTH
      HWIDTH = 0.5*WIDTH
      SKEW   = PAR(2)
      SKWDX  = 0.0
      SKWDY  = 1.0
      SKWOF  = 0.0
      XMIN   = 0.0
      XMAX   = XMIN
C      OFFSET = PAR(3) - HWIDTH + 0.5
      OFFSET = PAR(3)
      IF (SKEW.NE.0.0) THEN       
          DD      = SKEW*DEGREE
          SKWDX  = -DSIN(DD)      !   DX PER PIXEL
          SKWDY  = DCOS(DD)       !   DY PER PIXEL
          SKWOF  = -SKWDX*HWIDTH  !   2D INTERPOLATION
          IF1D   = .FALSE.
      END IF             
      AX     = 1.D0 + SKWOF
      IYLIM  = NPIXA2 - IWIDTH
      NX     = SKWOF
      N1     = NX
      N2     = NPIXA1 - NX
C
C ... get ordinate values
C
      K      = PAR(8)
      L      = PAR(9)
      YV     = PAR(10)
C
C ... loop over the samples
C

      DO 10 IX = 1,N1            
          OUT(IX,INDX) = 0.
   10 CONTINUE


      DO 100 IX = N2 + 1,NPIXA1
          OUT(IX,INDX) = 0.
  100 CONTINUE

C----------Data for all MARQUARDT loops------------------------------------

c              LISTA(6) = 3    !common sigma
c              LISTA(5) = 2    !pos1
c              LISTA(1) = 1    !int1
c              LISTA(4) = 5    !pos2
c              LISTA(2) = 4    !int2
c              LISTA(3) = 6    !sky0

              FUNCS = 'GAUSS2'

              MA = 6
c              MFIT = 3
              NCA = 6
C--------------------------------------------------------------------------
C...Here starts the loop for each wavelength channel...
C----------------IX = POS IN X---------IW = POS IN Y ----------------------
C ******* INDX = ORDER NUMBER ********* IX1 = X PIXEL



          DO 500 IX1 = N1 + 1,N2


              ROW = (INDX-1)*NPIXA1 + IX1
              IP     = 0
              DC     = 1.D0
              YVAL   = 0.D0
              XVAL   = AX

C Computes position of the center of the slit from the coefficients of
C the bivariate polynomial stored in table order.

              DO 30 L1 = 0,L
                  IP     = IP + 1
                  WW(IP) = DC
                  YVAL   = YVAL + WW(IP)*DPAR(IP)
                  DO 20 K1 = 1,K
                      IP     = IP + 1
                      WW(IP) = WW(IP-1)*XVAL
                      YVAL   = YVAL + WW(IP)*DPAR(IP)
   20             CONTINUE
                  DC     = DC*YV
   30         CONTINUE
C
C Corrects the central position for the offset
C OFFSET does not take into account HWIDTH 
C
              YVAL   = YVAL + OFFSET
C
              YLOW   = YVAL - HWIDTH
              YHIGH  = YVAL + HWIDTH
              LOSLIT = INT(YLOW)
              HISLIT = INT(YHIGH)
c
c--- Order out of frame?:
c
              IF ((LOSLIT.LT.1) .OR. (HISLIT.GT.NPIXA2)) THEN
                 CALL TBRDEL(TABOUTID,ROW,STATUS)
                 GOTO 50
              ENDIF
c
              SUM    = 0.D0
              WSUM   = 0.D0
              IX     = INT(XVAL)
C

	              IF (LOSLIT.EQ.HISLIT) THEN

                  SUM = (YHIGH-YLOW)*IN(IX,LOSLIT)

C
              ELSE
C
              NDATA = 0
              SUMSIG = 0

c             write(*,*)  IX1,LOSLIT,HISLIT
              DO 40 IW = LOSLIT,HISLIT

c                 IF (IW.EQ.LOSLIT) THEN
c                     SUM = SUM + ABS(IN(IX,IW))*(LOSLIT+1.-YLOW)
c
c                 ELSE IF (IW.EQ.HISLIT) THEN
c                     SUM = SUM + ABS(IN(IX,IW))*(YHIGH-HISLIT)
c
c                 ELSE
c                     SUM = SUM + ABS(IN(IX,IW))
c
c                 ENDIF

C
C------ Data for MARQUARDT loops in each PIXEL position -------------------
C
          
            NDATA = NDATA + 1
         X(NDATA) = IW - YVAL   !Pos. along slit with resp. to order center  
         Y(NDATA) = IN(IX,IW)   !Intensity at that place

c       write(*,*) INVAR(IX,IW) 
       SIG(NDATA) = SQRT(INVAR(IX,IW))  !Standard dev. of Y(NDATA)

c         IF (IX1.eq.1500) then
c         write(*,*) IX1,X(NDATA),Y(NDATA),SIG(NDATA)
c         endif

C--------------------------------------------------------------------------

C...Count pixels with infinity variances

        IF ((SIG(NDATA) - Y(NDATA))/ABS(Y(NDATA)) .GT. 1000.) 
     +     SUMSIG = SUMSIG + 1
40         CONTINUE

           IF (SUMSIG .GT. MFIT(2)) THEN
                 WRITE(*,1000) IX,SUMSIG
                 CALL TBEDEL(TABOUTID,ROW,COLINT1,STATUS)
                 CALL TBEDEL(TABOUTID,ROW,COLINT2,STATUS)
                 CALL TBEDEL(TABOUTID,ROW,COLERR1,STATUS)
                 CALL TBEDEL(TABOUTID,ROW,COLERR2,STATUS)
                 CALL TBEDEL(TABOUTID,ROW,COLSKY0,STATUS)
           GOTO 50
           ENDIF
1000             FORMAT('               Column ',i4,' with ',i3,
     +            ' undefined pixels. No fit performed')
C
            ENDIF

C
C Parameters for MARQUARDT:



C
              CALL TBERDR(TABID,ROW,COLINT1,A(1),NULL,STATUS)
              CALL TBERDR(TABID,ROW,COLINT2,A(4),NULL,STATUS)
              CALL TBERDR(TABID,ROW,COLPOS1,A(2),NULL,STATUS)
              CALL TBERDR(TABID,ROW,COLDELTA,DELTA,NULL,STATUS)
              CALL TBERDR(TABID,ROW,COLSIGMA,A(3),NULL,STATUS)
              CALL TBERDR(TABID,ROW,COLSKY0,A(6),NULL,STATUS)
C
              A(5) = A(2) - DELTA
C  ---------  A(3) = Common width  -----------
C
              

              ALAMBDA  = -1.
              CHISQOLD = -1.
C
C  Loop for MARQUARDT:
C
c         write(*,*) A(1),A(2),A(3),A(4),A(5),A(6)
c             IF (IX.eq.1000)   write(*,*) A(1),A(2),A(3),A(4),A(5),
c     +A(6)
              DO 47 N = 1,30
                 ALAOLD = ABS(ALAMBDA)

                 CALL MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT(1),COVAR,
     +                       ALPHA,NCA,CHISQ,FUNCS,ALAMBDA)

                 IF (ALAMBDA .GE. ALAOLD) GOTO 48

47            CONTINUE
48            CONTINUE 

c             IF (IX.eq.1000)   write(*,*) A(1),A(2),A(3),A(4),A(5),
c     +A(6)

C
C  Compute parameter errors:
C
              ALAMBDA=0.
              CALL MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT(1),COVAR,
     +                    ALPHA,NCA,CHISQ,FUNCS,ALAMBDA)
       
              ERR1 = SQRT(COVAR(1,1))
              ERR2 = SQRT(COVAR(4,4))

c             IF (IX.eq.1000)   write(*,*) A(1),A(2),A(3),A(4),A(5),
c     +A(6)

C
C  Output parameters in table "startopt":
C
c              DELTA = A(2) - A(5)

              CALL TBEWRR(TABOUTID,ROW,COLINT1,A(1),STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLINT2,A(4),STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLPOS1,A(2),STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLDELTA,DELTA,STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLSIGMA,A(3),STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLSKY0,A(6),STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLERR1,ERR1,STATUS)
              CALL TBEWRR(TABOUTID,ROW,COLERR2,ERR2,STATUS)


C  Parameter to be rebined:

              OUT(IX1,INDX) = ERR2
C  Fitted profile:

             DO 49 J = 1,NDATA

               CALL GAUSS2(X(J),A,YFIT,DYDA,MA)


                 YPIX = X(J)+YVAL
                 OUTFIT(IX1,YPIX) = YFIT

c                 OUTFIT(IX1,YPIX) = A(6)

49            CONTINUE
50            CONTINUE
              CALL TBEWRI(TABOUTID,ROW,COLPIX,SUMSIG,STATUS)      
              XMIN   = AMIN1(XMIN,OUT(IX1,INDX))
              XMAX   = AMAX1(XMAX,OUT(IX1,INDX))
              AX     = AX + 1.D0

500     CONTINUE

      CALL TBTCLO(TABID,STATUS)
      CALL TBTCLO(TABOUTID,STATUS)

      RETURN

      END
C
C****************** Numerical Recipies ***************************************
C
      SUBROUTINE MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,
     *    COVAR,ALPHA,NCA,CHISQ,FUNCS,ALAMDA)
      PARAMETER (MMAX=50)
      DIMENSION X(NDATA),Y(NDATA),SIG(NDATA),A(MA),LISTA(MA),
     *  COVAR(NCA,NCA),ALPHA(NCA,NCA),ATRY(MMAX),BETA(MMAX),DA(MMAX)
      IF(ALAMDA.LT.0.)THEN
        KK=MFIT+1
        DO 12 J=1,MA
          IHIT=0
          DO 11 K=1,MFIT
            IF(LISTA(K).EQ.J)IHIT=IHIT+1
11        CONTINUE
          IF (IHIT.EQ.0) THEN
            LISTA(KK)=J
            KK=KK+1
          ELSE IF (IHIT.GT.1) THEN
c            PAUSE 'Improper permutation in LISTA'
            write(*,*) 'Improper permutation in LISTA'
          ENDIF
12      CONTINUE
        IF (KK.NE.(MA+1)) write(*,*) 'Improper permutation in LISTA'
        ALAMDA=0.001
        CALL MRQCOF(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,ALPHA,BETA,NCA,CHISQ,F
     *UNCS)
        OCHISQ=CHISQ
        DO 13 J=1,MA
          ATRY(J)=A(J)
13      CONTINUE
      ENDIF
      DO 15 J=1,MFIT
        DO 14 K=1,MFIT
          COVAR(J,K)=ALPHA(J,K)
14      CONTINUE
        COVAR(J,J)=ALPHA(J,J)*(1.+ALAMDA)
        DA(J)=BETA(J)
15    CONTINUE
      CALL GAUSSJ(COVAR,MFIT,NCA,DA,1,1)
      IF(ALAMDA.EQ.0.)THEN
        CALL COVSRT(COVAR,NCA,MA,LISTA,MFIT)
        RETURN
      ENDIF
      DO 16 J=1,MFIT
        ATRY(LISTA(J))=A(LISTA(J))+DA(J)
16    CONTINUE
      CALL MRQCOF(X,Y,SIG,NDATA,ATRY,MA,LISTA,MFIT,COVAR,DA,NCA,CHISQ,FU
     *NCS)
      IF(CHISQ.LT.OCHISQ)THEN
        ALAMDA=0.1*ALAMDA
        OCHISQ=CHISQ
        DO 18 J=1,MFIT
          DO 17 K=1,MFIT
            ALPHA(J,K)=COVAR(J,K)
17        CONTINUE
          BETA(J)=DA(J)
          A(LISTA(J))=ATRY(LISTA(J))
18      CONTINUE
      ELSE
        ALAMDA=10.*ALAMDA
        CHISQ=OCHISQ
      ENDIF
      RETURN
      END


C****************************************************************************
      SUBROUTINE MRQCOF(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,ALPHA,BETA,NALP,
     *CHISQ,FUNCS)
      PARAMETER (MMAX=50)
      DIMENSION X(NDATA),Y(NDATA),SIG(NDATA),ALPHA(NALP,NALP),BETA(MA),
     *    DYDA(MMAX),LISTA(MFIT),A(MA)
      DO 12 J=1,MFIT
        DO 11 K=1,J
          ALPHA(J,K)=0.
11      CONTINUE
        BETA(J)=0.
12    CONTINUE
      CHISQ=0.
      DO 15 I=1,NDATA
        CALL GAUSS2(X(I),A,YMOD,DYDA,MA)
        SIG2I=1./(SIG(I)*SIG(I))
        DY=Y(I)-YMOD
        DO 14 J=1,MFIT
          WT=DYDA(LISTA(J))*SIG2I
          DO 13 K=1,J
            ALPHA(J,K)=ALPHA(J,K)+WT*DYDA(LISTA(K))
13        CONTINUE
          BETA(J)=BETA(J)+DY*WT
14      CONTINUE
        CHISQ=CHISQ+DY*DY*SIG2I
15    CONTINUE
      DO 17 J=2,MFIT
        DO 16 K=1,J-1
          ALPHA(K,J)=ALPHA(J,K)
16      CONTINUE
17    CONTINUE

      RETURN
      END

C**************************************************************************

      SUBROUTINE COVSRT(COVAR,NCVM,MA,LISTA,MFIT)
      DIMENSION COVAR(NCVM,NCVM),LISTA(MFIT)
      DO 12 J=1,MA-1
        DO 11 I=J+1,MA
          COVAR(I,J)=0.
11      CONTINUE
12    CONTINUE
      DO 14 I=1,MFIT-1
        DO 13 J=I+1,MFIT
          IF(LISTA(J).GT.LISTA(I)) THEN
            COVAR(LISTA(J),LISTA(I))=COVAR(I,J)
          ELSE
            COVAR(LISTA(I),LISTA(J))=COVAR(I,J)
          ENDIF
13      CONTINUE
14    CONTINUE
      SWAP=COVAR(1,1)
      DO 15 J=1,MA
        COVAR(1,J)=COVAR(J,J)
        COVAR(J,J)=0.
15    CONTINUE
      COVAR(LISTA(1),LISTA(1))=SWAP
      DO 16 J=2,MFIT
        COVAR(LISTA(J),LISTA(J))=COVAR(1,J)
16    CONTINUE
      DO 18 J=2,MA
        DO 17 I=1,J-1
          COVAR(I,J)=COVAR(J,I)
17      CONTINUE
18    CONTINUE
      RETURN
      END

C***************************************************************************
      SUBROUTINE GAUSSJ(A,N,NP,B,M,MP)
      PARAMETER (NMAX=50)
      DIMENSION A(NP,NP),B(NP,MP),IPIV(NMAX),INDXR(NMAX),INDXC(NMAX)
c      integer i,j,irow,icol
      DO 11 J=1,N
        IPIV(J)=0
11    CONTINUE
      DO 22 I=1,N
        BIG=0.
        DO 13 J=1,N
          IF(IPIV(J).NE.1)THEN
            DO 12 K=1,N
              IF (IPIV(K).EQ.0) THEN
                IF (ABS(A(J,K)).GE.BIG)THEN
                  BIG=ABS(A(J,K))
                  IROW=J
                  ICOL=K
                ENDIF
              ELSE IF (IPIV(K).GT.1) THEN
c                PAUSE 'Singular matrix'
                write(*,*) 'Singular matrix'
              ENDIF
12          CONTINUE
          ENDIF
13      CONTINUE
        IPIV(ICOL)=IPIV(ICOL)+1
        IF (IROW.NE.ICOL) THEN
          DO 14 L=1,N
            DUM=A(IROW,L)
            A(IROW,L)=A(ICOL,L)
            A(ICOL,L)=DUM
14        CONTINUE
          DO 15 L=1,M
c            write(*,*) L,IROW
            DUM=B(IROW,L)
            B(IROW,L)=B(ICOL,L)
            B(ICOL,L)=DUM
15        CONTINUE

        ENDIF
        INDXR(I)=IROW
        INDXC(I)=ICOL
        IF (A(ICOL,ICOL).EQ.0.) write(*,*) 'Singular matrix.'
        PIVINV=1./A(ICOL,ICOL)
        A(ICOL,ICOL)=1.
        DO 16 L=1,N
          A(ICOL,L)=A(ICOL,L)*PIVINV
16      CONTINUE
        DO 17 L=1,M
          B(ICOL,L)=B(ICOL,L)*PIVINV
17      CONTINUE
        DO 21 LL=1,N
          IF(LL.NE.ICOL)THEN
            DUM=A(LL,ICOL)
            A(LL,ICOL)=0.
            DO 18 L=1,N
              A(LL,L)=A(LL,L)-A(ICOL,L)*DUM
18          CONTINUE
            DO 19 L=1,M
              B(LL,L)=B(LL,L)-B(ICOL,L)*DUM
19          CONTINUE
          ENDIF
21      CONTINUE
22    CONTINUE
      DO 24 L=N,1,-1
        IF(INDXR(L).NE.INDXC(L))THEN
          DO 23 K=1,N
            DUM=A(K,INDXR(L))
            A(K,INDXR(L))=A(K,INDXC(L))
            A(K,INDXC(L))=DUM
23        CONTINUE
        ENDIF
24    CONTINUE
      RETURN
      END

C***************************************************************************

      SUBROUTINE GAUSS2(X,A,Y,DYDA,NA)
C
C 2 Gaussians with common width (SIGMA) + constant. 
C

      DIMENSION A(NA),DYDA(NA)
      REAL SIGMA,DELTA
      common DELTA
      SIGMA = A(3)
      A(5) = A(2) - DELTA
      Y=0.
      DO 11 I=1,NA-1,3
        ARG=(X-A(I+1))/SIGMA
        EX=EXP(-ARG**2)
        FAC=A(I)*EX*2.*ARG
        Y=Y+A(I)*EX
        DYDA(I)=EX
        DYDA(I+1)=FAC/SIGMA
        DYDA(I+2)=FAC*ARG/SIGMA
11    CONTINUE
C
      Y = Y + A(6)
      DYDA(6) = 1.     

      RETURN
      END
