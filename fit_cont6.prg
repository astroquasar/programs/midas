!  Procedure fit_cont6.prg                   (c) SL 2006
!
!procedimiento que crea table con puntos en continuo ("table_SPEC.tbl")


DEFINE/PARAMETER P1 ? c "ingrese table con espectro: "
DEFINE/PARAMETER P2 ? c "ingrese radio de mediana: "
DEFINE/PARAMETER P3 ? c "ingrese paso: "
!
DEFINE/LOCAL dir/c/1/62 /Users/vincent/prog/midas
DEFINE/LOCAL table/c/1/100 {P1}
DEFINE/LOCAL R/c/1/100 {P2}
DEFINE/LOCAL N/c/1/100 {P3}
!
! median filter and create table out with points 
!
@@ {dir}/median {P1} out ? ? {R}
delete/col out :flux
comp/tab out :x_axis=:wave
comp/tab out :y_axis=:median
delete/col out :wave
delete/col out :median
set/context spec
comp/tab out :aux=seq/{N} - INT(seq/{N})
select/tab out :aux.eq.0
copy/table out table_{P1}
comp/tab table_{P1} :n=seq


@@ {dir}/fitspline1 {P1} table_{P1} :x_axis :y_axis 2
name/col table_{P1} :n ? F5.0

set/graph color=1 stype=0
plot/tab {P1} :wave :flux
set/graph color=2 stype=5
overplot/tab table_{P1} :x_axis :y_axis
set/graph color=3 stype=0
overplot/tab {P1} :wave :fit
set/graph color=4
overplot/ident table_{P1} :x_axis :n

