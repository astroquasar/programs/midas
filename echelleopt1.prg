!
!
!  Procedure echelleopt.prg                   (c) SVCLM May 1995
!
!  Optimal extraction of MIKE echelle orders
!-------------------------------------------------------
!
DEFINE/PAR P1 ? IMAGE    "Enter input image with object:"
DEFINE/PAR P2 ? IMAGE    "Enter input image with variances:"
DEFINE/PAR P3 ? TABLE    "Enter table with input parameters:"
DEFINE/PAR P4 ? TABLE    "Enter table with fitted parameters:"
DEFINE/PAR P5 ? CHAR     "Enter method (S)INGLE/(D)OUBLE:"
DEFINE/PAR P6 ? NUM      "Enter number of parameters to fit:"
!
!
select/tab {P3} ALL
copy/tab {P3} {P4}
!
!set/echelle slit=20 
!
WRITE/KEY IN_A/C/1/60     {P1}
WRITE/KEY IN_VAR/C/1/60   {P2}
WRITE/KEY IN_B/C/1/60     {ORDTAB}
WRITE/KEY IN_PAR/C/1/60   {P3}
WRITE/KEY OUT_PAR/C/1/60  {P4}
WRITE/KEY OUT_A/C/1/60    ext
WRITE/KEY OUT_B/C/1/60    profile1  

WRITE/KEY INPUTC/C/1/60   COEFF
WRITE/KEY INPUTR/R/1/3    {SLIT},0.,{OFFSET}
WRITE/KEY INPUTI/I/1/2    1,{NBORDI}
WRITE/KEY METHOD/C/1/1    {P5}
WRITE/KEY MFIT/I/1/2      {P6},15          
WRITE/KEY display/I/1/3      1,30,552
WRITE/KEY tabtest/C/1/60      example.dat
!
!
!To create table with positions of test slit at x=display(2), order=display(1):
!create/tab example 2 30 example.dat
!load/table example :lab001 :lab002
!
!----------Organization of fit parameters:------------!
!                                                     !
! METHOD DOUBLE:                 METHOD SINGLE:       !
! -------------                  -------------        !
!  LISTA(6) = 3    !common sigma = A(3)               !
!  LISTA(5) = 2    !pos1         = A(2)               !
!  LISTA(1) = 1    !int1         = A(1)               !
!  LISTA(4) = 5    !pos2             --               !
!  LISTA(2) = 4    !int2             --               !
!  LISTA(3) = 6    !sky0         = A(4)               !
!                                                     !
!-----------------------------------------------------!
!
! 
!
  WRITE/KEY LISTA/I/1/6    1,6,3,2,5,4  !int1,sky0,sigma,pos1,pos2,int2
!
RUN /musica2/QbC/procs/echelleoptdelta  
!RUN /musica2/LCOmar04/procs/echelle_mike  

!
!MFIT(1): First MFIT(1) parameters will be actually fitted
!MFIT(2): Max number of allowed "hot pixels" to perform a fit.







