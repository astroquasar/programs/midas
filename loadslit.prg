!Procedure LOADSLIT     Loads slit around shifted echelle orders
!                                                 (c) SEBASTIAN LOPEZ 30.03.95
clear/channel over 
comp/tab order :YPLUS = :YFIT + {OFFSET} + {SLIT}/2.
comp/tab order :YMINUS = :YFIT + {OFFSET} - {SLIT}/2.
!load/tab order.tbl :X :YPLUS :order -1
!load/tab order.tbl :X :YMINUS :order -1
load/tab order.tbl :X :YPLUS :order ? 1
load/tab order.tbl :X :YMINUS :order ? 1