!
! MIDAS procedure  FIT_CONT		                09.02.00 
!								(C)SVCLM
! Steps to fit continuum 
!
WRITE/OUT Procedure FIT_CONT
DEFINE/PARAMETER P1 ?      C   "Enter order:"
!
define/local N/i/1/1 {P1}
define/local ROOT/c/1/11 nothing
define/local ff/r/1/1 10000.
define/local ratio/r/1/1 0.001
set/format I4
!
!goto there

write/out "click zero first, then maximum"
plot/tab  ff{N} :wave :lab002
get/gcurs aux
comp/key ff = {aux.tbl,:y_axis,@2} - {aux.tbl,:y_axis,@1}

write/out "click maximum"
!plot/tab r{N} :wave :flux
plot/tab {N} :wave :fluxA
!plot/tab {N} :wave :fluxB
get/gcurs aux1
comp/key ratio = {aux1.tbl,:y_axis,@1}/{ff}
comp/tab ff{N} :test = (:lab002 - {aux.tbl,:y_axis,@1})* {ratio}
overplot/tab ff{N} :wave :test

there:

!get/gcurs table_{ROOT(8:10)}_{N} 
!@@ /canopus/sebastian/prg/fitspline1 {ROOT}{N} table_{ROOT(8:10)}_{N} ? ? 0,0
!set/graph color=2
!overplot/tab {ROOT}{N} :wave :fit
!set/graph color=1
!comp/tab {ROOT}{N} :normflux=:flux/:fit
!comp/tab {ROOT}{N} :stdev=:sigma/:fit
