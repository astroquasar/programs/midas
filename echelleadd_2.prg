!
! MIDAS procedure  ECHELLEADD_2243a		                03.11.00 
!								(C)SVCLM
! same as ecehlleadd_2243 but for 3 spectra instead of 4.
! Coadd orders. Flux
! is weighted with inverse of variances.
!
WRITE/OUT Procedure ECHELLEADD
DEFINE/PARAMETER P1 ?      C   "Enter root name 1:"
DEFINE/PARAMETER P2 ?      C   "Enter root name 2:"
DEFINE/PARAMETER P3 ?      C   "Enter output root name:"
DEFINE/PARAMETER P4 ?      C   "Enter order1,order2:"
!
define/local N/i/1/1 1
define/local ORDER/i/1/2 {P4}
set/format I4
!
DO N = {ORDER(1)} {ORDER(2)}
!
write/out Now working on order 'N'
@@ /crater/prg_linux/combi_2 {P1}{N} {P2}{N} {P3}{N} 
!
ENDDO
