
*PROGRAMM PLINES		03.02.01
*------------------ 		(C)	SVCLM
*
* Display absorption profiles in velocity space.
*
*
* Assumptions:
*
*  1. Columns ':wave', ':normflux' in table with displayed data.
*  Up to 20 lines can be displayed


       PROGRAM PLINES
       IMPLICIT NONE

c For data:
      real X(300000),Y(300000),V(300000),YNEW(300000)

c General:
      integer numcol,numrow,nsc,arow,lonull,acol,nlines
      integer tiddata1,tiddata2,acts,kun,knul,i,j,k
      character*40 tabname1,tabname2,tabname3,col1,col2
      logical selflag

c For graphic:
      integer access,plmode,key,status,maxvals,actvals,lines(20)
      integer stype,ltype,bin,yoff,ssize
      real xcur,ycur,x1,x2

c codes:
      real  wave(3000),z,range(2),liney(2),xaxis(4),yaxis(4)
      integer ipos,yc,xc
      character*15 text
      character*9 ion(3000)

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'


      DATA      access/1/
      DATA      maxvals/20/
      DATA      plmode/-1/
      DATA      bin/1/
      DATA      STYPE/1/
      DATA      YOFF/0.0/
      DATA      npix/2/      
      DATA      pixsize/0.04/      

      DATA      wave(   1)/   228.3010/
      DATA      wave(   2)/   228.6280/
      DATA      wave(   3)/   228.8250/
      DATA      wave(   4)/   228.8520/
      DATA      wave(   5)/   228.8760/
      DATA      wave(   6)/   228.9090/
      DATA      wave(   7)/   228.9160/
      DATA      wave(   8)/   229.4310/
      DATA      wave(   9)/   229.7340/
      DATA      wave(  10)/   229.7360/
      DATA      wave(  11)/   229.8690/
      DATA      wave(  12)/   229.9900/
      DATA      wave(  13)/   230.0890/
      DATA      wave(  17)/   230.1390/
      DATA      wave(  18)/   230.4950/
      DATA      wave(  19)/   230.6860/
      DATA      wave(  20)/   230.7650/
      DATA      wave(  21)/   230.7890/
      DATA      wave(  22)/   231.4000/
      DATA      wave(  23)/   231.4540/
      DATA      wave(  24)/   231.5200/
      DATA      wave(  25)/   231.6930/
      DATA      wave(  26)/   231.7333/
      DATA      wave(  27)/   231.8840/
      DATA      wave(  28)/   232.0470/
      DATA      wave(  29)/   232.4420/
      DATA      wave(  30)/   232.5840/
      DATA      wave(  33)/   233.5990/
      DATA      wave(  34)/   233.6200/
      DATA      wave(  35)/   233.7620/
      DATA      wave(  36)/   234.1530/
      DATA      wave(  38)/   234.2500/
      DATA      wave(  39)/   234.2631/
      DATA      wave(  40)/   234.3470/
      DATA      wave(  41)/   234.3560/
      DATA      wave(  42)/   234.4100/
      DATA      wave(  43)/   234.8400/
      DATA      wave(  44)/   235.2210/
      DATA      wave(  46)/   236.2700/
      DATA      wave(  47)/   237.3310/
      DATA      wave(  48)/   237.4200/
      DATA      wave(  50)/   237.7000/
      DATA      wave(  51)/   237.9940/
      DATA      wave(  52)/   238.0340/
      DATA      wave(  53)/   238.1000/
      DATA      wave(  54)/   238.3600/
      DATA      wave(  56)/   238.6170/
      DATA      wave(  57)/   239.4200/
      DATA      wave(  58)/   239.5080/
      DATA      wave(  59)/   239.8100/
      DATA      wave(  61)/   240.2200/
      DATA      wave(  64)/   240.7130/
      DATA      wave(  67)/   242.7600/
      DATA      wave(  69)/   243.0270/
      DATA      wave(  71)/   243.7400/
      DATA      wave(  73)/   244.4600/
      DATA      wave(  74)/   244.5600/
      DATA      wave(  75)/   244.7000/
      DATA      wave(  76)/   244.8900/
      DATA      wave(  77)/   244.9070/
      DATA      wave(  78)/   244.9110/
      DATA      wave(  79)/   244.9120/
      DATA      wave(  81)/   246.0040/
      DATA      wave(  82)/   247.1900/
      DATA      wave(  83)/   247.2050/
      DATA      wave(  84)/   247.4010/
      DATA      wave(  85)/   247.4500/
      DATA      wave(  86)/   247.4730/
      DATA      wave(  87)/   247.6500/
      DATA      wave(  88)/   247.9500/
      DATA      wave(  89)/   248.3200/
      DATA      wave(  90)/   248.3710/
      DATA      wave(  91)/   248.5380/
      DATA      wave(  93)/   248.9900/
      DATA      wave(  94)/   249.1890/
      DATA      wave(  95)/   249.2700/
      DATA      wave(  97)/   250.1530/
      DATA      wave(  98)/   250.4820/
      DATA      wave(  99)/   250.5120/
      DATA      wave( 100)/   250.5160/
      DATA      wave( 102)/   251.1200/
      DATA      wave( 103)/   251.1300/
      DATA      wave( 104)/   251.1400/
      DATA      wave( 105)/   251.5840/
      DATA      wave( 107)/   252.1900/
      DATA      wave( 108)/   252.4400/
      DATA      wave( 109)/   252.9580/
      DATA      wave( 110)/   253.7720/
      DATA      wave( 111)/   255.0900/
      DATA      wave( 112)/   255.1550/
      DATA      wave( 113)/   255.5870/
      DATA      wave( 116)/   255.7700/
      DATA      wave( 117)/   255.8630/
      DATA      wave( 118)/   255.8800/
      DATA      wave( 119)/   256.3170/
      DATA      wave( 120)/   256.3700/
      DATA      wave( 121)/   256.6600/
      DATA      wave( 122)/   257.1600/
      DATA      wave( 123)/   257.3850/
      DATA      wave( 124)/   257.4300/
      DATA      wave( 125)/   257.9760/
      DATA      wave( 126)/   258.2510/
      DATA      wave( 129)/   259.2960/
      DATA      wave( 131)/   259.5200/
      DATA      wave( 132)/   262.6620/
      DATA      wave( 133)/   263.2200/
      DATA      wave( 134)/   263.2460/
      DATA      wave( 136)/   263.6920/
      DATA      wave( 137)/   263.7600/
      DATA      wave( 138)/   264.0780/
      DATA      wave( 139)/   264.2400/
      DATA      wave( 140)/   264.2570/
      DATA      wave( 141)/   264.7800/
      DATA      wave( 142)/   264.8220/
      DATA      wave( 143)/   264.8460/
      DATA      wave( 144)/   265.3100/
      DATA      wave( 145)/   265.7400/
      DATA      wave( 146)/   266.8050/
      DATA      wave( 147)/   266.8470/
      DATA      wave( 148)/   266.8960/
      DATA      wave( 149)/   266.8960/
      DATA      wave( 150)/   266.9670/
      DATA      wave( 151)/   267.0500/
      DATA      wave( 152)/   267.0700/
      DATA      wave( 153)/   267.4010/
      DATA      wave( 155)/   267.6450/
      DATA      wave( 156)/   268.1780/
      DATA      wave( 159)/   268.3470/
      DATA      wave( 160)/   269.0720/
      DATA      wave( 161)/   269.5330/
      DATA      wave( 162)/   270.0730/
      DATA      wave( 163)/   270.3050/
      DATA      wave( 164)/   270.3240/
      DATA      wave( 165)/   270.4940/
      DATA      wave( 167)/   271.0770/
      DATA      wave( 168)/   271.9950/
      DATA      wave( 169)/   272.0900/
      DATA      wave( 170)/   272.5230/
      DATA      wave( 171)/   272.6410/
      DATA      wave( 172)/   273.6950/
      DATA      wave( 173)/   273.9770/
      DATA      wave( 174)/   274.0510/
      DATA      wave( 175)/   274.2030/
      DATA      wave( 176)/   274.4110/
      DATA      wave( 177)/   274.4610/
      DATA      wave( 178)/   275.2810/
      DATA      wave( 179)/   275.3520/
      DATA      wave( 180)/   276.1450/
      DATA      wave( 181)/   276.1930/
      DATA      wave( 182)/   276.3640/
      DATA      wave( 183)/   277.2320/
      DATA      wave( 184)/   277.4200/
      DATA      wave( 186)/   277.9470/
      DATA      wave( 187)/   278.2440/
      DATA      wave( 188)/   278.2600/
      DATA      wave( 189)/   278.4360/
      DATA      wave( 190)/   278.6940/
      DATA      wave( 191)/   279.6310/
      DATA      wave( 192)/   279.7330/
      DATA      wave( 194)/   280.0430/
      DATA      wave( 195)/   280.1140/
      DATA      wave( 196)/   280.1410/
      DATA      wave( 197)/   280.1430/
      DATA      wave( 198)/   280.2340/
      DATA      wave( 200)/   281.4000/
      DATA      wave( 201)/   282.0700/
      DATA      wave( 203)/   282.4070/
      DATA      wave( 204)/   283.1300/
      DATA      wave( 205)/   283.1460/
      DATA      wave( 206)/   283.1670/
      DATA      wave( 207)/   283.2600/
      DATA      wave( 210)/   284.1470/
      DATA      wave( 211)/   284.9780/
      DATA      wave( 213)/   285.3700/
      DATA      wave( 215)/   285.8550/
      DATA      wave( 216)/   286.0940/
      DATA      wave( 218)/   288.4230/
      DATA      wave( 219)/   288.4500/
      DATA      wave( 220)/   289.1650/
      DATA      wave( 221)/   289.4900/
      DATA      wave( 222)/   289.5790/
      DATA      wave( 223)/   289.8510/
      DATA      wave( 224)/   290.4670/
      DATA      wave( 225)/   290.6300/
      DATA      wave( 227)/   291.0200/
      DATA      wave( 228)/   291.3261/
      DATA      wave( 229)/   291.3650/
      DATA      wave( 230)/   291.9850/
      DATA      wave( 231)/   292.1500/
      DATA      wave( 232)/   292.4470/
      DATA      wave( 237)/   293.8200/
      DATA      wave( 238)/   294.5160/
      DATA      wave( 239)/   294.7700/
      DATA      wave( 240)/   294.9500/
      DATA      wave( 241)/   295.1690/
      DATA      wave( 242)/   295.3210/
      DATA      wave( 243)/   295.6340/
      DATA      wave( 246)/   296.5540/
      DATA      wave( 247)/   296.9580/
      DATA      wave( 248)/   298.1500/
      DATA      wave( 251)/   299.6610/
      DATA      wave( 252)/   300.0800/
      DATA      wave( 253)/   300.1530/
      DATA      wave( 254)/   300.5170/
      DATA      wave( 255)/   300.5310/
      DATA      wave( 256)/   300.5600/
      DATA      wave( 257)/   301.4360/
      DATA      wave( 258)/   301.7410/
      DATA      wave( 260)/   302.2150/
      DATA      wave( 261)/   302.2630/
      DATA      wave( 262)/   303.3100/
      DATA      wave( 263)/   303.3180/
      DATA      wave( 264)/   303.3200/
      DATA      wave( 265)/   303.4110/
      DATA      wave( 266)/   303.7822/
      DATA      wave( 267)/   304.8500/
      DATA      wave( 268)/   305.5960/
      DATA      wave( 269)/   305.7610/
      DATA      wave( 270)/   307.2490/
      DATA      wave( 272)/   308.8530/
      DATA      wave( 274)/   309.2000/
      DATA      wave( 275)/   309.5970/
      DATA      wave( 277)/   310.1697/
      DATA      wave( 278)/   310.1820/
      DATA      wave( 279)/   310.2340/
      DATA      wave( 280)/   310.2570/
      DATA      wave( 281)/   310.6900/
      DATA      wave( 282)/   311.5500/
      DATA      wave( 283)/   311.8060/
      DATA      wave( 284)/   311.9210/
      DATA      wave( 285)/   312.4220/
      DATA      wave( 286)/   312.4530/
      DATA      wave( 287)/   312.5420/
      DATA      wave( 288)/   313.0590/
      DATA      wave( 289)/   313.5390/
      DATA      wave( 290)/   313.7430/
      DATA      wave( 291)/   314.3100/
      DATA      wave( 292)/   314.7150/
      DATA      wave( 295)/   315.6500/
      DATA      wave( 297)/   316.2020/
      DATA      wave( 298)/   316.5300/
      DATA      wave( 299)/   318.0940/
      DATA      wave( 300)/   318.3850/
      DATA      wave( 301)/   318.4520/
      DATA      wave( 303)/   318.8250/
      DATA      wave( 305)/   318.8900/
      DATA      wave( 306)/   319.8290/
      DATA      wave( 307)/   320.5580/
      DATA      wave( 309)/   320.9943/
      DATA      wave( 311)/   322.5741/
      DATA      wave( 312)/   322.7570/
      DATA      wave( 314)/   323.4360/
      DATA      wave( 315)/   323.4930/
      DATA      wave( 316)/   323.5200/
      DATA      wave( 317)/   324.4770/
      DATA      wave( 318)/   324.5680/
      DATA      wave( 319)/   324.5700/
      DATA      wave( 324)/   326.5170/
      DATA      wave( 325)/   326.5420/
      DATA      wave( 326)/   326.7770/
      DATA      wave( 327)/   326.7870/
      DATA      wave( 328)/   327.1370/
      DATA      wave( 329)/   327.1810/
      DATA      wave( 330)/   327.2440/
      DATA      wave( 331)/   327.2620/
      DATA      wave( 332)/   328.2900/
      DATA      wave( 334)/   328.4700/
      DATA      wave( 335)/   328.4720/
      DATA      wave( 336)/   328.7750/
      DATA      wave( 337)/   328.9750/
      DATA      wave( 338)/   329.1100/
      DATA      wave( 340)/   329.2800/
      DATA      wave( 341)/   329.3710/
      DATA      wave( 342)/   329.7740/
      DATA      wave( 343)/   330.2160/
      DATA      wave( 344)/   330.6600/
      DATA      wave( 345)/   330.7900/
      DATA      wave( 346)/   331.4340/
      DATA      wave( 348)/   331.9860/
      DATA      wave( 349)/   332.1400/
      DATA      wave( 350)/   332.7830/
      DATA      wave( 351)/   333.4400/
      DATA      wave( 352)/   333.8570/
      DATA      wave( 354)/   334.1400/
      DATA      wave( 355)/   334.1780/
      DATA      wave( 356)/   335.2300/
      DATA      wave( 357)/   335.3300/
      DATA      wave( 358)/   335.3440/
      DATA      wave( 359)/   335.4070/
      DATA      wave( 362)/   337.5600/
      DATA      wave( 363)/   338.8200/
      DATA      wave( 364)/   338.8220/
      DATA      wave( 365)/   338.8240/
      DATA      wave( 366)/   339.4940/
      DATA      wave( 367)/   340.2820/
      DATA      wave( 368)/   340.3890/
      DATA      wave( 369)/   341.1130/
      DATA      wave( 370)/   341.2910/
      DATA      wave( 371)/   341.9500/
      DATA      wave( 372)/   344.7720/
      DATA      wave( 373)/   345.4480/
      DATA      wave( 374)/   345.7230/
      DATA      wave( 377)/   346.8520/
      DATA      wave( 378)/   347.0210/
      DATA      wave( 379)/   347.4170/
      DATA      wave( 380)/   348.1840/
      DATA      wave( 384)/   349.6390/
      DATA      wave( 385)/   349.7950/
      DATA      wave( 386)/   350.6100/
      DATA      wave( 387)/   350.6450/
      DATA      wave( 388)/   351.0880/
      DATA      wave( 389)/   352.1070/
      DATA      wave( 390)/   352.1450/
      DATA      wave( 391)/   352.2466/
      DATA      wave( 392)/   352.2750/
      DATA      wave( 393)/   352.6610/
      DATA      wave( 394)/   352.9561/
      DATA      wave( 395)/   353.0910/
      DATA      wave( 398)/   353.7690/
      DATA      wave( 399)/   353.9200/
      DATA      wave( 400)/   354.1650/
      DATA      wave( 401)/   354.4850/
      DATA      wave( 402)/   354.6100/
      DATA      wave( 403)/   354.9622/
      DATA      wave( 404)/   355.1100/
      DATA      wave( 405)/   355.4541/
      DATA      wave( 406)/   355.8150/
      DATA      wave( 407)/   356.5405/
      DATA      wave( 408)/   356.7995/
      DATA      wave( 409)/   356.8800/
      DATA      wave( 410)/   357.9500/
      DATA      wave( 411)/   357.9690/
      DATA      wave( 412)/   359.2030/
      DATA      wave( 413)/   360.7980/
      DATA      wave( 414)/   360.9700/
      DATA      wave( 415)/   361.4326/
      DATA      wave( 416)/   361.5600/
      DATA      wave( 417)/   361.5700/
      DATA      wave( 418)/   361.6590/
      DATA      wave( 419)/   363.4440/
      DATA      wave( 420)/   363.7700/
      DATA      wave( 421)/   364.4680/
      DATA      wave( 422)/   365.4400/
      DATA      wave( 423)/   365.6560/
      DATA      wave( 424)/   366.7000/
      DATA      wave( 425)/   367.3080/
      DATA      wave( 426)/   367.3930/
      DATA      wave( 428)/   367.5550/
      DATA      wave( 429)/   368.0710/
      DATA      wave( 430)/   368.0910/
      DATA      wave( 431)/   368.3130/
      DATA      wave( 432)/   368.9900/
      DATA      wave( 433)/   369.6460/
      DATA      wave( 435)/   370.4020/
      DATA      wave( 444)/   371.7290/
      DATA      wave( 445)/   371.7460/
      DATA      wave( 447)/   372.0750/
      DATA      wave( 448)/   374.0050/
      DATA      wave( 449)/   374.2040/
      DATA      wave( 459)/   376.6930/
      DATA      wave( 460)/   376.7450/
      DATA      wave( 461)/   376.7780/
      DATA      wave( 463)/   377.1540/
      DATA      wave( 464)/   378.1350/
      DATA      wave( 465)/   378.1360/
      DATA      wave( 466)/   378.2200/
      DATA      wave( 470)/   381.1500/
      DATA      wave( 471)/   382.1420/
      DATA      wave( 472)/   383.0360/
      DATA      wave( 473)/   383.9600/
      DATA      wave( 474)/   384.7400/
      DATA      wave( 475)/   384.9500/
      DATA      wave( 476)/   385.4600/
      DATA      wave( 478)/   386.1400/
      DATA      wave( 479)/   386.2028/
      DATA      wave( 480)/   386.2610/
      DATA      wave( 481)/   386.2700/
      DATA      wave( 482)/   387.6510/
      DATA      wave( 483)/   387.9010/
      DATA      wave( 484)/   387.9840/
      DATA      wave( 485)/   388.0580/
      DATA      wave( 486)/   388.3180/
      DATA      wave( 488)/   389.1400/
      DATA      wave( 489)/   389.8100/
      DATA      wave( 490)/   390.1500/
      DATA      wave( 494)/   391.9120/
      DATA      wave( 495)/   391.9430/
      DATA      wave( 496)/   392.0020/
      DATA      wave( 497)/   393.0000/
      DATA      wave( 502)/   393.7430/
      DATA      wave( 503)/   395.9190/
      DATA      wave( 505)/   396.3690/
      DATA      wave( 506)/   396.4220/
      DATA      wave( 507)/   396.8700/
      DATA      wave( 508)/   398.5500/
      DATA      wave( 509)/   399.2890/
      DATA      wave( 510)/   399.6300/
      DATA      wave( 511)/   399.8200/
      DATA      wave( 512)/   400.6760/
      DATA      wave( 513)/   401.1400/
      DATA      wave( 515)/   403.3150/
      DATA      wave( 516)/   403.7240/
      DATA      wave( 517)/   405.8480/
      DATA      wave( 518)/   405.8538/
      DATA      wave( 519)/   407.7640/
      DATA      wave( 520)/   408.3780/
      DATA      wave( 521)/   408.6836/
      DATA      wave( 522)/   410.3715/
      DATA      wave( 523)/   411.1450/
      DATA      wave( 524)/   411.6550/
      DATA      wave( 526)/   411.9900/
      DATA      wave( 527)/   412.6290/
      DATA      wave( 528)/   413.1120/
      DATA      wave( 529)/   414.3700/
      DATA      wave( 530)/   415.5000/
      DATA      wave( 532)/   416.6900/
      DATA      wave( 533)/   417.2400/
      DATA      wave( 534)/   417.6100/
      DATA      wave( 535)/   418.2900/
      DATA      wave( 536)/   418.5980/
      DATA      wave( 537)/   418.8810/
      DATA      wave( 538)/   419.0650/
      DATA      wave( 539)/   419.6450/
      DATA      wave( 541)/   422.7850/
      DATA      wave( 543)/   423.4870/
      DATA      wave( 544)/   423.8170/
      DATA      wave( 546)/   426.4020/
      DATA      wave( 547)/   426.6440/
      DATA      wave( 548)/   426.8220/
      DATA      wave( 549)/   427.5510/
      DATA      wave( 550)/   427.9790/
      DATA      wave( 551)/   428.0940/
      DATA      wave( 552)/   428.6920/
      DATA      wave( 553)/   429.1340/
      DATA      wave( 555)/   429.5000/
      DATA      wave( 556)/   429.9180/
      DATA      wave( 557)/   430.0410/
      DATA      wave( 558)/   430.1770/
      DATA      wave( 559)/   430.4670/
      DATA      wave( 560)/   430.9140/
      DATA      wave( 563)/   431.2590/
      DATA      wave( 564)/   431.4550/
      DATA      wave( 566)/   432.8690/
      DATA      wave( 568)/   433.1760/
      DATA      wave( 569)/   433.5670/
      DATA      wave( 571)/   434.5590/
      DATA      wave( 572)/   434.6710/
      DATA      wave( 573)/   435.3090/
      DATA      wave( 574)/   435.6360/
      DATA      wave( 576)/   436.1072/
      DATA      wave( 577)/   437.2550/
      DATA      wave( 578)/   437.6550/
      DATA      wave( 580)/   438.7730/
      DATA      wave( 581)/   438.8240/
      DATA      wave( 582)/   439.6030/
      DATA      wave( 583)/   439.6910/
      DATA      wave( 584)/   441.5990/
      DATA      wave( 585)/   442.0520/
      DATA      wave( 587)/   445.0397/
      DATA      wave( 588)/   445.7700/
      DATA      wave( 589)/   445.9970/
      DATA      wave( 590)/   446.2556/
      DATA      wave( 592)/   448.2921/
      DATA      wave( 593)/   448.9396/
      DATA      wave( 595)/   451.2000/
      DATA      wave( 596)/   451.8690/
      DATA      wave( 597)/   451.8700/
      DATA      wave( 600)/   452.6390/
      DATA      wave( 601)/   452.9100/
      DATA      wave( 602)/   453.2370/
      DATA      wave( 606)/   455.6390/
      DATA      wave( 607)/   455.6700/
      DATA      wave( 608)/   455.7560/
      DATA      wave( 611)/   457.4800/
      DATA      wave( 612)/   457.8180/
      DATA      wave( 613)/   458.1200/
      DATA      wave( 614)/   458.1550/
      DATA      wave( 616)/   459.8970/
      DATA      wave( 617)/   460.7284/
      DATA      wave( 618)/   460.7410/
      DATA      wave( 619)/   461.0510/
      DATA      wave( 620)/   461.7110/
      DATA      wave( 621)/   463.2630/
      DATA      wave( 622)/   464.3670/
      DATA      wave( 623)/   464.9740/
      DATA      wave( 624)/   465.2210/
      DATA      wave( 625)/   465.3740/
      DATA      wave( 626)/   465.8365/
      DATA      wave( 627)/   466.1290/
      DATA      wave( 628)/   466.2400/
      DATA      wave( 629)/   466.3520/
      DATA      wave( 630)/   466.4070/
      DATA      wave( 631)/   466.5300/
      DATA      wave( 632)/   466.7373/
      DATA      wave( 634)/   467.3990/
      DATA      wave( 635)/   469.6301/
      DATA      wave( 636)/   469.6330/
      DATA      wave( 637)/   471.5740/
      DATA      wave( 641)/   473.0280/
      DATA      wave( 642)/   473.9220/
      DATA      wave( 643)/   474.4909/
      DATA      wave( 645)/   475.6481/
      DATA      wave( 646)/   475.9054/
      DATA      wave( 647)/   476.3510/
      DATA      wave( 648)/   476.4360/
      DATA      wave( 649)/   477.1050/
      DATA      wave( 650)/   477.5240/
      DATA      wave( 651)/   479.0060/
      DATA      wave( 652)/   479.8810/
      DATA      wave( 653)/   480.1930/
      DATA      wave( 654)/   480.4100/
      DATA      wave( 655)/   480.4200/
      DATA      wave( 656)/   480.5330/
      DATA      wave( 658)/   481.8540/
      DATA      wave( 660)/   482.4550/
      DATA      wave( 661)/   482.4670/
      DATA      wave( 662)/   482.4790/
      DATA      wave( 663)/   482.5520/
      DATA      wave( 664)/   484.1720/
      DATA      wave( 665)/   486.0680/
      DATA      wave( 666)/   486.2110/
      DATA      wave( 667)/   486.7400/
      DATA      wave( 668)/   486.8839/
      DATA      wave( 669)/   486.9124/
      DATA      wave( 671)/   487.2272/
      DATA      wave( 672)/   487.2780/
      DATA      wave( 676)/   488.1080/
      DATA      wave( 677)/   488.4510/
      DATA      wave( 678)/   488.7926/
      DATA      wave( 679)/   488.9620/
      DATA      wave( 680)/   489.1953/
      DATA      wave( 681)/   489.5050/
      DATA      wave( 682)/   489.5800/
      DATA      wave( 684)/   491.4400/
      DATA      wave( 685)/   494.5590/
      DATA      wave( 686)/   497.0000/
      DATA      wave( 687)/   497.1010/
      DATA      wave( 688)/   498.4460/
      DATA      wave( 689)/   499.4000/
      DATA      wave( 690)/   499.4790/
      DATA      wave( 691)/   499.9192/
      DATA      wave( 694)/   500.8016/
      DATA      wave( 695)/   501.1897/
      DATA      wave( 697)/   501.8510/
      DATA      wave( 698)/   501.9760/
      DATA      wave( 699)/   502.1600/
      DATA      wave( 701)/   504.8010/
      DATA      wave( 704)/   506.2000/
      DATA      wave( 705)/   506.5702/
      DATA      wave( 706)/   507.0576/
      DATA      wave( 707)/   507.3910/
      DATA      wave( 708)/   507.7178/
      DATA      wave( 710)/   508.4390/
      DATA      wave( 711)/   508.5750/
      DATA      wave( 712)/   508.6070/
      DATA      wave( 713)/   508.6431/
      DATA      wave( 714)/   508.6680/
      DATA      wave( 715)/   508.6690/
      DATA      wave( 716)/   509.9979/
      DATA      wave( 717)/   510.5509/
      DATA      wave( 718)/   510.5564/
      DATA      wave( 719)/   511.1384/
      DATA      wave( 720)/   511.1907/
      DATA      wave( 721)/   512.0982/
      DATA      wave( 722)/   514.2060/
      DATA      wave( 724)/   515.6165/
      DATA      wave( 727)/   518.9088/
      DATA      wave( 728)/   519.3269/
      DATA      wave( 729)/   519.3820/
      DATA      wave( 730)/   520.6700/
      DATA      wave( 732)/   522.0940/
      DATA      wave( 733)/   522.2128/
      DATA      wave( 736)/   524.6803/
      DATA      wave( 737)/   525.6890/
      DATA      wave( 738)/   526.2930/
      DATA      wave( 739)/   526.4969/
      DATA      wave( 740)/   526.6340/
      DATA      wave( 741)/   529.3572/
      DATA      wave( 742)/   529.8960/
      DATA      wave( 743)/   530.2740/
      DATA      wave( 744)/   530.4550/
      DATA      wave( 746)/   532.4100/
      DATA      wave( 747)/   533.5099/
      DATA      wave( 749)/   535.2070/
      DATA      wave( 750)/   537.0296/
      DATA      wave( 751)/   538.4060/
      DATA      wave( 752)/   539.0855/
      DATA      wave( 753)/   539.3960/
      DATA      wave( 754)/   539.5489/
      DATA      wave( 755)/   539.8544/
      DATA      wave( 756)/   541.1270/
      DATA      wave( 757)/   541.1820/
      DATA      wave( 758)/   541.1860/
      DATA      wave( 759)/   541.1880/
      DATA      wave( 760)/   542.0730/
      DATA      wave( 761)/   543.2032/
      DATA      wave( 762)/   543.2570/
      DATA      wave( 763)/   543.7305/
      DATA      wave( 764)/   543.8910/
      DATA      wave( 765)/   544.7300/
      DATA      wave( 766)/   546.1768/
      DATA      wave( 769)/   548.9100/
      DATA      wave( 770)/   549.3195/
      DATA      wave( 771)/   549.3785/
      DATA      wave( 772)/   550.0300/
      DATA      wave( 773)/   551.1700/
      DATA      wave( 774)/   551.6800/
      DATA      wave( 775)/   553.3300/
      DATA      wave( 776)/   553.4650/
      DATA      wave( 777)/   554.0750/
      DATA      wave( 781)/   557.7630/
      DATA      wave( 783)/   558.5900/
      DATA      wave( 784)/   558.7550/
      DATA      wave( 785)/   558.9240/
      DATA      wave( 786)/   559.1310/
      DATA      wave( 787)/   560.2394/
      DATA      wave( 788)/   560.3173/
      DATA      wave( 789)/   560.4331/
      DATA      wave( 791)/   566.6130/
      DATA      wave( 792)/   568.1500/
      DATA      wave( 793)/   568.4200/
      DATA      wave( 795)/   570.6400/
      DATA      wave( 796)/   571.1560/
      DATA      wave( 798)/   571.3640/
      DATA      wave( 799)/   571.7790/
      DATA      wave( 800)/   572.0136/
      DATA      wave( 802)/   573.3619/
      DATA      wave( 803)/   574.0100/
      DATA      wave( 804)/   574.3970/
      DATA      wave( 805)/   576.0570/
      DATA      wave( 806)/   576.8748/
      DATA      wave( 807)/   576.9780/
      DATA      wave( 808)/   578.6043/
      DATA      wave( 809)/   580.2631/
      DATA      wave( 810)/   580.8500/
      DATA      wave( 811)/   582.8450/
      DATA      wave( 812)/   584.3340/
      DATA      wave( 813)/   585.7500/
      DATA      wave( 815)/   588.8400/
      DATA      wave( 816)/   588.9200/
      DATA      wave( 817)/   593.5070/
      DATA      wave( 818)/   593.8350/
      DATA      wave( 819)/   594.0720/
      DATA      wave( 820)/   594.4750/
      DATA      wave( 821)/   594.8000/
      DATA      wave( 822)/   597.7000/
      DATA      wave( 823)/   600.6610/
      DATA      wave( 824)/   602.4460/
      DATA      wave( 825)/   603.4300/
      DATA      wave( 827)/   606.8040/
      DATA      wave( 830)/   608.3980/
      DATA      wave( 831)/   609.7900/
      DATA      wave( 832)/   615.6283/
      DATA      wave( 834)/   618.6716/
      DATA      wave( 835)/   619.1023/
      DATA      wave( 836)/   621.4520/
      DATA      wave( 837)/   624.3850/
      DATA      wave( 838)/   624.9500/
      DATA      wave( 839)/   626.8232/
      DATA      wave( 841)/   629.6020/
      DATA      wave( 842)/   629.7300/
      DATA      wave( 843)/   629.7388/
      DATA      wave( 844)/   633.8440/
      DATA      wave( 845)/   635.9945/
      DATA      wave( 846)/   636.3250/
      DATA      wave( 849)/   637.2880/
      DATA      wave( 850)/   637.9170/
      DATA      wave( 852)/   640.4120/
      DATA      wave( 853)/   640.9020/
      DATA      wave( 854)/   641.7670/
      DATA      wave( 855)/   641.9040/
      DATA      wave( 856)/   644.6337/
      DATA      wave( 857)/   646.5340/
      DATA      wave( 858)/   654.0290/
      DATA      wave( 859)/   656.0010/
      DATA      wave( 861)/   656.3290/
      DATA      wave( 862)/   656.8690/
      DATA      wave( 863)/   657.3400/
      DATA      wave( 864)/   658.3980/
      DATA      wave( 865)/   661.8689/
      DATA      wave( 866)/   662.2670/
      DATA      wave( 867)/   664.3150/
      DATA      wave( 868)/   664.5622/
      DATA      wave( 869)/   665.5190/
      DATA      wave( 871)/   671.3580/
      DATA      wave( 872)/   671.4102/
      DATA      wave( 873)/   671.8513/
      DATA      wave( 874)/   672.8562/
      DATA      wave( 876)/   676.1190/
      DATA      wave( 877)/   677.7460/
      DATA      wave( 878)/   679.0240/
      DATA      wave( 880)/   681.4700/
      DATA      wave( 881)/   681.7200/
      DATA      wave( 884)/   683.5290/
      DATA      wave( 885)/   684.9960/
      DATA      wave( 886)/   685.5130/
      DATA      wave( 888)/   687.0526/
      DATA      wave( 889)/   689.9360/
      DATA      wave( 892)/   694.2700/
      DATA      wave( 893)/   695.8289/
      DATA      wave( 894)/   696.2170/
      DATA      wave( 895)/   698.7310/
      DATA      wave( 896)/   699.4890/
      DATA      wave( 897)/   700.2450/
      DATA      wave( 898)/   702.3320/
      DATA      wave( 899)/   705.3640/
      DATA      wave( 901)/   710.5260/
      DATA      wave( 902)/   713.8120/
      DATA      wave( 903)/   714.6950/
      DATA      wave( 904)/   718.0898/
      DATA      wave( 905)/   720.4400/
      DATA      wave( 906)/   723.3605/
      DATA      wave( 907)/   724.2890/
      DATA      wave( 908)/   724.4410/
      DATA      wave( 912)/   727.6820/
      DATA      wave( 913)/   728.8120/
      DATA      wave( 915)/   731.8690/
      DATA      wave( 918)/   734.2960/
      DATA      wave( 922)/   735.2470/
      DATA      wave( 923)/   735.3440/
      DATA      wave( 925)/   735.8962/
      DATA      wave( 926)/   737.1460/
      DATA      wave( 928)/   737.6690/
      DATA      wave( 929)/   737.6830/
      DATA      wave( 930)/   739.0710/
      DATA      wave( 931)/   739.0850/
      DATA      wave( 932)/   740.2691/
      DATA      wave( 933)/   741.0310/
      DATA      wave( 934)/   741.0550/
      DATA      wave( 935)/   741.1820/
      DATA      wave( 937)/   743.7195/
      DATA      wave( 938)/   743.9010/
      DATA      wave( 939)/   743.9290/
      DATA      wave( 940)/   744.9070/
      DATA      wave( 941)/   745.2580/
      DATA      wave( 945)/   748.3550/
      DATA      wave( 947)/   748.3800/
      DATA      wave( 948)/   748.4000/
      DATA      wave( 952)/   753.7130/
      DATA      wave( 953)/   754.1910/
      DATA      wave( 954)/   754.9300/
      DATA      wave( 955)/   755.7650/
      DATA      wave( 956)/   755.7900/
      DATA      wave( 958)/   763.3400/
      DATA      wave( 959)/   763.6570/
      DATA      wave( 960)/   764.4200/
      DATA      wave( 961)/   765.1480/
      DATA      wave( 963)/   765.6440/
      DATA      wave( 964)/   765.6930/
      DATA      wave( 966)/   769.3528/
      DATA      wave( 967)/   769.4083/
      DATA      wave( 968)/   770.4090/
      DATA      wave( 976)/   780.3240/
      DATA      wave( 979)/   781.7300/
      DATA      wave( 980)/   786.4800/
      DATA      wave( 981)/   787.7110/
      DATA      wave( 984)/   791.5136/
      DATA      wave( 986)/   791.9732/
      DATA      wave( 989)/   794.4750/
      DATA      wave( 990)/   800.6410/
      DATA      wave( 991)/   803.2340/
      DATA      wave( 992)/   804.6540/
      DATA      wave( 993)/   806.9700/
      DATA      wave( 995)/   808.0780/
      DATA      wave( 996)/   809.6680/
      DATA      wave( 998)/   810.6650/
      DATA      wave( 999)/   811.0512/
      DATA      wave(1000)/   812.9600/
      DATA      wave(1001)/   813.3790/
      DATA      wave(1002)/   814.1370/
      DATA      wave(1003)/   818.5900/
      DATA      wave(1004)/   820.9210/
      DATA      wave(1005)/   822.1600/
      DATA      wave(1006)/   823.2560/
      DATA      wave(1007)/   824.7990/
      DATA      wave(1008)/   826.3870/
      DATA      wave(1010)/   832.7572/
      DATA      wave(1011)/   832.9270/
      DATA      wave(1012)/   833.3294/
      DATA      wave(1013)/   834.4655/
      DATA      wave(1014)/   840.0300/
      DATA      wave(1017)/   843.7192/
      DATA      wave(1018)/   843.7700/
      DATA      wave(1019)/   844.2880/
      DATA      wave(1020)/   848.0700/
      DATA      wave(1021)/   850.6000/
      DATA      wave(1022)/   850.9070/
      DATA      wave(1023)/   854.2000/
      DATA      wave(1024)/   858.0918/
      DATA      wave(1025)/   858.5510/
      DATA      wave(1026)/   858.6090/
      DATA      wave(1027)/   859.7230/
      DATA      wave(1028)/   860.1500/
      DATA      wave(1029)/   861.1500/
      DATA      wave(1030)/   862.1400/
      DATA      wave(1031)/   862.4020/
      DATA      wave(1032)/   862.9850/
      DATA      wave(1033)/   863.3360/
      DATA      wave(1034)/   865.2250/
      DATA      wave(1035)/   865.4060/
      DATA      wave(1036)/   865.8350/
      DATA      wave(1037)/   866.1800/
      DATA      wave(1038)/   866.8000/
      DATA      wave(1040)/   869.4040/
      DATA      wave(1041)/   869.4240/
      DATA      wave(1042)/   869.4820/
      DATA      wave(1043)/   869.7542/
      DATA      wave(1044)/   870.0290/
      DATA      wave(1045)/   870.3320/
      DATA      wave(1046)/   870.3460/
      DATA      wave(1047)/   870.3670/
      DATA      wave(1048)/   871.1050/
      DATA      wave(1050)/   875.2770/
      DATA      wave(1051)/   875.6560/
      DATA      wave(1052)/   875.7210/
      DATA      wave(1053)/   876.0577/
      DATA      wave(1054)/   876.3310/
      DATA      wave(1055)/   876.6450/
      DATA      wave(1056)/   876.9870/
      DATA      wave(1057)/   877.7983/
      DATA      wave(1058)/   877.8787/
      DATA      wave(1059)/   878.7290/
      DATA      wave(1060)/   879.9466/
      DATA      wave(1061)/   883.1100/
      DATA      wave(1062)/   883.1250/
      DATA      wave(1063)/   884.6967/
      DATA      wave(1064)/   884.7189/
      DATA      wave(1065)/   885.9730/
      DATA      wave(1066)/   886.2260/
      DATA      wave(1067)/   886.3320/
      DATA      wave(1068)/   887.4570/
      DATA      wave(1069)/   888.0220/
      DATA      wave(1070)/   888.3720/
      DATA      wave(1071)/   889.7228/
      DATA      wave(1072)/   890.7860/
      DATA      wave(1074)/   899.4063/
      DATA      wave(1075)/   903.6235/
      DATA      wave(1076)/   903.9616/
      DATA      wave(1077)/   906.2060/
      DATA      wave(1078)/   906.4330/
      DATA      wave(1079)/   906.6170/
      DATA      wave(1080)/   906.8850/
      DATA      wave(1081)/   907.3752/
      DATA      wave(1082)/   907.4115/
      DATA      wave(1083)/   909.6976/
      DATA      wave(1084)/   910.2785/
      DATA      wave(1085)/   910.4840/
      DATA      wave(1086)/   910.6456/
      DATA      wave(1088)/   912.7350/
      DATA      wave(1089)/   912.7680/
      DATA      wave(1090)/   912.8390/
      DATA      wave(1091)/   912.9180/
      DATA      wave(1092)/   913.0060/
      DATA      wave(1093)/   913.1040/
      DATA      wave(1094)/   913.2150/
      DATA      wave(1095)/   913.3390/
      DATA      wave(1096)/   913.4800/
      DATA      wave(1097)/   913.6410/
      DATA      wave(1098)/   913.8260/
      DATA      wave(1099)/   913.9680/
      DATA      wave(1100)/   914.0390/
      DATA      wave(1101)/   914.2860/
      DATA      wave(1102)/   914.5760/
      DATA      wave(1103)/   914.9190/
      DATA      wave(1104)/   915.3290/
      DATA      wave(1105)/   915.6131/
      DATA      wave(1106)/   915.8240/
      DATA      wave(1107)/   916.4290/
      DATA      wave(1108)/   916.8150/
      DATA      wave(1109)/   916.8150/
      DATA      wave(1110)/   916.8150/
      DATA      wave(1111)/   917.1180/
      DATA      wave(1112)/   917.1806/
      DATA      wave(1113)/   918.0440/
      DATA      wave(1114)/   918.0440/
      DATA      wave(1115)/   918.0440/
      DATA      wave(1116)/   918.1294/
      DATA      wave(1117)/   918.5690/
      DATA      wave(1118)/   919.3514/
      DATA      wave(1119)/   919.6580/
      DATA      wave(1120)/   919.6580/
      DATA      wave(1121)/   919.6580/
      DATA      wave(1122)/   919.7810/
      DATA      wave(1123)/   919.9080/
      DATA      wave(1124)/   920.9631/
      DATA      wave(1125)/   921.5010/
      DATA      wave(1126)/   921.8600/
      DATA      wave(1127)/   921.8600/
      DATA      wave(1128)/   921.8600/
      DATA      wave(1129)/   922.2000/
      DATA      wave(1130)/   923.1504/
      DATA      wave(1131)/   923.7800/
      DATA      wave(1132)/   923.8790/
      DATA      wave(1133)/   924.9520/
      DATA      wave(1134)/   924.9520/
      DATA      wave(1135)/   924.9520/
      DATA      wave(1136)/   925.4420/
      DATA      wave(1137)/   926.2120/
      DATA      wave(1138)/   926.2257/
      DATA      wave(1139)/   926.8980/
      DATA      wave(1141)/   928.3250/
      DATA      wave(1142)/   929.5168/
      DATA      wave(1143)/   929.5168/
      DATA      wave(1144)/   929.5168/
      DATA      wave(1146)/   930.2566/
      DATA      wave(1147)/   930.7483/
      DATA      wave(1148)/   933.1340/
      DATA      wave(1149)/   933.3400/
      DATA      wave(1150)/   933.3780/
      DATA      wave(1152)/   935.2738/
      DATA      wave(1153)/   935.5180/
      DATA      wave(1154)/   935.7720/
      DATA      wave(1155)/   936.6295/
      DATA      wave(1156)/   936.6295/
      DATA      wave(1157)/   936.6295/
      DATA      wave(1158)/   936.6370/
      DATA      wave(1159)/   937.6520/
      DATA      wave(1160)/   937.8035/
      DATA      wave(1161)/   937.8405/
      DATA      wave(1163)/   938.0110/
      DATA      wave(1164)/   939.0610/
      DATA      wave(1165)/   939.1650/
      DATA      wave(1166)/   940.1920/
      DATA      wave(1168)/   944.5230/
      DATA      wave(1169)/   944.6330/
      DATA      wave(1170)/   944.8400/
      DATA      wave(1171)/   945.1910/
      DATA      wave(1173)/   946.7033/
      DATA      wave(1174)/   946.7694/
      DATA      wave(1176)/   948.6855/
      DATA      wave(1177)/   948.6855/
      DATA      wave(1178)/   948.6855/
      DATA      wave(1179)/   949.7431/
      DATA      wave(1180)/   950.6570/
      DATA      wave(1181)/   950.8846/
      DATA      wave(1182)/   951.0791/
      DATA      wave(1183)/   951.2947/
      DATA      wave(1186)/   952.3034/
      DATA      wave(1187)/   952.4148/
      DATA      wave(1188)/   952.5227/
      DATA      wave(1189)/   953.4152/
      DATA      wave(1190)/   953.6549/
      DATA      wave(1191)/   953.9699/
      DATA      wave(1192)/   954.1042/
      DATA      wave(1193)/   954.8260/
      DATA      wave(1194)/   955.2643/
      DATA      wave(1196)/   955.5292/
      DATA      wave(1198)/   955.8814/
      DATA      wave(1200)/   959.4936/
      DATA      wave(1202)/   961.0410/
      DATA      wave(1203)/   963.8010/
      DATA      wave(1204)/   963.9903/
      DATA      wave(1205)/   964.6256/
      DATA      wave(1206)/   965.0413/
      DATA      wave(1210)/   971.7371/
      DATA      wave(1211)/   971.7376/
      DATA      wave(1212)/   971.7382/
      DATA      wave(1214)/   972.1422/
      DATA      wave(1215)/   972.1428/
      DATA      wave(1217)/   972.5368/
      DATA      wave(1219)/   973.1877/
      DATA      wave(1222)/   974.0700/
      DATA      wave(1223)/   976.4481/
      DATA      wave(1225)/   977.0200/
      DATA      wave(1228)/   984.2865/
      DATA      wave(1230)/   987.9145/
      DATA      wave(1231)/   988.5778/
      DATA      wave(1232)/   988.6549/
      DATA      wave(1233)/   988.7734/
      DATA      wave(1234)/   989.7990/
      DATA      wave(1235)/   989.8731/
      DATA      wave(1238)/   996.5402/
      DATA      wave(1241)/   998.0000/
      DATA      wave(1245)/  1002.3464/
      DATA      wave(1251)/  1004.6676/
      DATA      wave(1253)/  1005.2779/
      DATA      wave(1257)/  1008.7670/
      DATA      wave(1261)/  1012.5010/
      DATA      wave(1263)/  1014.2330/
      DATA      wave(1265)/  1014.8590/
      DATA      wave(1266)/  1015.0249/
      DATA      wave(1270)/  1020.6989/
      DATA      wave(1273)/  1025.2821/
      DATA      wave(1275)/  1025.7223/
      DATA      wave(1276)/  1025.7616/
      DATA      wave(1277)/  1025.7626/
      DATA      wave(1278)/  1025.7633/
      DATA      wave(1279)/  1025.9681/
      DATA      wave(1280)/  1026.1134/
      DATA      wave(1281)/  1026.4733/
      DATA      wave(1282)/  1026.4744/
      DATA      wave(1283)/  1026.4757/
      DATA      wave(1286)/  1028.1740/
      DATA      wave(1288)/  1030.1000/
      DATA      wave(1290)/  1030.8845/
      DATA      wave(1291)/  1031.5070/
      DATA      wave(1293)/  1031.9261/
      DATA      wave(1295)/  1033.3311/
      DATA      wave(1298)/  1036.3367/
      DATA      wave(1299)/  1037.6167/
      DATA      wave(1303)/  1039.2303/
      DATA      wave(1304)/  1039.3130/
      DATA      wave(1305)/  1040.0500/
      DATA      wave(1306)/  1041.7148/
      DATA      wave(1308)/  1042.7793/
      DATA      wave(1309)/  1043.9857/
      DATA      wave(1313)/  1048.2198/
      DATA      wave(1314)/  1049.8199/
      DATA      wave(1316)/  1050.3000/
      DATA      wave(1322)/  1055.2617/
      DATA      wave(1327)/  1062.1520/
      DATA      wave(1328)/  1062.1980/
      DATA      wave(1329)/  1062.6620/
      DATA      wave(1331)/  1063.1764/
      DATA      wave(1332)/  1063.8010/
      DATA      wave(1333)/  1063.8311/
      DATA      wave(1334)/  1063.9718/
      DATA      wave(1335)/  1064.2100/
      DATA      wave(1336)/  1066.6599/
      DATA      wave(1339)/  1071.0358/
      DATA      wave(1342)/  1079.2930/
      DATA      wave(1345)/  1081.8748/
      DATA      wave(1346)/  1081.9351/
      DATA      wave(1347)/  1082.0410/
      DATA      wave(1348)/  1083.4204/
      DATA      wave(1350)/  1083.9937/
      DATA      wave(1352)/  1084.6670/
      DATA      wave(1354)/  1084.9919/
      DATA      wave(1355)/  1085.1381/
      DATA      wave(1359)/  1085.5790/
      DATA      wave(1361)/  1088.0590/
      DATA      wave(1362)/  1088.5000/
      DATA      wave(1366)/  1092.4365/
      DATA      wave(1367)/  1092.6200/
      DATA      wave(1368)/  1092.9900/
      DATA      wave(1370)/  1096.8098/
      DATA      wave(1371)/  1096.8770/
      DATA      wave(1372)/  1097.3691/
      DATA      wave(1377)/  1101.9362/
      DATA      wave(1378)/  1103.6290/
      DATA      wave(1379)/  1104.4270/
      DATA      wave(1380)/  1104.9420/
      DATA      wave(1381)/  1105.5320/
      DATA      wave(1383)/  1106.2500/
      DATA      wave(1384)/  1106.3149/
      DATA      wave(1385)/  1106.3596/
      DATA      wave(1387)/  1106.5710/
      DATA      wave(1388)/  1107.0430/
      DATA      wave(1389)/  1107.1460/
      DATA      wave(1391)/  1108.5040/
      DATA      wave(1392)/  1109.0310/
      DATA      wave(1394)/  1109.6340/
      DATA      wave(1395)/  1110.2803/
      DATA      wave(1396)/  1110.4410/
      DATA      wave(1399)/  1111.4210/
      DATA      wave(1400)/  1112.0480/
      DATA      wave(1401)/  1112.2690/
      DATA      wave(1402)/  1113.7930/
      DATA      wave(1403)/  1114.6281/
      DATA      wave(1404)/  1117.0000/
      DATA      wave(1405)/  1117.1320/
      DATA      wave(1407)/  1117.8660/
      DATA      wave(1408)/  1117.9202/
      DATA      wave(1409)/  1117.9771/
      DATA      wave(1412)/  1121.4520/
      DATA      wave(1413)/  1121.5197/
      DATA      wave(1414)/  1121.9749/
      DATA      wave(1415)/  1122.4380/
      DATA      wave(1416)/  1122.5183/
      DATA      wave(1417)/  1122.5240/
      DATA      wave(1422)/  1125.4478/
      DATA      wave(1423)/  1127.0984/
      DATA      wave(1424)/  1128.0081/
      DATA      wave(1425)/  1128.0790/
      DATA      wave(1426)/  1128.1713/
      DATA      wave(1427)/  1128.4771/
      DATA      wave(1430)/  1129.1930/
      DATA      wave(1432)/  1129.3175/
      DATA      wave(1434)/  1133.6650/
      DATA      wave(1436)/  1134.1653/
      DATA      wave(1437)/  1134.4149/
      DATA      wave(1438)/  1134.9803/
      DATA      wave(1441)/  1138.3840/
      DATA      wave(1442)/  1138.5609/
      DATA      wave(1443)/  1139.0867/
      DATA      wave(1444)/  1139.7930/
      DATA      wave(1445)/  1140.0096/
      DATA      wave(1446)/  1142.3656/
      DATA      wave(1447)/  1143.2260/
      DATA      wave(1448)/  1144.9379/
      DATA      wave(1457)/  1152.8180/
      DATA      wave(1458)/  1153.1790/
      DATA      wave(1459)/  1155.8092/
      DATA      wave(1460)/  1155.9900/
      DATA      wave(1461)/  1156.1842/
      DATA      wave(1462)/  1156.2600/
      DATA      wave(1463)/  1157.1857/
      DATA      wave(1464)/  1157.9097/
      DATA      wave(1465)/  1158.3240/
      DATA      wave(1466)/  1159.8170/
      DATA      wave(1468)/  1162.0150/
      DATA      wave(1472)/  1163.3260/
      DATA      wave(1473)/  1164.2080/
      DATA      wave(1475)/  1167.1479/
      DATA      wave(1478)/  1187.4170/
      DATA      wave(1480)/  1188.7515/
      DATA      wave(1481)/  1188.7742/
      DATA      wave(1482)/  1188.8333/
      DATA      wave(1483)/  1189.0280/
      DATA      wave(1484)/  1190.0208/
      DATA      wave(1485)/  1190.1910/
      DATA      wave(1486)/  1190.4158/
      DATA      wave(1489)/  1192.2175/
      DATA      wave(1491)/  1193.0308/
      DATA      wave(1492)/  1193.2897/
      DATA      wave(1494)/  1193.9955/
      DATA      wave(1495)/  1197.1840/
      DATA      wave(1497)/  1199.1340/
      DATA      wave(1498)/  1199.3910/
      DATA      wave(1499)/  1199.5496/
      DATA      wave(1500)/  1200.2233/
      DATA      wave(1501)/  1200.7098/
      DATA      wave(1502)/  1201.1180/
      DATA      wave(1508)/  1206.5000/
      DATA      wave(1516)/  1214.2950/
      DATA      wave(1517)/  1214.3180/
      DATA      wave(1518)/  1214.5620/
      DATA      wave(1519)/  1215.6700/
      DATA      wave(1520)/  1216.4500/
      DATA      wave(1521)/  1217.8430/
      DATA      wave(1522)/  1218.3440/
      DATA      wave(1524)/  1218.5710/
      DATA      wave(1525)/  1218.5950/
      DATA      wave(1529)/  1221.7531/
      DATA      wave(1531)/  1224.5056/
      DATA      wave(1532)/  1224.5439/
      DATA      wave(1533)/  1226.7000/
      DATA      wave(1534)/  1226.7419/
      DATA      wave(1535)/  1227.0000/
      DATA      wave(1536)/  1229.6080/
      DATA      wave(1537)/  1229.8400/
      DATA      wave(1539)/  1233.3124/
      DATA      wave(1540)/  1233.3455/
      DATA      wave(1544)/  1238.8210/
      DATA      wave(1545)/  1239.9253/
      DATA      wave(1546)/  1240.3947/
      DATA      wave(1547)/  1241.9050/
      DATA      wave(1548)/  1242.8040/
      DATA      wave(1551)/  1247.1602/
      DATA      wave(1553)/  1250.5780/
      DATA      wave(1555)/  1253.8051/
      DATA      wave(1556)/  1255.2720/
      DATA      wave(1557)/  1259.5179/
      DATA      wave(1558)/  1260.4221/
      DATA      wave(1559)/  1260.5330/
      DATA      wave(1560)/  1260.7355/
      DATA      wave(1561)/  1262.8599/
      DATA      wave(1562)/  1270.1434/
      DATA      wave(1564)/  1270.7804/
      DATA      wave(1565)/  1270.7874/
      DATA      wave(1566)/  1276.4825/
      DATA      wave(1567)/  1277.2454/
      DATA      wave(1568)/  1280.1353/
      DATA      wave(1569)/  1283.8810/
      DATA      wave(1571)/  1286.4440/
      DATA      wave(1575)/  1295.6531/
      DATA      wave(1576)/  1295.8840/
      DATA      wave(1577)/  1296.1740/
      DATA      wave(1578)/  1298.6970/
      DATA      wave(1579)/  1301.8740/
      DATA      wave(1580)/  1302.1685/
      DATA      wave(1581)/  1303.4301/
      DATA      wave(1582)/  1304.3702/
      DATA      wave(1583)/  1308.8660/
      DATA      wave(1584)/  1314.9940/
      DATA      wave(1585)/  1316.5425/
      DATA      wave(1586)/  1316.6150/
      DATA      wave(1587)/  1316.6219/
      DATA      wave(1588)/  1317.2170/
      DATA      wave(1589)/  1318.6840/
      DATA      wave(1590)/  1319.0220/
      DATA      wave(1592)/  1328.8333/
      DATA      wave(1594)/  1334.5323/
      DATA      wave(1595)/  1334.8130/
      DATA      wave(1596)/  1335.7258/
      DATA      wave(1597)/  1341.8901/
      DATA      wave(1598)/  1342.5536/
      DATA      wave(1600)/  1345.8781/
      DATA      wave(1601)/  1347.2396/
      DATA      wave(1602)/  1355.5977/
      DATA      wave(1603)/  1358.7729/
      DATA      wave(1604)/  1362.4611/
      DATA      wave(1608)/  1370.1320/
      DATA      wave(1610)/  1373.4905/
      DATA      wave(1612)/  1377.0730/
      DATA      wave(1613)/  1377.9343/
      DATA      wave(1617)/  1379.4282/
      DATA      wave(1619)/  1381.4760/
      DATA      wave(1620)/  1381.5527/
      DATA      wave(1621)/  1381.6353/
      DATA      wave(1622)/  1381.6851/
      DATA      wave(1623)/  1388.4358/
      DATA      wave(1625)/  1393.3240/
      DATA      wave(1626)/  1393.7550/
      DATA      wave(1632)/  1401.5142/
      DATA      wave(1633)/  1402.7700/
      DATA      wave(1635)/  1412.8660/
      DATA      wave(1636)/  1414.2920/
      DATA      wave(1637)/  1414.4020/
      DATA      wave(1638)/  1415.7200/
      DATA      wave(1640)/  1424.7791/
      DATA      wave(1641)/  1425.0299/
      DATA      wave(1642)/  1425.1877/
      DATA      wave(1643)/  1425.2189/
      DATA      wave(1646)/  1444.2960/
      DATA      wave(1647)/  1448.0110/
      DATA      wave(1648)/  1449.9969/
      DATA      wave(1650)/  1454.8420/
      DATA      wave(1651)/  1466.2030/
      DATA      wave(1652)/  1467.2590/
      DATA      wave(1654)/  1467.7560/
      DATA      wave(1657)/  1472.9706/
      DATA      wave(1658)/  1473.9943/
      DATA      wave(1659)/  1474.3785/
      DATA      wave(1660)/  1474.5706/
      DATA      wave(1661)/  1477.2220/
      DATA      wave(1662)/  1480.9510/
      DATA      wave(1667)/  1492.9540/
      DATA      wave(1668)/  1495.0450/
      DATA      wave(1669)/  1501.9590/
      DATA      wave(1670)/  1502.1479/
      DATA      wave(1681)/  1526.7065/
      DATA      wave(1684)/  1532.5330/
      DATA      wave(1690)/  1539.7380/
      DATA      wave(1691)/  1540.3540/
      DATA      wave(1693)/  1542.4320/
      DATA      wave(1696)/  1545.6121/
      DATA      wave(1698)/  1546.6740/
      DATA      wave(1699)/  1547.1300/
      DATA      wave(1700)/  1547.9460/
      DATA      wave(1701)/  1548.1949/
      DATA      wave(1702)/  1550.7700/
      DATA      wave(1703)/  1552.0220/
      DATA      wave(1704)/  1552.7600/
      DATA      wave(1705)/  1552.9480/
      DATA      wave(1706)/  1553.7540/
      DATA      wave(1707)/  1554.2960/
      DATA      wave(1708)/  1555.5140/
      DATA      wave(1711)/  1560.3092/
      DATA      wave(1712)/  1560.4060/
      DATA      wave(1714)/  1562.0020/
      DATA      wave(1718)/  1566.3030/
      DATA      wave(1719)/  1567.5730/
      DATA      wave(1720)/  1568.6180/
      DATA      wave(1721)/  1572.6479/
      DATA      wave(1722)/  1572.7180/
      DATA      wave(1724)/  1574.5450/
      DATA      wave(1728)/  1584.8521/
      DATA      wave(1729)/  1588.6876/
      DATA      wave(1730)/  1589.1730/
      DATA      wave(1731)/  1589.5610/
      DATA      wave(1738)/  1608.4510/
      DATA      wave(1742)/  1611.2004/
      DATA      wave(1743)/  1613.3763/
      DATA      wave(1744)/  1614.5660/
      DATA      wave(1745)/  1620.0608/
      DATA      wave(1748)/  1625.7051/
      DATA      wave(1751)/  1631.1705/
      DATA      wave(1755)/  1649.8579/
      DATA      wave(1756)/  1651.1636/
      DATA      wave(1757)/  1651.9908/
      DATA      wave(1758)/  1656.9283/
      DATA      wave(1759)/  1658.3115/
      DATA      wave(1762)/  1666.3762/
      DATA      wave(1764)/  1668.4288/
      DATA      wave(1765)/  1670.7874/
      DATA      wave(1766)/  1671.6713/
      DATA      wave(1767)/  1674.5953/
      DATA      wave(1768)/  1679.6969/
      DATA      wave(1769)/  1683.4116/
      DATA      wave(1770)/  1693.2935/
      DATA      wave(1771)/  1700.6360/
      DATA      wave(1772)/  1703.4050/
      DATA      wave(1773)/  1707.0605/
      DATA      wave(1774)/  1709.6000/
      DATA      wave(1775)/  1741.5490/
      DATA      wave(1777)/  1747.7937/
      DATA      wave(1780)/  1751.9100/
      DATA      wave(1782)/  1762.8920/
      DATA      wave(1783)/  1763.6606/
      DATA      wave(1784)/  1765.6320/
      DATA      wave(1786)/  1773.9490/
      DATA      wave(1787)/  1774.9487/
      DATA      wave(1789)/  1782.8291/
      DATA      wave(1790)/  1783.3180/
      DATA      wave(1791)/  1787.6481/
      DATA      wave(1792)/  1804.4730/
      DATA      wave(1793)/  1807.3113/
      DATA      wave(1794)/  1808.0126/
      DATA      wave(1797)/  1822.4553/
      DATA      wave(1799)/  1827.9351/
      DATA      wave(1801)/  1841.1521/
      DATA      wave(1802)/  1842.8890/
      DATA      wave(1803)/  1845.5201/
      DATA      wave(1804)/  1851.3804/
      DATA      wave(1805)/  1851.6880/
      DATA      wave(1806)/  1854.7164/
      DATA      wave(1807)/  1859.2635/
      DATA      wave(1808)/  1862.7896/
      DATA      wave(1810)/  1865.3093/
      DATA      wave(1811)/  1873.0576/
      DATA      wave(1812)/  1883.7789/
      DATA      wave(1815)/  1892.0300/
      DATA      wave(1816)/  1896.1500/
      DATA      wave(1818)/  1900.2866/
      DATA      wave(1819)/  1901.7729/
      DATA      wave(1820)/  1905.7740/
      DATA      wave(1821)/  1906.2390/
      DATA      wave(1823)/  1910.6090/
      DATA      wave(1824)/  1910.9380/
      DATA      wave(1825)/  1915.2235/
      DATA      wave(1827)/  1932.2700/
      DATA      wave(1828)/  1934.5352/
      DATA      wave(1829)/  1937.2686/
      DATA      wave(1832)/  1940.4510/
      DATA      wave(1833)/  1940.5610/
      DATA      wave(1834)/  1940.6400/
      DATA      wave(1835)/  1941.2800/
      DATA      wave(1836)/  1946.9880/
      DATA      wave(1837)/  1949.1427/
      DATA      wave(1839)/  1960.1440/
      DATA      wave(1842)/  1977.5979/
      DATA      wave(1844)/  1996.0470/
      DATA      wave(1846)/  1999.5000/
      DATA      wave(1848)/  2004.4900/
      DATA      wave(1850)/  2012.1610/
      DATA      wave(1856)/  2026.1360/
      DATA      wave(1857)/  2026.2690/
      DATA      wave(1858)/  2026.4070/
      DATA      wave(1859)/  2026.4768/
      DATA      wave(1860)/  2027.2640/
      DATA      wave(1861)/  2040.5690/
      DATA      wave(1862)/  2042.3676/
      DATA      wave(1864)/  2056.2539/
      DATA      wave(1865)/  2059.4709/
      DATA      wave(1867)/  2062.2339/
      DATA      wave(1869)/  2062.6641/
      DATA      wave(1870)/  2066.1609/
      DATA      wave(1876)/  2078.0110/
      DATA      wave(1877)/  2083.4460/
      DATA      wave(1878)/  2084.7849/
      DATA      wave(1879)/  2086.3760/
      DATA      wave(1883)/  2098.1599/
      DATA      wave(1884)/  2100.5708/
      DATA      wave(1885)/  2108.8054/
      DATA      wave(1888)/  2118.9900/
      DATA      wave(1889)/  2119.3459/
      DATA      wave(1892)/  2127.2812/
      DATA      wave(1893)/  2130.3350/
      DATA      wave(1894)/  2132.6902/
      DATA      wave(1895)/  2138.8269/
      DATA      wave(1896)/  2139.2473/
      DATA      wave(1897)/  2139.2668/
      DATA      wave(1898)/  2140.3726/
      DATA      wave(1900)/  2142.9021/
      DATA      wave(1902)/  2146.2300/
      DATA      wave(1903)/  2151.4719/
      DATA      wave(1905)/  2165.2290/
      DATA      wave(1906)/  2165.7749/
      DATA      wave(1907)/  2167.4534/
      DATA      wave(1909)/  2169.5068/
      DATA      wave(1912)/  2179.6321/
      DATA      wave(1913)/  2182.4050/
      DATA      wave(1914)/  2186.9338/
      DATA      wave(1916)/  2199.8679/
      DATA      wave(1917)/  2201.4141/
      DATA      wave(1919)/  2205.3555/
      DATA      wave(1920)/  2208.6665/
      DATA      wave(1921)/  2209.4971/
      DATA      wave(1922)/  2214.5400/
      DATA      wave(1923)/  2222.5229/
      DATA      wave(1924)/  2226.3889/
      DATA      wave(1933)/  2249.8767/
      DATA      wave(1934)/  2251.4880/
      DATA      wave(1936)/  2255.4980/
      DATA      wave(1938)/  2256.5730/
      DATA      wave(1939)/  2258.7051/
      DATA      wave(1940)/  2260.2097/
      DATA      wave(1941)/  2260.7805/
      DATA      wave(1942)/  2264.1638/
      DATA      wave(1943)/  2264.1748/
      DATA      wave(1946)/  2276.1689/
      DATA      wave(1947)/  2276.7290/
      DATA      wave(1950)/  2290.6899/
      DATA      wave(1952)/  2298.8770/
      DATA      wave(1956)/  2311.6689/
      DATA      wave(1958)/  2320.7439/
      DATA      wave(1963)/  2338.2019/
      DATA      wave(1964)/  2344.2141/
      DATA      wave(1965)/  2346.2590/
      DATA      wave(1966)/  2348.2280/
      DATA      wave(1970)/  2365.4529/
      DATA      wave(1971)/  2365.7820/
      DATA      wave(1972)/  2366.6340/
      DATA      wave(1973)/  2367.5339/
      DATA      wave(1974)/  2367.5906/
      DATA      wave(1975)/  2367.7761/
      DATA      wave(1977)/  2372.7944/
      DATA      wave(1979)/  2374.4612/
      DATA      wave(1981)/  2382.7649/
      DATA      wave(1982)/  2385.5869/
      DATA      wave(1984)/  2399.2891/
      DATA      wave(1985)/  2407.9829/
      DATA      wave(1986)/  2425.6680/
      DATA      wave(1991)/  2448.4509/
      DATA      wave(1993)/  2463.3923/
      DATA      wave(1994)/  2473.9041/
      DATA      wave(1998)/  2484.0205/
      DATA      wave(1999)/  2487.1230/
      DATA      wave(2005)/  2501.8857/
      DATA      wave(2009)/  2515.0725/
      DATA      wave(2010)/  2522.1221/
      DATA      wave(2011)/  2523.6084/
      DATA      wave(2022)/  2568.7522/
      DATA      wave(2024)/  2576.8770/
      DATA      wave(2026)/  2586.6499/
      DATA      wave(2027)/  2594.4990/
      DATA      wave(2031)/  2600.1729/
      DATA      wave(2032)/  2606.4619/
      DATA      wave(2035)/  2652.3577/
      DATA      wave(2036)/  2653.2649/
      DATA      wave(2040)/  2681.1370/
      DATA      wave(2041)/  2681.2300/
      DATA      wave(2042)/  2683.8870/
      DATA      wave(2045)/  2719.8330/
      DATA      wave(2046)/  2722.4500/
      DATA      wave(2047)/  2735.6230/
      DATA      wave(2049)/  2740.5249/
      DATA      wave(2053)/  2795.6411/
      DATA      wave(2054)/  2796.3521/
      DATA      wave(2055)/  2799.0940/
      DATA      wave(2056)/  2801.9070/
      DATA      wave(2057)/  2803.5310/
      DATA      wave(2058)/  2808.0708/
      DATA      wave(2060)/  2836.2903/
      DATA      wave(2061)/  2852.9641/
      DATA      wave(2062)/  2853.6489/
      DATA      wave(2063)/  2853.8501/
      DATA      wave(2065)/  2875.0156/
      DATA      wave(2066)/  2875.0779/
      DATA      wave(2067)/  2913.0100/
      DATA      wave(2069)/  2934.3921/
      DATA      wave(2070)/  2937.7625/
      DATA      wave(2071)/  2942.8511/
      DATA      wave(2073)/  2967.7646/
      DATA      wave(2075)/  2984.4402/
      DATA      wave(2076)/  2984.9990/
      DATA      wave(2080)/  3020.0200/
      DATA      wave(2081)/  3021.5188/
      DATA      wave(2085)/  3044.4351/
      DATA      wave(2087)/  3054.5420/
      DATA      wave(2088)/  3058.2920/
      DATA      wave(2090)/  3067.2451/
      DATA      wave(2093)/  3073.8770/
      DATA      wave(2096)/  3083.0479/
      DATA      wave(2106)/  3148.9609/
      DATA      wave(2113)/  3184.9331/
      DATA      wave(2114)/  3187.3721/
      DATA      wave(2115)/  3192.5820/
      DATA      wave(2116)/  3194.1489/
      DATA      wave(2119)/  3227.9109/
      DATA      wave(2122)/  3230.1311/
      DATA      wave(2124)/  3233.8640/
      DATA      wave(2125)/  3242.9290/
      DATA      wave(2126)/  3248.4741/
      DATA      wave(2129)/  3270.8420/
      DATA      wave(2130)/  3274.8979/
      DATA      wave(2134)/  3303.3191/
      DATA      wave(2135)/  3303.9290/
      DATA      wave(2136)/  3342.8340/
      DATA      wave(2142)/  3370.5310/
      DATA      wave(2143)/  3384.7400/
      DATA      wave(2144)/  3392.0129/
      DATA      wave(2146)/  3410.5500/
      DATA      wave(2147)/  3438.2600/
      DATA      wave(2148)/  3441.5920/
      DATA      wave(2155)/  3527.8521/
      DATA      wave(2158)/  3579.7051/
      DATA      wave(2159)/  3581.9470/
      DATA      wave(2162)/  3594.5071/
      DATA      wave(2163)/  3606.3501/
      DATA      wave(2166)/  3636.4980/
      DATA      wave(2167)/  3643.8220/
      DATA      wave(2168)/  3650.3428/
      DATA      wave(2169)/  3680.9612/
      DATA      wave(2172)/  3718.4490/
      DATA      wave(2173)/  3720.9929/
      DATA      wave(2174)/  3730.8669/
      DATA      wave(2178)/  3809.5991/
      DATA      wave(2179)/  3819.3259/
      DATA      wave(2180)/  3825.5288/
      DATA      wave(2182)/  3856.4551/
      DATA      wave(2183)/  3861.0059/
      DATA      wave(2191)/  3908.5950/
      DATA      wave(2192)/  3934.7771/
      DATA      wave(2193)/  3945.1223/
      DATA      wave(2194)/  3963.9719/
      DATA      wave(2195)/  3969.5911/
      DATA      wave(2196)/  3982.8879/
      DATA      wave(2199)/  4021.5281/
      DATA      wave(2200)/  4031.8921/
      DATA      wave(2202)/  4034.2019/
      DATA      wave(2203)/  4035.6230/
      DATA      wave(2204)/  4045.2849/
      DATA      wave(2205)/  4048.3560/
      DATA      wave(2207)/  4078.8650/
      DATA      wave(2214)/  4216.7109/
      DATA      wave(2216)/  4217.3716/
      DATA      wave(2217)/  4227.9180/
      DATA      wave(2218)/  4255.5288/
      DATA      wave(2219)/  4275.9990/
      DATA      wave(2220)/  4290.9229/
      DATA      wave(2223)/  4377.1597/
      DATA      wave(2230)/  4608.6221/
      DATA      wave(2235)/  5111.8374/
      DATA      wave(2236)/  5537.0210/
      DATA      wave(2238)/  5891.5835/
      DATA      wave(2239)/  5897.5581/
      DATA      wave(2243)/  6709.6128/
      DATA      wave(2244)/  6709.7642/
      DATA      wave(2245)/  7667.0210/
      DATA      wave(2246)/  7701.0928/
      DATA      wave(2247)/  7802.3740/
      DATA      wave(3000)/  1037.0182/

c*************

      DATA ion(   1)/"V V     "/
      DATA ion(   2)/"CaVI    "/
      DATA ion(   3)/"O III   "/
      DATA ion(   4)/"NeIII   "/
      DATA ion(   5)/"NeIII   "/
      DATA ion(   6)/"TiV     "/
      DATA ion(   7)/"NeIII   "/
      DATA ion(   8)/"HeII    "/
      DATA ion(   9)/"CaVI    "/
      DATA ion(  10)/"HeII    "/
      DATA ion(  11)/"NaIII   "/
      DATA ion(  12)/"FeX     "/
      DATA ion(  13)/"FeX     "/
      DATA ion(  17)/"HeII    "/
      DATA ion(  18)/"CaVI    "/
      DATA ion(  19)/"HeII    "/
      DATA ion(  20)/"N III   "/
      DATA ion(  21)/"N III   "/
      DATA ion(  22)/"P XI    "/
      DATA ion(  23)/"HeII    "/
      DATA ion(  24)/"FeX     "/
      DATA ion(  25)/"FeVII   "/
      DATA ion(  26)/"MgIII   "/
      DATA ion(  27)/"FeVIII  "/
      DATA ion(  28)/"FeVII   "/
      DATA ion(  29)/"FeVII   "/
      DATA ion(  30)/"HeII    "/
      DATA ion(  33)/"N III   "/
      DATA ion(  34)/"N III   "/
      DATA ion(  35)/"FeVII   "/
      DATA ion(  36)/"NiXXVI  "/
      DATA ion(  38)/"P XI    "/
      DATA ion(  39)/"MgIII   "/
      DATA ion(  40)/"HeII    "/
      DATA ion(  41)/"FeX     "/
      DATA ion(  42)/"CoXV    "/
      DATA ion(  43)/"ClXI    "/
      DATA ion(  44)/"FeVII   "/
      DATA ion(  46)/"ArXIII  "/
      DATA ion(  47)/"HeII    "/
      DATA ion(  48)/"ClXI    "/
      DATA ion(  50)/"ClXIV   "/
      DATA ion(  51)/"N III   "/
      DATA ion(  52)/"N III   "/
      DATA ion(  53)/"NeIII   "/
      DATA ion(  54)/"O IV    "/
      DATA ion(  56)/"MnVII   "/
      DATA ion(  57)/"CoXV    "/
      DATA ion(  58)/"NiXVI   "/
      DATA ion(  59)/"S XI    "/
      DATA ion(  61)/"V VIII  "/
      DATA ion(  64)/"FeXIII  "/
      DATA ion(  67)/"ClXI    "/
      DATA ion(  69)/"HeII    "/
      DATA ion(  71)/"ArXIV   "/
      DATA ion(  73)/"V IX    "/
      DATA ion(  74)/"P VIII  "/
      DATA ion(  75)/"CrXII   "/
      DATA ion(  76)/"V IX    "/
      DATA ion(  77)/"C IV    "/
      DATA ion(  78)/"C IV    "/
      DATA ion(  79)/"FeIX    "/
      DATA ion(  81)/"SiVI    "/
c      DATA ion(  82)/"FeXXII  "/
      DATA ion(  83)/"N IV    "/
      DATA ion(  84)/"AlVIII  "/
      DATA ion(  85)/"TiVI    "/
      DATA ion(  86)/"MnVII   "/
      DATA ion(  87)/"P VIII  "/
      DATA ion(  88)/"P XI    "/
      DATA ion(  89)/"N III   "/
      DATA ion(  90)/"N III   "/
      DATA ion(  91)/"O III   "/
      DATA ion(  93)/"S VI    "/
c      DATA ion(  94)/"NiXVII  "/
      DATA ion(  95)/"S VI    "/
      DATA ion(  97)/"CaIV    "/
      DATA ion(  98)/"TiVI    "/
      DATA ion(  99)/"NaIII   "/
      DATA ion( 100)/"NaIII   "/
      DATA ion( 102)/"NeIII   "/
      DATA ion( 103)/"NeIII   "/
      DATA ion( 104)/"NeIII   "/
      DATA ion( 105)/"MgV     "/
      DATA ion( 107)/"FeXIV   "/
      DATA ion( 108)/"V V     "/
      DATA ion( 109)/"TiV     "/
      DATA ion( 110)/"SiX     "/
c      DATA ion( 111)/"FeXXIV  "/
      DATA ion( 112)/"O III   "/
      DATA ion( 113)/"P V     "/
      DATA ion( 116)/"F III   "/
      DATA ion( 117)/"F III   "/
      DATA ion( 118)/"CoXV    "/
      DATA ion( 119)/"HeII    "/
      DATA ion( 120)/"SiX     "/
      DATA ion( 121)/"S XIII  "/
      DATA ion( 122)/"S X     "/
      DATA ion( 123)/"FeXIV   "/
      DATA ion( 124)/"ClXII   "/
      DATA ion( 125)/"CaV     "/
      DATA ion( 126)/"CaV     "/
      DATA ion( 129)/"TiXX    "/
      DATA ion( 131)/"S X     "/
      DATA ion( 132)/"O III   "/
      DATA ion( 133)/"P X     "/
      DATA ion( 134)/"TiVI    "/
      DATA ion( 136)/"O III   "/
c      DATA ion( 137)/"FeXXIII "/
      DATA ion( 138)/"CrVI    "/
      DATA ion( 139)/"S X     "/
      DATA ion( 140)/"O III   "/
      DATA ion( 141)/"ClXIII  "/
      DATA ion( 142)/"N III   "/
      DATA ion( 143)/"N III   "/
      DATA ion( 144)/"V XI    "/
      DATA ion( 145)/"CoXVI   "/
      DATA ion( 146)/"N III   "/
      DATA ion( 147)/"N III   "/
      DATA ion( 148)/"NaIII   "/
      DATA ion( 149)/"MnXXIII "/
      DATA ion( 150)/"O III   "/
      DATA ion( 151)/"NeIII   "/
      DATA ion( 152)/"NeIII   "/
      DATA ion( 153)/"TiVIII  "/
      DATA ion( 155)/"NaIII   "/
      DATA ion( 156)/"TiVIII  "/
      DATA ion( 159)/"N III   "/
      DATA ion( 160)/"N III   "/
      DATA ion( 161)/"TiVIII  "/
      DATA ion( 162)/"N III   "/
      DATA ion( 163)/"CaV     "/
      DATA ion( 164)/"C III   "/
      DATA ion( 165)/"CaV     "/
      DATA ion( 167)/"N III   "/
      DATA ion( 168)/"SiX     "/
      DATA ion( 169)/"MnXIII  "/
      DATA ion( 170)/"N III   "/
      DATA ion( 171)/"SiVII   "/
      DATA ion( 172)/"CaIII   "/
      DATA ion( 173)/"N III   "/
      DATA ion( 174)/"C III   "/
      DATA ion( 175)/"FeXIV   "/
      DATA ion( 176)/"TiIX    "/
      DATA ion( 177)/"CaIII   "/
      DATA ion( 178)/"O III   "/
      DATA ion( 179)/"SiVII   "/
      DATA ion( 180)/"MgVII   "/
      DATA ion( 181)/"N III   "/
      DATA ion( 182)/"FeXI    "/
      DATA ion( 183)/"FeVI    "/
      DATA ion( 184)/"MnXIII  "/
      DATA ion( 186)/"FeVI    "/
      DATA ion( 187)/"FeVI    "/
      DATA ion( 188)/"P XII   "/
      DATA ion( 189)/"N III   "/
      DATA ion( 190)/"AlV     "/
      DATA ion( 191)/"O IV    "/
      DATA ion( 192)/"CrXXII  "/
      DATA ion( 194)/"C III   "/
      DATA ion( 195)/"AlIX    "/
      DATA ion( 196)/"TiIX    "/
      DATA ion( 197)/"CrVI    "/
      DATA ion( 198)/"O III   "/
      DATA ion( 200)/"S XI    "/
      DATA ion( 201)/"N III   "/
      DATA ion( 203)/"AlIX    "/
      DATA ion( 204)/"NeIII   "/
      DATA ion( 205)/"NeIII   "/
      DATA ion( 206)/"NeIII   "/
      DATA ion( 207)/"P IX    "/
      DATA ion( 210)/"FeXV    "/
      DATA ion( 211)/"CaV     "/
      DATA ion( 213)/"P IX    "/
      DATA ion( 215)/"N III   "/
      DATA ion( 216)/"S V     "/
      DATA ion( 218)/"C III   "/
      DATA ion( 219)/"S XII   "/
      DATA ion( 220)/"NiXVI   "/
      DATA ion( 221)/"P IX    "/
      DATA ion( 222)/"TiX     "/
      DATA ion( 223)/"ScIV    "/
      DATA ion( 224)/"FeVI    "/
      DATA ion( 225)/"SiIX    "/
      DATA ion( 227)/"FeVI    "/
      DATA ion( 228)/"C III   "/
      DATA ion( 229)/"FeVI    "/
c      DATA ion( 230)/"NiXVIII "/
      DATA ion( 231)/"ArVI    "/
      DATA ion( 232)/"N III   "/
      DATA ion( 237)/"FeVI    "/
      DATA ion( 238)/"FeVI    "/
      DATA ion( 239)/"CrXII   "/
      DATA ion( 240)/"MnXIII  "/
      DATA ion( 241)/"NeII    "/
      DATA ion( 242)/"NiXII   "/
      DATA ion( 243)/"FeVI    "/
      DATA ion( 246)/"CaIV    "/
      DATA ion( 247)/"CaIII   "/
      DATA ion( 248)/"NiXV    "/
      DATA ion( 251)/"N III   "/
      DATA ion( 252)/"CrXII   "/
      DATA ion( 253)/"NaII    "/
      DATA ion( 254)/"SiIV    "/
      DATA ion( 255)/"SiIV    "/
      DATA ion( 256)/"AlIX    "/
      DATA ion( 257)/"NaII    "/
      DATA ion( 258)/"CaIII   "/
      DATA ion( 260)/"CaXVIII "/
      DATA ion( 261)/"NiXIV   "/
      DATA ion( 262)/"NiXIII  "/
      DATA ion( 263)/"SiXI    "/
      DATA ion( 264)/"FeXIII  "/
      DATA ion( 265)/"O III   "/
      DATA ion( 266)/"HeII    "/
      DATA ion( 267)/"MnXIV   "/
      DATA ion( 268)/"O III   "/
      DATA ion( 269)/"N III   "/
      DATA ion( 270)/"AlVI    "/
      DATA ion( 272)/"MnVI    "/
      DATA ion( 274)/"P X     "/
      DATA ion( 275)/"AlVI    "/
      DATA ion( 277)/"C III   "/
      DATA ion( 278)/"MnVI    "/
      DATA ion( 279)/"SiIV    "/
      DATA ion( 280)/"SiIV    "/
      DATA ion( 281)/"CoXV    "/
      DATA ion( 282)/"N III   "/
      DATA ion( 283)/"MgVIII  "/
      DATA ion( 284)/"NaVI    "/
      DATA ion( 285)/"C IV    "/
      DATA ion( 286)/"C IV    "/
      DATA ion( 287)/"CoXVII  "/
      DATA ion( 288)/"NeIII   "/
      DATA ion( 289)/"NeII    "/
      DATA ion( 290)/"MgVIII  "/
      DATA ion( 291)/"SiVIII  "/
      DATA ion( 292)/"N III   "/
      DATA ion( 295)/"P XI    "/
      DATA ion( 297)/"SiVIII  "/
      DATA ion( 298)/"NiXIV   "/
      DATA ion( 299)/"CaIV    "/
      DATA ion( 300)/"CaIV    "/
      DATA ion( 301)/"NeII    "/
      DATA ion( 303)/"NeII    "/
      DATA ion( 305)/"CrXII   "/
      DATA ion( 306)/"SiVIII  "/
c      DATA ion( 307)/"NiXVIII "/
      DATA ion( 309)/"MgIV    "/
      DATA ion( 311)/"C III   "/
      DATA ion( 312)/"CaV     "/
      DATA ion( 314)/"N III   "/
      DATA ion( 315)/"N III   "/
      DATA ion( 316)/"AlVIII  "/
      DATA ion( 317)/"CaV     "/
      DATA ion( 318)/"NeII    "/
      DATA ion( 319)/"NeII    "/
      DATA ion( 324)/"NeII    "/
      DATA ion( 325)/"NeII    "/
      DATA ion( 326)/"K XVII  "/
      DATA ion( 327)/"NeII    "/
      DATA ion( 328)/"SiIV    "/
      DATA ion( 329)/"SiIV    "/
      DATA ion( 330)/"NeII    "/
      DATA ion( 331)/"NeII    "/
      DATA ion( 332)/"CrXIII  "/
      DATA ion( 334)/"P V     "/
      DATA ion( 335)/"NeII    "/
      DATA ion( 336)/"P V     "/
      DATA ion( 337)/"NeII    "/
      DATA ion( 338)/"CaIV    "/
      DATA ion( 340)/"MnXII   "/
      DATA ion( 341)/"CaIV    "/
      DATA ion( 342)/"NeII    "/
      DATA ion( 343)/"NeII    "/
      DATA ion( 344)/"NeII    "/
      DATA ion( 345)/"NeII    "/
      DATA ion( 346)/"CaIV    "/
      DATA ion( 348)/"CaIV    "/
      DATA ion( 349)/"N III   "/
      DATA ion( 350)/"AlX     "/
      DATA ion( 351)/"CaVI    "/
      DATA ion( 352)/"CaV     "/
      DATA ion( 354)/"CaVI    "/
      DATA ion( 355)/"FeXIV   "/
      DATA ion( 356)/"MgVIII  "/
      DATA ion( 357)/"CaVI    "/
      DATA ion( 358)/"CaV     "/
      DATA ion( 359)/"FeXVI   "/
      DATA ion( 362)/"ArV     "/
      DATA ion( 363)/"CoXIII  "/
      DATA ion( 364)/"CaIV    "/
      DATA ion( 365)/"CaVII   "/
      DATA ion( 366)/"CoXVII  "/
      DATA ion( 367)/"CaIV    "/
      DATA ion( 368)/"CaV     "/
      DATA ion( 369)/"FeXI    "/
      DATA ion( 370)/"CaIV    "/
      DATA ion( 371)/"SiIX    "/
      DATA ion( 372)/"CaXVIII "/
      DATA ion( 373)/"NeIII   "/
      DATA ion( 374)/"FeX     "/
      DATA ion( 377)/"FeXII   "/
      DATA ion( 378)/"CaVII   "/
      DATA ion( 379)/"SiX     "/
      DATA ion( 380)/"FeXIII  "/
      DATA ion( 384)/"ArIII   "/
      DATA ion( 385)/"ArIII   "/
      DATA ion( 386)/"TiX     "/
      DATA ion( 387)/"NaVII   "/
      DATA ion( 388)/"MgV     "/
      DATA ion( 389)/"FeXII   "/
      DATA ion( 390)/"AlVII   "/
      DATA ion( 391)/"NeII    "/
      DATA ion( 392)/"NaVII   "/
      DATA ion( 393)/"FeXI    "/
      DATA ion( 394)/"NeII    "/
      DATA ion( 395)/"MgV     "/
      DATA ion( 398)/"AlVII   "/
      DATA ion( 399)/"ArXVI   "/
      DATA ion( 400)/"CaVIII  "/
      DATA ion( 401)/"ArIII   "/
      DATA ion( 402)/"ArIII   "/
      DATA ion( 403)/"NeII    "/
      DATA ion( 404)/"V XII   "/
      DATA ion( 405)/"NeII    "/
      DATA ion( 406)/"TiX     "/
      DATA ion( 407)/"NeII    "/
      DATA ion( 408)/"NeII    "/
      DATA ion( 409)/"AlVII   "/
      DATA ion( 410)/"NeV     "/
      DATA ion( 411)/"CaIII   "/
      DATA ion( 412)/"CrXI    "/
      DATA ion( 413)/"FeXVI   "/
      DATA ion( 414)/"MnXV    "/
      DATA ion( 415)/"NeII    "/
      DATA ion( 416)/"SiIV    "/
      DATA ion( 417)/"MnXIII  "/
      DATA ion( 418)/"SiIV    "/
      DATA ion( 419)/"FeV     "/
      DATA ion( 420)/"MgVII   "/
      DATA ion( 421)/"FeXII   "/
      DATA ion( 422)/"FeV     "/
      DATA ion( 423)/"K XVII  "/
c      DATA ion( 424)/"NiXVII  "/
      DATA ion( 425)/"ArIII   "/
      DATA ion( 426)/"ArIII   "/
      DATA ion( 428)/"ArIII   "/
      DATA ion( 429)/"MgIX    "/
      DATA ion( 430)/"ArIII   "/
      DATA ion( 431)/"ArIII   "/
      DATA ion( 432)/"S IV    "/
      DATA ion( 433)/"CaIV    "/
      DATA ion( 435)/"CaIV    "/
      DATA ion( 444)/"ArIII   "/
      DATA ion( 445)/"ArIII   "/
      DATA ion( 447)/"NaII    "/
      DATA ion( 448)/"O III   "/
      DATA ion( 449)/"N III   "/
      DATA ion( 459)/"O II    "/
      DATA ion( 460)/"O II    "/
      DATA ion( 461)/"MnIX    "/
      DATA ion( 463)/"MnXI    "/
      DATA ion( 464)/"TiX     "/
      DATA ion( 465)/"NaIII   "/
      DATA ion( 466)/"NaVII   "/
      DATA ion( 470)/"AlVIII  "/
      DATA ion( 471)/"MnXI    "/
      DATA ion( 472)/"MnX     "/
      DATA ion( 473)/"ClXV    "/
      DATA ion( 474)/"MnXV    "/
      DATA ion( 475)/"AlIX    "/
      DATA ion( 476)/"N II    "/
      DATA ion( 478)/"TiXI    "/
      DATA ion( 479)/"C III   "/
      DATA ion( 480)/"FeV     "/
      DATA ion( 481)/"MnXII   "/
      DATA ion( 482)/"O II    "/
      DATA ion( 483)/"O II    "/
      DATA ion( 484)/"FeV     "/
      DATA ion( 485)/"O II    "/
      DATA ion( 486)/"P IV    "/
      DATA ion( 488)/"ArXVI   "/
      DATA ion( 489)/"CrXIV   "/
      DATA ion( 490)/"ClV     "/
      DATA ion( 494)/"O II    "/
      DATA ion( 495)/"O II    "/
      DATA ion( 496)/"O II    "/
      DATA ion( 497)/"CrXII   "/
      DATA ion( 502)/"MnXI    "/
      DATA ion( 503)/"ArIII   "/
      DATA ion( 505)/"ArIII   "/
      DATA ion( 506)/"ArIII   "/
      DATA ion( 507)/"ArIV    "/
      DATA ion( 508)/"ArIV    "/
      DATA ion( 509)/"MgVI    "/
      DATA ion( 510)/"ArIV    "/
      DATA ion( 511)/"NeVI    "/
      DATA ion( 512)/"MgVI    "/
      DATA ion( 513)/"NeVI    "/
      DATA ion( 515)/"MgVI    "/
      DATA ion( 516)/"CaIII   "/
      DATA ion( 517)/"NeII    "/
      DATA ion( 518)/"NeII    "/
      DATA ion( 519)/"CaVII   "/
      DATA ion( 520)/"SiIII   "/
      DATA ion( 521)/"NaIV    "/
      DATA ion( 522)/"NaIV    "/
      DATA ion( 523)/"NaVIII  "/
      DATA ion( 524)/"CrX     "/
      DATA ion( 526)/"CrXIV   "/
      DATA ion( 527)/"CrXI    "/
      DATA ion( 528)/"CrVIII  "/
      DATA ion( 529)/"NaVI    "/
      DATA ion( 530)/"ClXV    "/
      DATA ion( 532)/"CrX     "/
      DATA ion( 533)/"FeXV    "/
      DATA ion( 534)/"S XIV   "/
      DATA ion( 535)/"CrIX    "/
      DATA ion( 536)/"O II    "/
      DATA ion( 537)/"O II    "/
      DATA ion( 538)/"O II    "/
      DATA ion( 539)/"F IV    "/
      DATA ion( 541)/"V XIII  "/
      DATA ion( 543)/"TiIV    "/
      DATA ion( 544)/"SiIII   "/
      DATA ion( 546)/"N II    "/
      DATA ion( 547)/"SiIII   "/
      DATA ion( 548)/"N II    "/
      DATA ion( 549)/"CrX     "/
      DATA ion( 550)/"N II    "/
      DATA ion( 551)/"CaVIII  "/
      DATA ion( 552)/"N II    "/
      DATA ion( 553)/"MgVII   "/
      DATA ion( 555)/"N II    "/
      DATA ion( 556)/"O II    "/
      DATA ion( 557)/"O II    "/
      DATA ion( 558)/"O II    "/
      DATA ion( 559)/"MgVIII  "/
      DATA ion( 560)/"F II    "/
      DATA ion( 563)/"N II    "/
      DATA ion( 564)/"N II    "/
      DATA ion( 566)/"CaVIII  "/
      DATA ion( 568)/"NeVI    "/
      DATA ion( 569)/"TiIX    "/
      DATA ion( 571)/"CaIV    "/
      DATA ion( 572)/"N II    "/
      DATA ion( 573)/"N II    "/
      DATA ion( 574)/"CrV     "/
      DATA ion( 576)/"N II    "/
      DATA ion( 577)/"SiIII   "/
      DATA ion( 578)/"CrV     "/
      DATA ion( 580)/"C II    "/
      DATA ion( 581)/"C II    "/
      DATA ion( 582)/"N II    "/
      DATA ion( 583)/"CaIII   "/
      DATA ion( 584)/"N II    "/
      DATA ion( 585)/"N II    "/
      DATA ion( 587)/"NeII    "/
      DATA ion( 588)/"S XIV   "/
      DATA ion( 589)/"ArV     "/
      DATA ion( 590)/"NeII    "/
      DATA ion( 592)/"N II    "/
      DATA ion( 593)/"N II    "/
      DATA ion( 595)/"ArIV    "/
      DATA ion( 596)/"N III   "/
      DATA ion( 597)/"ArIV    "/
      DATA ion( 600)/"N II    "/
      DATA ion( 601)/"ArIV    "/
      DATA ion( 602)/"N II    "/
      DATA ion( 606)/"FeIII   "/
      DATA ion( 607)/"P XIII  "/
      DATA ion( 608)/"FeIII   "/
      DATA ion( 611)/"ArVI    "/
      DATA ion( 612)/"SiIV    "/
      DATA ion( 613)/"ArV     "/
      DATA ion( 614)/"SiIV    "/
      DATA ion( 616)/"NaV     "/
      DATA ion( 617)/"NeII    "/
      DATA ion( 618)/"TiXII   "/
      DATA ion( 619)/"NaV     "/
      DATA ion( 620)/"CaVIII  "/
      DATA ion( 621)/"NaV     "/
      DATA ion( 622)/"F V     "/
      DATA ion( 623)/"ArII    "/
      DATA ion( 624)/"NeVII   "/
      DATA ion( 625)/"F V     "/
      DATA ion( 626)/"ArII    "/
      DATA ion( 627)/"SiIII   "/
      DATA ion( 628)/"CaIX    "/
      DATA ion( 629)/"C II    "/
      DATA ion( 630)/"C II    "/
      DATA ion( 631)/"ArIII   "/
      DATA ion( 632)/"N II    "/
      DATA ion( 634)/"ArIII   "/
      DATA ion( 635)/"ArII    "/
      DATA ion( 636)/"ArII    "/
      DATA ion( 637)/"TiX     "/
      DATA ion( 641)/"ArIII   "/
      DATA ion( 642)/"ArIII   "/
      DATA ion( 643)/"N II    "/
      DATA ion( 645)/"N II    "/
      DATA ion( 646)/"ArII    "/
      DATA ion( 647)/"S III   "/
      DATA ion( 648)/"ArIII   "/
      DATA ion( 649)/"ArII    "/
      DATA ion( 650)/"ArII    "/
      DATA ion( 651)/"ArII    "/
      DATA ion( 652)/"TiXII   "/
      DATA ion( 653)/"ArII    "/
      DATA ion( 654)/"NeV     "/
      DATA ion( 655)/"P XIII  "/
      DATA ion( 656)/"S III   "/
      DATA ion( 658)/"ArIII   "/
      DATA ion( 660)/"FeIII   "/
      DATA ion( 661)/"FeIII   "/
      DATA ion( 662)/"FeIII   "/
      DATA ion( 663)/"ArIII   "/
      DATA ion( 664)/"S III   "/
      DATA ion( 665)/"FeIII   "/
      DATA ion( 666)/"FeIII   "/
      DATA ion( 667)/"NaVII   "/
      DATA ion( 668)/"AlIII   "/
      DATA ion( 669)/"AlIII   "/
      DATA ion( 671)/"ArII    "/
      DATA ion( 672)/"ArII    "/
      DATA ion( 676)/"NeIII   "/
      DATA ion( 677)/"ArIII   "/
      DATA ion( 678)/"ArII    "/
      DATA ion( 679)/"ArII    "/
      DATA ion( 680)/"ArII    "/
      DATA ion( 681)/"NeIII   "/
      DATA ion( 682)/"NaVI    "/
      DATA ion( 684)/"S XIII  "/
      DATA ion( 685)/"FeIII   "/
      DATA ion( 686)/"FeIII   "/
      DATA ion( 687)/"FeIII   "/
      DATA ion( 688)/"ArII    "/
      DATA ion( 689)/"SiXII   "/
      DATA ion( 690)/"TiIX    "/
      DATA ion( 691)/"ArII    "/
      DATA ion( 694)/"ArII    "/
      DATA ion( 695)/"ArII    "/
      DATA ion( 697)/"FeIII   "/
      DATA ion( 698)/"FeIII   "/
      DATA ion( 699)/"FeIII   "/
      DATA ion( 701)/"TiVIII  "/
      DATA ion( 704)/"HeI     "/
      DATA ion( 705)/"HeI     "/
      DATA ion( 706)/"HeI     "/
      DATA ion( 707)/"O III   "/
      DATA ion( 708)/"HeI     "/
      DATA ion( 710)/"ArIII   "/
      DATA ion( 711)/"TiVI    "/
      DATA ion( 712)/"ArIII   "/
      DATA ion( 713)/"HeI     "/
      DATA ion( 714)/"ArIII   "/
      DATA ion( 715)/"N II    "/
      DATA ion( 716)/"HeI     "/
      DATA ion( 717)/"ArII    "/
      DATA ion( 718)/"ArII    "/
      DATA ion( 719)/"AlIII   "/
      DATA ion( 720)/"AlIII   "/
      DATA ion( 721)/"HeI     "/
      DATA ion( 722)/"TiVIII  "/
      DATA ion( 724)/"HeI     "/
      DATA ion( 727)/"ArII    "/
      DATA ion( 728)/"ArII    "/
      DATA ion( 729)/"K VIII  "/
      DATA ion( 730)/"SiXII   "/
      DATA ion( 732)/"ArV     "/
      DATA ion( 733)/"HeI     "/
      DATA ion( 736)/"ArII    "/
      DATA ion( 737)/"FeIV    "/
      DATA ion( 738)/"FeIV    "/
      DATA ion( 739)/"ArII    "/
      DATA ion( 740)/"FeIV    "/
      DATA ion( 741)/"N II    "/
      DATA ion( 742)/"ArIII   "/
      DATA ion( 743)/"C II    "/
      DATA ion( 744)/"ArIII   "/
      DATA ion( 746)/"ArIII   "/
      DATA ion( 747)/"N II    "/
      DATA ion( 749)/"F VI    "/
      DATA ion( 750)/"HeI     "/
      DATA ion( 751)/"C II    "/
      DATA ion( 752)/"O II    "/
      DATA ion( 753)/"CaVII   "/
      DATA ion( 754)/"O II    "/
      DATA ion( 755)/"O II    "/
      DATA ion( 756)/"NeIV    "/
      DATA ion( 757)/"FeIII   "/
      DATA ion( 758)/"FeIII   "/
      DATA ion( 759)/"FeIII   "/
      DATA ion( 760)/"NeIV    "/
      DATA ion( 761)/"ArII    "/
      DATA ion( 762)/"C II    "/
      DATA ion( 763)/"ArII    "/
      DATA ion( 764)/"NeIV    "/
      DATA ion( 765)/"ArVI    "/
      DATA ion( 766)/"ArII    "/
      DATA ion( 769)/"ArVI    "/
      DATA ion( 770)/"C II    "/
      DATA ion( 771)/"C II    "/
      DATA ion( 772)/"AlXI    "/
      DATA ion( 773)/"S IV    "/
      DATA ion( 774)/"C II    "/
      DATA ion( 775)/"O IV    "/
      DATA ion( 776)/"ArIII   "/
      DATA ion( 777)/"O IV    "/
      DATA ion( 781)/"CaX     "/
      DATA ion( 783)/"NeVI    "/
      DATA ion( 784)/"S II    "/
      DATA ion( 785)/"S II    "/
      DATA ion( 786)/"S II    "/
      DATA ion( 787)/"C II    "/
      DATA ion( 788)/"AlIII   "/
      DATA ion( 789)/"AlIII   "/
      DATA ion( 791)/"SiIII   "/
      DATA ion( 792)/"AlXI    "/
      DATA ion( 793)/"NeV     "/
      DATA ion( 795)/"F IV    "/
      DATA ion( 796)/"S II    "/
      DATA ion( 798)/"S II    "/
      DATA ion( 799)/"S II    "/
      DATA ion( 800)/"ArII    "/
      DATA ion( 802)/"ArII    "/
      DATA ion( 803)/"CaX     "/
      DATA ion( 804)/"S II    "/
      DATA ion( 805)/"S II    "/
      DATA ion( 806)/"C II    "/
      DATA ion( 807)/"S II    "/
      DATA ion( 808)/"ArII    "/
      DATA ion( 809)/"ArII    "/
      DATA ion( 810)/"SiXI    "/
      DATA ion( 811)/"CaVIII  "/
      DATA ion( 812)/"HeI     "/
      DATA ion( 813)/"ArVII   "/
      DATA ion( 815)/"F I     "/
      DATA ion( 816)/"ArVI    "/
      DATA ion( 817)/"S II    "/
      DATA ion( 818)/"S II    "/
      DATA ion( 819)/"FeIII   "/
      DATA ion( 820)/"S II    "/
      DATA ion( 821)/"C II    "/
      DATA ion( 822)/"ArII    "/
      DATA ion( 823)/"S II    "/
      DATA ion( 824)/"S II    "/
      DATA ion( 825)/"S II    "/
      DATA ion( 827)/"F II    "/
      DATA ion( 830)/"O IV    "/
      DATA ion( 831)/"MgX     "/
      DATA ion( 832)/"NeI     "/
      DATA ion( 834)/"NeI     "/
      DATA ion( 835)/"NeI     "/
      DATA ion( 836)/"K IX    "/
      DATA ion( 837)/"CaVII   "/
      DATA ion( 838)/"MgX     "/
      DATA ion( 839)/"NeI     "/
      DATA ion( 841)/"CaVI    "/
      DATA ion( 842)/"O V     "/
      DATA ion( 843)/"NeI     "/
      DATA ion( 844)/"CaVI    "/
      DATA ion( 845)/"C II    "/
      DATA ion( 846)/"K IX    "/
      DATA ion( 849)/"ArIII   "/
      DATA ion( 850)/"CaV     "/
      DATA ion( 852)/"S II    "/
      DATA ion( 853)/"S II    "/
      DATA ion( 854)/"S II    "/
      DATA ion( 855)/"CaVI    "/
      DATA ion( 856)/"N II    "/
      DATA ion( 857)/"CaV     "/
      DATA ion( 858)/"F V     "/
      DATA ion( 859)/"CaIV    "/
      DATA ion( 861)/"F III   "/
      DATA ion( 862)/"F III   "/
      DATA ion( 863)/"S IV    "/
      DATA ion( 864)/"K VII   "/
      DATA ion( 865)/"ArII    "/
      DATA ion( 866)/"S II    "/
      DATA ion( 867)/"S II    "/
      DATA ion( 868)/"ArII    "/
      DATA ion( 869)/"S II    "/
      DATA ion( 871)/"ClVI    "/
      DATA ion( 872)/"N II    "/
      DATA ion( 873)/"ArII    "/
      DATA ion( 874)/"ArII    "/
      DATA ion( 876)/"F IV    "/
      DATA ion( 877)/"S III   "/
      DATA ion( 878)/"AlII    "/
      DATA ion( 880)/"S III   "/
      DATA ion( 881)/"NaIX    "/
      DATA ion( 884)/"AlII    "/
      DATA ion( 885)/"N III   "/
      DATA ion( 886)/"N III   "/
      DATA ion( 888)/"C II    "/
      DATA ion( 889)/"AlII    "/
      DATA ion( 892)/"NaIX    "/
      DATA ion( 893)/"AlIII   "/
      DATA ion( 894)/"AlIII   "/
      DATA ion( 895)/"S III   "/
      DATA ion( 896)/"AlII    "/
      DATA ion( 897)/"ArVIII  "/
      DATA ion( 898)/"O III   "/
      DATA ion( 899)/"ArV     "/
      DATA ion( 901)/"K VI    "/
      DATA ion( 902)/"ArVIII  "/
      DATA ion( 903)/"AlII    "/
      DATA ion( 904)/"ArII    "/
      DATA ion( 905)/"K V     "/
      DATA ion( 906)/"ArII    "/
      DATA ion( 907)/"S III   "/
      DATA ion( 908)/"K V     "/
      DATA ion( 912)/"FeIII   "/
      DATA ion( 913)/"FeIII   "/
      DATA ion( 915)/"K V     "/
      DATA ion( 918)/"FeIII   "/
      DATA ion( 922)/"FeIII   "/
      DATA ion( 923)/"FeIII   "/
      DATA ion( 925)/"NeI     "/
      DATA ion( 926)/"K IV    "/
      DATA ion( 928)/"O I     "/
      DATA ion( 929)/"O I     "/
      DATA ion( 930)/"O I     "/
      DATA ion( 931)/"O I     "/
      DATA ion( 932)/"ArII    "/
      DATA ion( 933)/"O I     "/
      DATA ion( 934)/"O I     "/
      DATA ion( 935)/"AlII    "/
      DATA ion( 937)/"NeI     "/
      DATA ion( 938)/"O I     "/
      DATA ion( 939)/"O I     "/
      DATA ion( 940)/"S IV    "/
      DATA ion( 941)/"K IV    "/
      DATA ion( 945)/"O I     "/
      DATA ion( 947)/"O I     "/
      DATA ion( 948)/"S IV    "/
      DATA ion( 952)/"SiII    "/
      DATA ion( 953)/"SiII    "/
      DATA ion( 954)/"ArVI    "/
      DATA ion( 955)/"O I     "/
      DATA ion( 956)/"O I     "/
      DATA ion( 958)/"N III   "/
      DATA ion( 959)/"S II    "/
      DATA ion( 960)/"S II    "/
      DATA ion( 961)/"N IV    "/
      DATA ion( 963)/"K III   "/
      DATA ion( 964)/"S II    "/
      DATA ion( 966)/"O I     "/
      DATA ion( 967)/"O I     "/
      DATA ion( 968)/"NeVIII  "/
      DATA ion( 976)/"NeVIII  "/
      DATA ion( 979)/"TiIV    "/
      DATA ion( 980)/"S V     "/
      DATA ion( 981)/"O IV    "/
      DATA ion( 984)/"O I     "/
      DATA ion( 986)/"O I     "/
      DATA ion( 989)/"AlII    "/
      DATA ion( 990)/"ClVII   "/
      DATA ion( 991)/"SiII    "/
      DATA ion( 992)/"SiII    "/
      DATA ion( 993)/"F I     "/
      DATA ion( 995)/"FeIII   "/
      DATA ion( 996)/"S IV    "/
      DATA ion( 998)/"O I     "/
      DATA ion( 999)/"O I     "/
      DATA ion(1000)/"ClVII   "/
      DATA ion(1001)/"FeIII   "/
      DATA ion(1002)/"FeIII   "/
      DATA ion(1003)/"SiII    "/
      DATA ion(1004)/"SiII    "/
      DATA ion(1005)/"ArV     "/
      DATA ion(1006)/"FeIII   "/
      DATA ion(1007)/"FeIII   "/
      DATA ion(1008)/"FeIII   "/
      DATA ion(1010)/"O II    "/
      DATA ion(1011)/"O III   "/
      DATA ion(1012)/"O II    "/
      DATA ion(1013)/"O II    "/
      DATA ion(1014)/"ArIV    "/
      DATA ion(1017)/"SiII    "/
      DATA ion(1018)/"ArIV    "/
      DATA ion(1019)/"FeIII   "/
      DATA ion(1020)/"SiII    "/
      DATA ion(1021)/"ArIV    "/
      DATA ion(1022)/"FeIII   "/
      DATA ion(1023)/"FeIII   "/
      DATA ion(1024)/"C II    "/
      DATA ion(1025)/"FeIII   "/
      DATA ion(1026)/"FeIII   "/
      DATA ion(1027)/"FeIII   "/
      DATA ion(1028)/"N I     "/
      DATA ion(1029)/"N I     "/
      DATA ion(1030)/"N I     "/
      DATA ion(1031)/"N I     "/
      DATA ion(1032)/"N I     "/
      DATA ion(1033)/"N I     "/
      DATA ion(1034)/"N I     "/
      DATA ion(1035)/"N I     "/
      DATA ion(1036)/"N I     "/
      DATA ion(1037)/"N I     "/
      DATA ion(1038)/"ArI     "/
      DATA ion(1040)/"N I     "/
      DATA ion(1041)/"N I     "/
      DATA ion(1042)/"N I     "/
      DATA ion(1043)/"ArI     "/
      DATA ion(1044)/"N I     "/
      DATA ion(1045)/"MgII    "/
      DATA ion(1046)/"MgII    "/
      DATA ion(1047)/"N I     "/
      DATA ion(1048)/"ArIII   "/
      DATA ion(1050)/"N I     "/
      DATA ion(1051)/"N I     "/
      DATA ion(1052)/"N I     "/
      DATA ion(1053)/"ArI     "/
      DATA ion(1054)/"N I     "/
      DATA ion(1055)/"N I     "/
      DATA ion(1056)/"N I     "/
      DATA ion(1057)/"O I     "/
      DATA ion(1058)/"O I     "/
      DATA ion(1059)/"ArIII   "/
      DATA ion(1060)/"ArI     "/
      DATA ion(1061)/"F VII   "/
      DATA ion(1062)/"ClV     "/
      DATA ion(1063)/"MgII    "/
      DATA ion(1064)/"MgII    "/
      DATA ion(1065)/"N I     "/
      DATA ion(1066)/"N I     "/
      DATA ion(1067)/"N I     "/
      DATA ion(1068)/"N I     "/
      DATA ion(1069)/"N I     "/
      DATA ion(1070)/"N I     "/
      DATA ion(1071)/"SiII    "/
      DATA ion(1072)/"F VII   "/
      DATA ion(1074)/"SiII    "/
      DATA ion(1075)/"C II    "/
      DATA ion(1076)/"C II    "/
      DATA ion(1077)/"N I     "/
      DATA ion(1078)/"N I     "/
      DATA ion(1079)/"N I     "/
      DATA ion(1080)/"S II    "/
      DATA ion(1081)/"MgII    "/
      DATA ion(1082)/"MgII    "/
      DATA ion(1083)/"N I     "/
      DATA ion(1084)/"N I     "/
      DATA ion(1085)/"S II    "/
      DATA ion(1086)/"N I     "/
      DATA ion(1088)/"S II    "/
      DATA ion(1089)/"H I     "/
      DATA ion(1090)/"H I     "/
      DATA ion(1091)/"H I     "/
      DATA ion(1092)/"H I     "/
      DATA ion(1093)/"H I     "/
      DATA ion(1094)/"H I     "/
      DATA ion(1095)/"H I     "/
      DATA ion(1096)/"H I     "/
      DATA ion(1097)/"H I     "/
      DATA ion(1098)/"H I     "/
      DATA ion(1099)/"P III   "/
      DATA ion(1100)/"H I     "/
      DATA ion(1101)/"H I     "/
      DATA ion(1102)/"H I     "/
      DATA ion(1103)/"H I     "/
      DATA ion(1104)/"H I     "/
      DATA ion(1105)/"N II    "/
      DATA ion(1106)/"H I     "/
      DATA ion(1107)/"H I     "/
      DATA ion(1108)/"O I     "/
      DATA ion(1109)/"O I     "/
      DATA ion(1110)/"O I     "/
      DATA ion(1111)/"P III   "/
      DATA ion(1112)/"H I     "/
      DATA ion(1113)/"O I     "/
      DATA ion(1114)/"O I     "/
      DATA ion(1115)/"O I     "/
      DATA ion(1116)/"H I     "/
      DATA ion(1117)/"CrIII   "/
      DATA ion(1118)/"H I     "/
      DATA ion(1119)/"O I     "/
      DATA ion(1120)/"O I     "/
      DATA ion(1121)/"O I     "/
      DATA ion(1122)/"ArII    "/
      DATA ion(1123)/"O I     "/
      DATA ion(1124)/"H I     "/
      DATA ion(1125)/"FeII    "/
      DATA ion(1126)/"O I     "/
      DATA ion(1127)/"O I     "/
      DATA ion(1128)/"O I     "/
      DATA ion(1129)/"O I     "/
      DATA ion(1130)/"H I     "/
      DATA ion(1131)/"CrIII   "/
      DATA ion(1132)/"FeII    "/
      DATA ion(1133)/"O I     "/
      DATA ion(1134)/"O I     "/
      DATA ion(1135)/"O I     "/
      DATA ion(1136)/"O I     "/
      DATA ion(1137)/"FeII    "/
      DATA ion(1138)/"H I     "/
      DATA ion(1139)/"FeII    "/
      DATA ion(1141)/"FeII    "/
      DATA ion(1142)/"O I     "/
      DATA ion(1143)/"O I     "/
      DATA ion(1144)/"O I     "/
      DATA ion(1146)/"O I     "/
      DATA ion(1147)/"H I     "/
      DATA ion(1148)/"FeII    "/
      DATA ion(1149)/"NiII    "/
      DATA ion(1150)/"S VI    "/
      DATA ion(1152)/"AlII    "/
      DATA ion(1153)/"FeII    "/
      DATA ion(1154)/"FeII    "/
      DATA ion(1155)/"O I     "/
      DATA ion(1156)/"O I     "/
      DATA ion(1157)/"O I     "/
      DATA ion(1158)/"CoIII   "/
      DATA ion(1159)/"FeII    "/
      DATA ion(1160)/"H I     "/
      DATA ion(1161)/"O I     "/
      DATA ion(1163)/"FeII    "/
      DATA ion(1164)/"CoIII   "/
      DATA ion(1165)/"FeII    "/
      DATA ion(1166)/"FeII    "/
      DATA ion(1168)/"S VI    "/
      DATA ion(1169)/"NiII    "/
      DATA ion(1170)/"NiII    "/
      DATA ion(1171)/"C I     "/
      DATA ion(1173)/"MgII    "/
      DATA ion(1174)/"MgII    "/
      DATA ion(1176)/"O I     "/
      DATA ion(1177)/"O I     "/
      DATA ion(1178)/"O I     "/
      DATA ion(1179)/"H I     "/
      DATA ion(1180)/"P IV    "/
      DATA ion(1181)/"O I     "/
      DATA ion(1182)/"N I     "/
      DATA ion(1183)/"N I     "/
      DATA ion(1186)/"N I     "/
      DATA ion(1187)/"N I     "/
      DATA ion(1188)/"N I     "/
      DATA ion(1189)/"N I     "/
      DATA ion(1190)/"N I     "/
      DATA ion(1191)/"N I     "/
      DATA ion(1192)/"N I     "/
      DATA ion(1193)/"F I     "/
      DATA ion(1194)/"N I     "/
      DATA ion(1196)/"N I     "/
      DATA ion(1198)/"N I     "/
      DATA ion(1200)/"N I     "/
      DATA ion(1202)/"P II    "/
      DATA ion(1203)/"P II    "/
      DATA ion(1204)/"N I     "/
      DATA ion(1205)/"N I     "/
      DATA ion(1206)/"N I     "/
      DATA ion(1210)/"O I     "/
      DATA ion(1211)/"O I     "/
      DATA ion(1212)/"O I     "/
      DATA ion(1214)/"O I     "/
      DATA ion(1215)/"O I     "/
      DATA ion(1217)/"H I     "/
      DATA ion(1219)/"ClIV    "/
      DATA ion(1222)/"O I     "/
      DATA ion(1223)/"O I     "/
      DATA ion(1225)/"C III   "/
      DATA ion(1228)/"ClI     "/
      DATA ion(1230)/"ClI     "/
      DATA ion(1231)/"O I     "/
      DATA ion(1232)/"O I     "/
      DATA ion(1233)/"O I     "/
      DATA ion(1234)/"N III   "/
      DATA ion(1235)/"SiII    "/
      DATA ion(1238)/"ClI     "/
      DATA ion(1241)/"P III   "/
      DATA ion(1245)/"ClI     "/
      DATA ion(1251)/"ClI     "/
      DATA ion(1253)/"ClIII   "/
      DATA ion(1257)/"ClIII   "/
      DATA ion(1261)/"S III   "/
      DATA ion(1263)/"FeII    "/
      DATA ion(1265)/"FeII    "/
      DATA ion(1266)/"ClIII   "/
      DATA ion(1270)/"SiII    "/
      DATA ion(1273)/"ClI     "/
      DATA ion(1275)/"H I     "/
      DATA ion(1276)/"O I     "/
      DATA ion(1277)/"O I     "/
      DATA ion(1278)/"O I     "/
      DATA ion(1279)/"MgII    "/
      DATA ion(1280)/"MgII    "/
      DATA ion(1281)/"O I     "/
      DATA ion(1282)/"O I     "/
      DATA ion(1283)/"O I     "/
      DATA ion(1286)/"ClI     "/
      DATA ion(1288)/"CrIII   "/
      DATA ion(1290)/"ClI     "/
      DATA ion(1291)/"ClI     "/
      DATA ion(1293)/"O VI    "/
      DATA ion(1295)/"CrIII   "/
      DATA ion(1298)/"C II    "/
      DATA ion(1299)/"O VI    "/
      DATA ion(1303)/"O I     "/
      DATA ion(1304)/"FeII    "/
      DATA ion(1305)/"CrIII   "/
      DATA ion(1306)/"ClI     "/
      DATA ion(1308)/"ClI     "/
      DATA ion(1309)/"ClI     "/
      DATA ion(1313)/"ArI     "/
      DATA ion(1314)/"S I     "/
      DATA ion(1316)/"S I     "/
      DATA ion(1322)/"FeII    "/
      DATA ion(1327)/"FeII    "/
      DATA ion(1328)/"CrII    "/
      DATA ion(1329)/"S IV    "/
      DATA ion(1331)/"FeII    "/
      DATA ion(1332)/"CrII    "/
      DATA ion(1333)/"ClII    "/
      DATA ion(1334)/"FeII    "/
      DATA ion(1335)/"CrII    "/
      DATA ion(1336)/"ArI     "/
      DATA ion(1339)/"ClII    "/
      DATA ion(1342)/"CrII    "/
      DATA ion(1345)/"FeII    "/
      DATA ion(1346)/"FeII    "/
      DATA ion(1347)/"CrII    "/
      DATA ion(1348)/"FeII    "/
      DATA ion(1350)/"N II    "/
      DATA ion(1352)/"ClI     "/
      DATA ion(1354)/"FeII    "/
      DATA ion(1355)/"FeII    "/
      DATA ion(1359)/"FeII    "/
      DATA ion(1361)/"ClI     "/
      DATA ion(1362)/"GeIII   "/
      DATA ion(1366)/"ClI     "/
      DATA ion(1367)/"S I     "/
      DATA ion(1368)/"S I     "/
      DATA ion(1370)/"ClI     "/
      DATA ion(1371)/"FeII    "/
      DATA ion(1372)/"ClI     "/
      DATA ion(1377)/"ClI     "/
      DATA ion(1378)/"C I     "/
      DATA ion(1379)/"C I     "/
      DATA ion(1380)/"C I     "/
      DATA ion(1381)/"C I     "/
      DATA ion(1383)/"C I     "/
      DATA ion(1384)/"C I     "/
      DATA ion(1385)/"FeII    "/
      DATA ion(1387)/"V II    "/
      DATA ion(1388)/"C I     "/
      DATA ion(1389)/"C I     "/
      DATA ion(1391)/"FeII    "/
      DATA ion(1392)/"C I     "/
      DATA ion(1394)/"C I     "/
      DATA ion(1395)/"FeII    "/
      DATA ion(1396)/"C I     "/
      DATA ion(1399)/"C I     "/
      DATA ion(1400)/"FeII    "/
      DATA ion(1401)/"C I     "/
      DATA ion(1402)/"C I     "/
      DATA ion(1403)/"C I     "/
      DATA ion(1404)/"C I     "/
      DATA ion(1405)/"C I     "/
      DATA ion(1407)/"C I     "/
      DATA ion(1408)/"C I     "/
      DATA ion(1409)/"P V     "/
      DATA ion(1412)/"C I     "/
      DATA ion(1413)/"C I     "/
      DATA ion(1414)/"FeII    "/
      DATA ion(1415)/"C I     "/
      DATA ion(1416)/"C I     "/
      DATA ion(1417)/"FeIII   "/
      DATA ion(1422)/"FeII    "/
      DATA ion(1423)/"FeII    "/
      DATA ion(1424)/"P V     "/
      DATA ion(1425)/"C I     "/
      DATA ion(1426)/"C I     "/
      DATA ion(1427)/"C I     "/
      DATA ion(1430)/"C I     "/
      DATA ion(1432)/"C I     "/
      DATA ion(1434)/"FeII    "/
      DATA ion(1436)/"N I     "/
      DATA ion(1437)/"N I     "/
      DATA ion(1438)/"N I     "/
      DATA ion(1441)/"C I     "/
      DATA ion(1442)/"C I     "/
      DATA ion(1443)/"C I     "/
      DATA ion(1444)/"C I     "/
      DATA ion(1445)/"C I     "/
      DATA ion(1446)/"FeII    "/
      DATA ion(1447)/"FeII    "/
      DATA ion(1448)/"FeII    "/
      DATA ion(1457)/"P II    "/
      DATA ion(1458)/"V III   "/
      DATA ion(1459)/"C I     "/
      DATA ion(1460)/"S I     "/
      DATA ion(1461)/"C I     "/
      DATA ion(1462)/"S I     "/
      DATA ion(1463)/"C I     "/
      DATA ion(1464)/"C I     "/
      DATA ion(1465)/"C I     "/
      DATA ion(1466)/"N I     "/
      DATA ion(1468)/"MnII    "/
      DATA ion(1472)/"MnII    "/
      DATA ion(1473)/"MnII    "/
      DATA ion(1475)/"ClI     "/
      DATA ion(1478)/"CoII    "/
      DATA ion(1480)/"ClI     "/
      DATA ion(1481)/"ClI     "/
      DATA ion(1482)/"C I     "/
      DATA ion(1483)/"GeIV    "/
      DATA ion(1484)/"C I     "/
      DATA ion(1485)/"S III   "/
      DATA ion(1486)/"SiII    "/
      DATA ion(1489)/"C I     "/
      DATA ion(1491)/"C I     "/
      DATA ion(1492)/"SiII    "/
      DATA ion(1494)/"C I     "/
      DATA ion(1495)/"MnII    "/
      DATA ion(1497)/"S V     "/
      DATA ion(1498)/"MnII    "/
      DATA ion(1499)/"N I     "/
      DATA ion(1500)/"N I     "/
      DATA ion(1501)/"N I     "/
      DATA ion(1502)/"MnII    "/
      DATA ion(1508)/"SiIII   "/
      DATA ion(1516)/"S I     "/
      DATA ion(1517)/"S I     "/
      DATA ion(1518)/"FeIII   "/
      DATA ion(1519)/"H I     "/
      DATA ion(1520)/"S I     "/
      DATA ion(1521)/"P I     "/
      DATA ion(1522)/"O V     "/
      DATA ion(1524)/"S I     "/
      DATA ion(1525)/"S I     "/
      DATA ion(1529)/"S I     "/
      DATA ion(1531)/"S I     "/
      DATA ion(1532)/"S I     "/
      DATA ion(1533)/"CaII    "/
      DATA ion(1534)/"P I     "/
      DATA ion(1535)/"CaII    "/
      DATA ion(1536)/"S I     "/
      DATA ion(1537)/"GeIV    "/
      DATA ion(1539)/"S I     "/
      DATA ion(1540)/"S I     "/
      DATA ion(1544)/"N V     "/
      DATA ion(1545)/"MgII    "/
      DATA ion(1546)/"MgII    "/
      DATA ion(1547)/"S I     "/
      DATA ion(1548)/"N V     "/
      DATA ion(1551)/"S I     "/
      DATA ion(1553)/"S II    "/
      DATA ion(1555)/"S II    "/
      DATA ion(1556)/"SiI     "/
      DATA ion(1557)/"S II    "/
      DATA ion(1558)/"SiII    "/
      DATA ion(1559)/"FeII    "/
      DATA ion(1560)/"C I     "/
      DATA ion(1561)/"S I     "/
      DATA ion(1562)/"C I     "/
      DATA ion(1564)/"S I     "/
      DATA ion(1565)/"S I     "/
      DATA ion(1566)/"C I     "/
      DATA ion(1567)/"C I     "/
      DATA ion(1568)/"C I     "/
      DATA ion(1569)/"P I     "/
      DATA ion(1571)/"P I     "/
      DATA ion(1575)/"S I     "/
      DATA ion(1576)/"TiIII   "/
      DATA ion(1577)/"S I     "/
      DATA ion(1578)/"TiIII   "/
      DATA ion(1579)/"P II    "/
      DATA ion(1580)/"O I     "/
      DATA ion(1581)/"S I     "/
      DATA ion(1582)/"SiII    "/
      DATA ion(1583)/"NiII    "/
      DATA ion(1584)/"P I     "/
      DATA ion(1585)/"S I     "/
      DATA ion(1586)/"S I     "/
      DATA ion(1587)/"S I     "/
      DATA ion(1588)/"NiII    "/
      DATA ion(1589)/"P I     "/
      DATA ion(1590)/"P I     "/
      DATA ion(1592)/"C I     "/
      DATA ion(1594)/"C II    "/
      DATA ion(1595)/"P III   "/
      DATA ion(1596)/"ClI     "/
      DATA ion(1597)/"CaII    "/
      DATA ion(1598)/"CaII    "/
      DATA ion(1600)/"NiII    "/
      DATA ion(1601)/"ClI     "/
      DATA ion(1602)/"O I     "/
      DATA ion(1603)/"CuII    "/
      DATA ion(1604)/"B II    "/
      DATA ion(1608)/"NiII    "/
      DATA ion(1610)/"P I     "/
      DATA ion(1612)/"P I     "/
      DATA ion(1613)/"P I     "/
      DATA ion(1617)/"P I     "/
      DATA ion(1619)/"P I     "/
      DATA ion(1620)/"S I     "/
      DATA ion(1621)/"P I     "/
      DATA ion(1622)/"NiII    "/
      DATA ion(1623)/"S I     "/
      DATA ion(1625)/"NiII    "/
      DATA ion(1626)/"SiIV    "/
      DATA ion(1632)/"S I     "/
      DATA ion(1633)/"SiIV    "/
      DATA ion(1635)/"NiII    "/
      DATA ion(1636)/"NiII    "/
      DATA ion(1637)/"GaII    "/
      DATA ion(1638)/"NiII    "/
      DATA ion(1640)/"CoII    "/
      DATA ion(1641)/"S I     "/
      DATA ion(1642)/"S I     "/
      DATA ion(1643)/"S I     "/
      DATA ion(1646)/"S I     "/
      DATA ion(1647)/"CoII    "/
      DATA ion(1648)/"NiII    "/
      DATA ion(1650)/"NiII    "/
      DATA ion(1651)/"CoII    "/
      DATA ion(1652)/"NiII    "/
      DATA ion(1654)/"NiII    "/
      DATA ion(1657)/"S I     "/
      DATA ion(1658)/"S I     "/
      DATA ion(1659)/"S I     "/
      DATA ion(1660)/"S I     "/
      DATA ion(1661)/"NiII    "/
      DATA ion(1662)/"CoII    "/
      DATA ion(1667)/"MnI     "/
      DATA ion(1668)/"GaIII   "/
      DATA ion(1669)/"NiII    "/
      DATA ion(1670)/"NiII    "/
      DATA ion(1681)/"SiII    "/
      DATA ion(1684)/"P II    "/
      DATA ion(1690)/"SiI     "/
      DATA ion(1691)/"SiI     "/
      DATA ion(1693)/"SiI     "/
      DATA ion(1696)/"SiI     "/
      DATA ion(1698)/"SiI     "/
      DATA ion(1699)/"SiI     "/
      DATA ion(1700)/"CoII    "/
      DATA ion(1701)/"C IV    "/
      DATA ion(1702)/"C IV    "/
      DATA ion(1703)/"SiI     "/
      DATA ion(1704)/"CoII    "/
      DATA ion(1705)/"SiI     "/
      DATA ion(1706)/"CoII    "/
      DATA ion(1707)/"SiI     "/
      DATA ion(1708)/"SiI     "/
      DATA ion(1711)/"C I     "/
      DATA ion(1712)/"SiI     "/
      DATA ion(1714)/"SiI     "/
      DATA ion(1718)/"SiI     "/
      DATA ion(1719)/"CoII    "/
      DATA ion(1720)/"SiI     "/
      DATA ion(1721)/"CoII    "/
      DATA ion(1722)/"SiI     "/
      DATA ion(1724)/"CoII    "/
      DATA ion(1728)/"SiI     "/
      DATA ion(1729)/"FeII    "/
      DATA ion(1730)/"SiI     "/
      DATA ion(1731)/"ZnI     "/
      DATA ion(1738)/"FeII    "/
      DATA ion(1742)/"FeII    "/
      DATA ion(1743)/"C I     "/
      DATA ion(1744)/"SiI     "/
      DATA ion(1745)/"FeII    "/
      DATA ion(1748)/"SiI     "/
      DATA ion(1751)/"SiI     "/
      DATA ion(1755)/"CaII    "/
      DATA ion(1756)/"MgI     "/
      DATA ion(1757)/"CaII    "/
      DATA ion(1758)/"C I     "/
      DATA ion(1759)/"MgI     "/
      DATA ion(1762)/"SiI     "/
      DATA ion(1764)/"MgI     "/
      DATA ion(1765)/"AlII    "/
      DATA ion(1766)/"P I     "/
      DATA ion(1767)/"P I     "/
      DATA ion(1768)/"P I     "/
      DATA ion(1769)/"MgI     "/
      DATA ion(1770)/"SiI     "/
      DATA ion(1771)/"SiI     "/
      DATA ion(1772)/"NiII    "/
      DATA ion(1773)/"MgI     "/
      DATA ion(1774)/"NiII    "/
      DATA ion(1775)/"NiII    "/
      DATA ion(1777)/"MgI     "/
      DATA ion(1780)/"NiII    "/
      DATA ion(1782)/"AlI     "/
      DATA ion(1783)/"SiI     "/
      DATA ion(1784)/"AlI     "/
      DATA ion(1786)/"NiII    "/
      DATA ion(1787)/"P I     "/
      DATA ion(1789)/"P I     "/
      DATA ion(1790)/"NiII    "/
      DATA ion(1791)/"P I     "/
      DATA ion(1792)/"NiII    "/
      DATA ion(1793)/"S I     "/
      DATA ion(1794)/"SiII    "/
      DATA ion(1797)/"SiI     "/
      DATA ion(1799)/"MgI     "/
      DATA ion(1801)/"SiI     "/
      DATA ion(1802)/"NiII    "/
      DATA ion(1803)/"SiI     "/
      DATA ion(1804)/"FeI     "/
      DATA ion(1805)/"FeI     "/
      DATA ion(1806)/"AlIII   "/
      DATA ion(1807)/"FeI     "/
      DATA ion(1808)/"AlIII   "/
      DATA ion(1810)/"FeI     "/
      DATA ion(1811)/"FeI     "/
      DATA ion(1812)/"FeI     "/
      DATA ion(1815)/"SiIII   "/
      DATA ion(1816)/"NiII    "/
      DATA ion(1818)/"S I     "/
      DATA ion(1819)/"FeII    "/
      DATA ion(1820)/"TiII    "/
      DATA ion(1821)/"TiII    "/
      DATA ion(1823)/"TiII    "/
      DATA ion(1824)/"TiII    "/
      DATA ion(1825)/"FeI     "/
      DATA ion(1827)/"AlI     "/
      DATA ion(1828)/"FeI     "/
      DATA ion(1829)/"FeI     "/
      DATA ion(1832)/"CrI     "/
      DATA ion(1833)/"CrI     "/
      DATA ion(1834)/"CrI     "/
      DATA ion(1835)/"CoII    "/
      DATA ion(1836)/"FeI     "/
      DATA ion(1837)/"FeI     "/
      DATA ion(1839)/"FeI     "/
      DATA ion(1842)/"SiI     "/
      DATA ion(1844)/"MnI     "/
      DATA ion(1846)/"MnI     "/
      DATA ion(1848)/"MnI     "/
      DATA ion(1850)/"CoII    "/
      DATA ion(1856)/"ZnII    "/
      DATA ion(1857)/"CrII    "/
      DATA ion(1858)/"CoII    "/
      DATA ion(1859)/"MgI     "/
      DATA ion(1860)/"NiI     "/
      DATA ion(1861)/"CrII    "/
      DATA ion(1862)/"GeI     "/
      DATA ion(1864)/"CrII    "/
      DATA ion(1865)/"CoII    "/
      DATA ion(1867)/"CrII    "/
      DATA ion(1869)/"ZnII    "/
      DATA ion(1870)/"CrII    "/
      DATA ion(1876)/"FeI     "/
      DATA ion(1877)/"CaI     "/
      DATA ion(1878)/"FeI     "/
      DATA ion(1879)/"FeI     "/
      DATA ion(1883)/"CaI     "/
      DATA ion(1884)/"FeI     "/
      DATA ion(1885)/"FeI     "/
      DATA ion(1888)/"AlI     "/
      DATA ion(1889)/"CaI     "/
      DATA ion(1892)/"FeI     "/
      DATA ion(1893)/"AlI     "/
      DATA ion(1894)/"FeI     "/
      DATA ion(1895)/"V II    "/
      DATA ion(1896)/"ZnI     "/
      DATA ion(1897)/"FeI     "/
      DATA ion(1898)/"FeI     "/
      DATA ion(1900)/"AlI     "/
      DATA ion(1902)/"AlI     "/
      DATA ion(1903)/"CaI     "/
      DATA ion(1905)/"AlI     "/
      DATA ion(1906)/"CuI     "/
      DATA ion(1907)/"FeI     "/
      DATA ion(1909)/"AlI     "/
      DATA ion(1912)/"CuI     "/
      DATA ion(1913)/"CuI     "/
      DATA ion(1914)/"FeI     "/
      DATA ion(1916)/"AlI     "/
      DATA ion(1917)/"CaI     "/
      DATA ion(1919)/"AlI     "/
      DATA ion(1920)/"SiI     "/
      DATA ion(1921)/"MnI     "/
      DATA ion(1922)/"MnI     "/
      DATA ion(1923)/"MnI     "/
      DATA ion(1924)/"CuI     "/
      DATA ion(1933)/"FeII    "/
      DATA ion(1934)/"FeI     "/
      DATA ion(1936)/"NiI     "/
      DATA ion(1938)/"NiI     "/
      DATA ion(1939)/"AlI     "/
      DATA ion(1940)/"FeI     "/
      DATA ion(1941)/"FeII    "/
      DATA ion(1942)/"AlI     "/
      DATA ion(1943)/"FeI     "/
      DATA ion(1946)/"CaI     "/
      DATA ion(1947)/"FeI     "/
      DATA ion(1950)/"NiI     "/
      DATA ion(1952)/"FeI     "/
      DATA ion(1956)/"NiI     "/
      DATA ion(1958)/"NiI     "/
      DATA ion(1963)/"NiI     "/
      DATA ion(1964)/"FeII    "/
      DATA ion(1965)/"NiI     "/
      DATA ion(1966)/"NiI     "/
      DATA ion(1970)/"CrI     "/
      DATA ion(1971)/"CoI     "/
      DATA ion(1972)/"CrI     "/
      DATA ion(1973)/"CrI     "/
      DATA ion(1974)/"FeII    "/
      DATA ion(1975)/"AlI     "/
      DATA ion(1977)/"AlI     "/
      DATA ion(1979)/"FeII    "/
      DATA ion(1981)/"FeII    "/
      DATA ion(1982)/"CoI     "/
      DATA ion(1984)/"CaI     "/
      DATA ion(1985)/"CoI     "/
      DATA ion(1986)/"CoI     "/
      DATA ion(1991)/"FeI     "/
      DATA ion(1993)/"FeI     "/
      DATA ion(1994)/"FeI     "/
      DATA ion(1998)/"FeI     "/
      DATA ion(1999)/"FeI     "/
      DATA ion(2005)/"FeI     "/
      DATA ion(2009)/"SiI     "/
      DATA ion(2010)/"CoI     "/
      DATA ion(2011)/"FeI     "/
      DATA ion(2022)/"AlI     "/
      DATA ion(2024)/"MnII    "/
      DATA ion(2026)/"FeII    "/
      DATA ion(2027)/"MnII    "/
      DATA ion(2031)/"FeII    "/
      DATA ion(2032)/"MnII    "/
      DATA ion(2035)/"GeI     "/
      DATA ion(2036)/"AlI     "/
      DATA ion(2040)/"NaI     "/
      DATA ion(2041)/"NaI     "/
      DATA ion(2042)/"V II    "/
      DATA ion(2045)/"FeI     "/
      DATA ion(2046)/"CaI     "/
      DATA ion(2047)/"CaI     "/
      DATA ion(2049)/"V II    "/
      DATA ion(2053)/"MnI     "/
      DATA ion(2054)/"MgII    "/
      DATA ion(2055)/"MnI     "/
      DATA ion(2056)/"MnI     "/
      DATA ion(2057)/"MgII    "/
      DATA ion(2058)/"FeI     "/
      DATA ion(2060)/"FeI     "/
      DATA ion(2061)/"MgI     "/
      DATA ion(2062)/"NaI     "/
      DATA ion(2063)/"NaI     "/
      DATA ion(2065)/"FeI     "/
      DATA ion(2066)/"GaI     "/
      DATA ion(2067)/"FeI     "/
      DATA ion(2069)/"TiI     "/
      DATA ion(2070)/"FeI     "/
      DATA ion(2071)/"TiI     "/
      DATA ion(2073)/"FeI     "/
      DATA ion(2075)/"FeI     "/
      DATA ion(2076)/"NiI     "/
      DATA ion(2080)/"NiI     "/
      DATA ion(2081)/"FeI     "/
      DATA ion(2085)/"V I     "/
      DATA ion(2087)/"V I     "/
      DATA ion(2088)/"TiII    "/
      DATA ion(2090)/"TiII    "/
      DATA ion(2093)/"TiII    "/
      DATA ion(2096)/"AlI     "/
      DATA ion(2106)/"TiII    "/
      DATA ion(2113)/"V I     "/
      DATA ion(2114)/"TiI     "/
      DATA ion(2115)/"FeI     "/
      DATA ion(2116)/"FeI     "/
      DATA ion(2119)/"NiI     "/
      DATA ion(2122)/"TiII    "/
      DATA ion(2124)/"NiI     "/
      DATA ion(2125)/"TiII    "/
      DATA ion(2126)/"CuI     "/
      DATA ion(2129)/"ScI     "/
      DATA ion(2130)/"CuI     "/
      DATA ion(2134)/"NaI     "/
      DATA ion(2135)/"NaI     "/
      DATA ion(2136)/"TiI     "/
      DATA ion(2142)/"NiI     "/
      DATA ion(2143)/"TiII    "/
      DATA ion(2144)/"NiI     "/
      DATA ion(2146)/"NiI     "/
      DATA ion(2147)/"NiI     "/
      DATA ion(2148)/"FeI     "/
      DATA ion(2155)/"CoI     "/
      DATA ion(2158)/"CrI     "/
      DATA ion(2159)/"ScII    "/
      DATA ion(2162)/"CrI     "/
      DATA ion(2163)/"CrI     "/
      DATA ion(2166)/"TiI     "/
      DATA ion(2167)/"ScII    "/
      DATA ion(2168)/"FeI     "/
      DATA ion(2169)/"FeI     "/
      DATA ion(2172)/"TiI     "/
      DATA ion(2173)/"FeI     "/
      DATA ion(2174)/"TiI     "/
      DATA ion(2178)/"V I     "/
      DATA ion(2179)/"V I     "/
      DATA ion(2180)/"FeI     "/
      DATA ion(2182)/"V I     "/
      DATA ion(2183)/"FeI     "/
      DATA ion(2191)/"ScI     "/
      DATA ion(2192)/"CaII    "/
      DATA ion(2193)/"AlI     "/
      DATA ion(2194)/"TiI     "/
      DATA ion(2195)/"CaII    "/
      DATA ion(2196)/"TiI     "/
      DATA ion(2199)/"ScI     "/
      DATA ion(2200)/"MnI     "/
      DATA ion(2202)/"MnI     "/
      DATA ion(2203)/"MnI     "/
      DATA ion(2204)/"K I     "/
      DATA ion(2205)/"K I     "/
      DATA ion(2207)/"SrII    "/
      DATA ion(2214)/"SrII    "/
      DATA ion(2216)/"FeI     "/
      DATA ion(2217)/"CaI     "/
      DATA ion(2218)/"CrI     "/
      DATA ion(2219)/"CrI     "/
      DATA ion(2220)/"CrI     "/
      DATA ion(2223)/"FeI     "/
      DATA ion(2230)/"SrI     "/
      DATA ion(2235)/"FeI     "/
      DATA ion(2236)/"BaI     "/
      DATA ion(2238)/"NaI     "/
      DATA ion(2239)/"NaI     "/
      DATA ion(2243)/"LiI     "/
      DATA ion(2244)/"LiI     "/
      DATA ion(2245)/"K I     "/
      DATA ion(2246)/"K I     "/
      DATA ion(2247)/"RbI     "/
      DATA ion(3000)/"CII*    "/



                          
      CALL STSPRO('plines')
   
c Read keywords:
      CALL STKRDC('tablein',1,1,40,acts,tabname1,kun,knul,status)
      CALL STKRDR('z',1,1,acts,z,kun,knul,status)
      CALL STKRDR('range',1,2,acts,range,kun,knul,status)
      CALL STKRDI('lines',1,20,acts,lines,kun,knul,status)
      DO 5 k = 1,20
c         write(*,*) lines(k)
         if (lines(k).EQ.0) GOTO 6
5     CONTINUE   
6     CONTINUE   
      nlines = k -1
c      write(*,*) nlines

c Open table with displayed data:
      CALL TBTOPN(tabname1,2,tiddata1,status)
      CALL TBIGET(tiddata1,numcol,numrow,nsc,acol,arow,status)

c Search for columns with wave, y and witdh:
      CALL TBCSER(tiddata1,':wave',colx,status)
      CALL TBCSER(tiddata1,':normflux',coly,status)

c Open graphic device:
      CALL PTOPEN(' ',' ',ACCESS,PLMODE)

c Read table data betwwen 1 and numrow (only selected rows are considered):
      NDATA=0
      do 30 i = 1,numrow
         CALL TBSGET(tiddata1,i,selflag,status)
         IF (.NOT.selflag) GOTO 30
         NDATA=NDATA+1
         CALL TBERDR(tiddata1,i,colx,X(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,coly,Y(NDATA),lonull,status)
30    continue

c Compute new flux and plot in velocity:
      LTYPE = 1

      DO 65 j = 1,nlines
c         write(*,*) lines(j),wave(lines(j))
         DO 60 i = 1,numrow
            IF (wave(lines(j)).EQ.0) GOTO 65
            V(i)=(X(i)/(wave(lines(j))*(1.+z))-1.)*299790.
            YNEW(i)=Y(i)/nlines  + (j-1.)/nlines
60       CONTINUE

c         xaxis(1)=-200.
c         xaxis(2)=200.
c         xaxis(3)=50.
c         xaxis(4)=10.
c         yaxis(1)=0.0
c         yaxis(2)=1.1
c         yaxis(3)=0.1
c         yaxis(4)=0.1
c         CALL PTAXES(xaxis,yaxis,'','','')
c         ipos=-1
c         yc=20
c         xc=20
c         CALL PTFRAM(xaxis,yaxis,'','')

c plot data:
         CALL PTDATA(STYPE,LTYPE,BIN,V,YNEW,YOFF,NDATA)
c plot lines:
         liney(1)=1./nlines  + (j-1.)/nlines
         liney(2)=1./nlines  + (j-1.)/nlines
c         write(*,*) range(1),liney(1),range(2),liney(2)
         CALL PTDATA(STYPE,3,OFF,range,liney,YOFF,2)
         write(text,1000) ion(lines(j)),wave(lines(j)) 
1000     format(a6,F6.1)
         CALL PTTEXT(text,range(1)*.9,(j+0.1)/nlines,0.,0.6,1)

65    CONTINUE
      CLOSE(1)
      end











