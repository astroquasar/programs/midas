!
! MIDAS procedure  COMBI_2243		 	                03.11.00 
!								(C)SVCLM
!
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter input table 3:"
DEFINE/PARAMETER P4 ?      C   "Enter output table:"
!
!                    Scale by median:
!
copy/tab {P1} {P4}
comp/tab {P4} :flux1 = :flux 
comp/tab {P4} :sigma1 = :sigma 
!
copy/tt {P2} :flux {P4} :flux2
copy/tt {P2} :sigma {P4} :sigma2
!
copy/tt {P3} :flux {P4} :flux3
copy/tt {P3} :sigma {P4} :sigma3
!

!
!                         Coadd flux and sky:
!
comp/tab {P4} :sigma = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2))
comp/tab {P4} :flux = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2)*:sigma**2


