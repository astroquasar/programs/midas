!
! MIDAS procedure  COMBI_2243		 	                03.11.00 
!								(C)SVCLM
!
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter input table 3:"
DEFINE/PARAMETER P4 ?      C   "Enter input table 4 :"
DEFINE/PARAMETER P5 ?      C   "Enter output table:"
!
!                    Scale by median:
!
copy/tab {P1} {P5}
comp/tab {P5} :flux1 = :flux 
comp/tab {P5} :sigma1 = :sigma 
!
copy/tt {P2} :flux {P5} :flux2
copy/tt {P2} :sigma {P5} :sigma2
!
copy/tt {P3} :flux {P5} :flux3
copy/tt {P3} :sigma {P5} :sigma3
!
copy/tt {P4} :flux {P5} :flux4
copy/tt {P4} :sigma {P5} :sigma4
!

!
!                         Coadd flux and sky:
!
comp/tab {P5} :sigma = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2+1/:sigma4**2))
comp/tab {P5} :flux = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2+:flux4/:sigma4**2)*:sigma**2


