!
! MIDAS procedure  COMBI_2243		 	                03.11.00 
!								(C)SVCLM
!
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter output table:"
!
!                    Scale by median:
!
copy/tab {P1} {P3}
comp/tab {P3} :fluxA1 = :flux1 
comp/tab {P3} :sigmaA1 = :sigma1 
!
copy/tt {P2} :flux1 {P3} :fluxA2
copy/tt {P2} :sigma1 {P3} :sigmaA2
!
!                         Coadd flux 1:
!
comp/tab {P3} :sigmaA = sqrt(1/(1/:sigmaA1**2+1/:sigmaA2**2))
comp/tab {P3} :fluxA = (:fluxA1/:sigmaA1**2+:fluxA2/:sigmaA2**2)*:sigmaA**2
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
comp/tab {P3} :fluxB1 = :flux2 
comp/tab {P3} :sigmaB1 = :sigma2 
!
copy/tt {P2} :flux2 {P3} :fluxB2
copy/tt {P2} :sigma2 {P3} :sigmaB2
!

!
!                         Coadd flux 1:
!
comp/tab {P3} :sigmaB = sqrt(1/(1/:sigmaB1**2+1/:sigmaB2**2))
comp/tab {P3} :fluxB = (:fluxB1/:sigmaB1**2+:fluxB2/:sigmaB2**2)*:sigmaB**2


