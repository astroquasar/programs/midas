! Find tilt in MIKE spectra -output table contains emission line pairs
! April 2004

WRITE/KEY off_tilt/r/1/1    3.        ! extract at this offset in pixels.
WRITE/KEY off_orig/r/1/1    {offset}  ! original offset (slit center)
WRITE/KEY off_aux/r/1/1    0.
WRITE/KEY out_tab/C/1/60   line_out

set/echelle slit=0.5

set/echelle wlcmtd=restart   wlc={p1} width2=4 thres2=20  
set/echelle tol=0.6 dc=4 seamtd=gravity
set/echelle WLCITER=1,0.3,20,5.0,4.0  WLCNITER = 5,10 LINCAT=MagE_thar.tbl
!
clear/chanl over
!
comp/key off_aux = {off_orig}+{off_tilt}
set/echelle offset={off_aux}
VERIFY/ECHELLE {WLC}
LOAD {WLC}
EXTRACT/ECHELLE {WLC} ext
SEARCH/ECHELLE ext
copy/tab line line_up
!load/table line_up :x :ynew ? 0 1 2   !! ynew is y with respect to offset!!

comp/key off_aux = {off_orig}-{off_tilt}
set/echelle offset={off_aux}
VERIFY/ECHELLE {WLC}
LOAD {WLC}
EXTRACT/ECHELLE {WLC} ext
SEARCH/ECHELLE ext
copy/tab line line_down
!load/table line_down :x :ynew ? 0 1 2

join/tab line_up :x,:ynew line_down :x,:ynew {out_tab} 2.5,0.1

comp/tab {out_tab} :ynew_1=:ynew_1 + {off_orig}+{off_tilt}
comp/tab {out_tab} :ynew_2=:ynew_2 + ({off_orig}-{off_tilt})
load/table {out_tab} :x_1 :ynew_1 ? 0 1 3
load/table {out_tab} :x_2 :ynew_2 ? 0 1 3

set/echelle offset={off_orig}
