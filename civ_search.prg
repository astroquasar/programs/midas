! MIDAS procedure  CIV_SEARCH		 			17.08.98 
!								(C)SVCLM
!
!Search C IV in Keck HIRES spectra (Diploma)
! Columns in input table are :flux,sigma,wave 
!
DEFINE/PARAMETER P1 ?           C   "Enter table with data:"
DEFINE/PARAMETER P2 ?           C   "Enter redshift range:"
DEFINE/PARAMETER P3 ?           C   "Enter table name for results:"
!
write/key TABLE/c/1/20    {P1}
write/key deltaz/r/1/2    {P2}
write/key TABLEOUT/c/1/20    {P3}
!
run /home/slopez/he1104/prg/civ_search


