!
!PROCEDURE ECHELLECAL.  (C) SEBASTIAN LOPEZ APR.95
!Wavelength Calibration in EMMI Echelle Frames. Session is already init.
!(SAVE/ECHELLE Sesion name afterwards!)
!
!
define/parameter p1 ? I "Enter frame with Th-Ar spectrum:"
!
!
!copy/tab order_1x2 order
set/echelle nbordi=32 slit=20 offset=0
set/echelle wlcmtd=restart wlc={p1} width2=2.3 thres2=70  tol=0.2 dc=6 seamtd=gravity
set/echelle WLCITER=1,0.3,20,5.0,4.0  WLCNITER = 5,10 LINCAT=thar.tbl

!
cal/echelle done
!
!
!LINES USED:  (frames divided by 3-5)
!
!4831.121  order  96
!4789.386  order  98
!4515.118  order 104
!4333.560  order 107
!4190.712  order 111
!3898.437  order 119
!3874.862  order 120
!3868.530  order 120
!!!!3868.530  order 120


96
4831.121
4190.712





4789.386
4515.118
4333.560
4190.712
3898.437
