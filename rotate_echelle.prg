
!rotate/echelle UM425_b_1.bdf test.bdf flip x -90.
!comp/ima UM425_b_1 = test
!write/desc UM425_b_1 start 1,1
!write/desc UM425_b_1 step 1,1


!rotate/echelle ThAr_b.bdf test.bdf flip x -90.
!comp/ima ThAr_b = test
!write/desc ThAr_b start 1,1
!write/desc ThAr_b step 1,1

!rotate/echelle CXOMP_b_3.bdf test.bdf flip x -90.
!comp/ima CXOMP_b_3 = test
!write/desc CXOMP_b_3 start 1,1
!write/desc CXOMP_b_3 step 1,1

!rotate/echelle flat_b.bdf test.bdf flip x -90.
!comp/ima flat_b = test
!write/desc flat_b start 1,1
!write/desc flat_b step 1,1

!rotate/echelle HR4172_b_2.bdf test.bdf flip x -90.
!comp/ima HR4172_b_2 = test
!write/desc HR4172_b_2 start 1,1
!write/desc HR4172_b_2 step 1,1

!goto there

!!!! convension of catalog frames: 9 characters + ".bdf"

DEFINE/LOCAL CATAL/I/1/1 0

DEFINE/PAR P1 ? IMAGE "Enter catalog:"

rotate/echelle {P1} rota flip x -90.

create/icat rota.cat rota*

cat_loop:

store/frame IN_A {P1} 1 finito
store/frame IN_B rota.cat 1 finito

write/out {IN_A},{IN_B}


write/out {IN_A(1:13)}

comp/ima {IN_A(1:13)} = {IN_A}

write/desc {IN_A(1:13)} start 1,1
write/desc {IN_A(1:13)} step 1,1


goto cat_loop

finito:

