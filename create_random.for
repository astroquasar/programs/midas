
*PROGRAMM CREATE_CIV		14.06.99
*------------------ 		(C)	SVCLM
* Create normalized spectrum with CIV doublet + noise
*
      implicit none
*
      real*4 wave,lambda,TOL,P,z,PMIN,wave0,dv,dv1,redshift,sum1,sum2
      real*4 deltaz(2),error,sigma,g_i,l1,l2,dz,alpha,flux,stdev
      real*4 chiquad,data(2),snr,idum,eqw
      integer i,i1,i2,i_l1,i_l2rad,numrowdata,numrowcursor,row
      integer numcol,nsc,arow,lonull,acol,NDATA,N

      integer tiddata,tidcursor,acts,kun,knul,status
      integer colwavedata,colcont,nc,col(2),colflux,colsigma,colwave,aux
      integer colz,colchiquad
      character*20 tabdata,tabname3
      character*10 ID
      logical selflag

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'


      CALL STSPRO('civ_search')

*Read parameters from keywords:

      CALL STKRDR('idum',1,1,acts,idum,kun,knul,status)
      CALL STKWRR('RAN',RAN2(idum),1,1,kun,status)
      eqw = RAN2(idum)
      write(*,*) eqw

      CALL STSEPI

      end

***************************************************************************

      FUNCTION RAN2(IDUM)
      real*4 idum
      PARAMETER (M=714025,IA=1366,IC=150889,RM=1.4005112E-6)
      DIMENSION IR(97)
      DATA IFF /0/
      IF(IDUM.LT.0.OR.IFF.EQ.0)THEN
        IFF=1
        IDUM=MOD(IC-IDUM,M)
        DO 11 J=1,97
          IDUM=MOD(IA*IDUM+IC,M)
          IR(J)=IDUM
11      CONTINUE
        IDUM=MOD(IA*IDUM+IC,M)
        IY=IDUM
      ENDIF
      J=1+(97*IY)/M
      IF(J.GT.97.OR.J.LT.1)PAUSE
      IY=IR(J)
      RAN2=IY*RM
      IDUM=MOD(IA*IDUM+IC,M)
      IR(J)=IDUM
      RETURN
      END

*****************************************************************************

      FUNCTION RAN3(IDUM)
C         IMPLICIT REAL*4(M)
C         PARAMETER (MBIG=4000000.,MSEED=1618033.,MZ=0.,FAC=2.5E-7)
      PARAMETER (MBIG=1000000000,MSEED=161803398,MZ=0,FAC=1.E-9)
      DIMENSION MA(55)
      DATA IFF /0/
      IF(IDUM.LT.0.OR.IFF.EQ.0)THEN
        IFF=1
        MJ=MSEED-IABS(IDUM)
        MJ=MOD(MJ,MBIG)
        MA(55)=MJ
        MK=1
        DO 11 I=1,54
          II=MOD(21*I,55)
          MA(II)=MK
          MK=MJ-MK
          IF(MK.LT.MZ)MK=MK+MBIG
          MJ=MA(II)
11      CONTINUE
        DO 13 K=1,4
          DO 12 I=1,55
            MA(I)=MA(I)-MA(1+MOD(I+30,55))
            IF(MA(I).LT.MZ)MA(I)=MA(I)+MBIG
12        CONTINUE
13      CONTINUE
        INEXT=0
        INEXTP=31
        IDUM=1
      ENDIF
      INEXT=INEXT+1
      IF(INEXT.EQ.56)INEXT=1
      INEXTP=INEXTP+1
      IF(INEXTP.EQ.56)INEXTP=1
      MJ=MA(INEXT)-MA(INEXTP)
      IF(MJ.LT.MZ)MJ=MJ+MBIG
      MA(INEXT)=MJ
      RAN3=MJ*FAC
      RETURN
      END
