
*PROGRAMM MULTIGAUSS2		09.09.96
*------------------ 		(C)	SVCLM
*
* Fit Gaussian(s) to displayed absorption profile.
*
*
* Assumptions:
*
*  1. Columns ':wave', ':flux' and ':sigma' in table with displayed data.
*  2. Flux and sigma data are rectified to continuum equals 1.
*  3. Max. number of components equals 20.
*  4. In the case of using input values via table, columns ':x_axis' and
*     ':yaxis' must be present (as resulted from 'get/gcurs table').
      implicit none

c For data:
      real X(2000),Y(2000),SIG(2000),YFIT(2000)
      real error,FWHM,W,errorW,b,errorb,SIG2,Wprof
      integer i1,i2,i3,NDATA
      integer colwave,colflux,colsigma,colx,coly,colfit
      integer colcen,colerc,colb,colW,colerW,colymin,colwidth

c For L-M fit:
      INTEGER MFIT,MA,NCA,N,ngauss
      INTEGER LISTA(60)
      REAL ERR(60),CHISQ,CHISQOLD,ALAMBDA,ALAOLD,delta
      REAL COVAR(60,60),ALPHA(60,60),A(60),DYDA(60)

c For start values:
      real*8 xstart,ystart,bstart
      real XAUX(2000),YAUX(2000)
      real xpos,ypos,pixsize
      integer npix,NDATAAUX 

c General:
      integer numcol,numrow,nsc,arow,lonull,acol
      integer tiddata1,tiddata2,tiddata3,acts,kun,knul,i,j,k
      character*40 tabname1,tabname2,tabname3,col1,col2
      character*1 option
      logical selflag
      EXTERNAL FGAUSS

c For graphic:
      integer access,plmode,key,status,maxvals,actvals
      integer stype,ltype,bin,yoff,ssize
      real xcur,ycur,x1,x2,xfirst,xlast

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'

      DATA      access/1/
      DATA      maxvals/20/
      DATA      plmode/-1/
      DATA      bin/0/
      DATA      STYPE/1/
      DATA      YOFF/0.0/
      DATA      npix/2/      
      DATA      pixsize/2.73/      

****************************************************************************

      CALL STSPRO('multigauss2')
   
c Read keywords:
      CALL STKRDI('NGAUSS',1,1,acts,ngauss,kun,knul,status)
      CALL STKRDC('tablein',1,1,40,acts,tabname2,kun,knul,status)
      CALL STKRDC('tableout',1,1,40,acts,tabname3,kun,knul,status)
      CALL STKRDC('option',1,1,1,acts,option,kun,knul,status)
      CALL STKRDR('x1',1,1,acts,x1,kun,knul,status)
      CALL STKRDR('x2',1,1,acts,x2,kun,knul,status)

c Open table with displayed data:
      CALL PTKRDC('DNAME',maxvals,actvals,tabname1)
      CALL TBTOPN(tabname1,2,tiddata1,status)
      CALL TBCSER(tiddata1,':wave',colwave,status)
      CALL TBCSER(tiddata1,':normflux',colflux,status)
      CALL TBCSER(tiddata1,':stdev',colsigma,status)
      CALL TBCSER(tiddata1,':fitgauss',colfit,status)
      if (colfit.LT.0) CALL TBCINI(tiddata1,D_R4_FORMAT,1,'F12.6',' ','
     +:fitgauss',colfit,status)
      CALL TBIGET(tiddata1,numcol,numrow,nsc,acol,arow,status)

      write(*,*) 

      if (tabname2 .NE. 'none') then

c Open table with input parameters:
         CALL TBTOPN(tabname2,2,tiddata2,status)

         if (option .NE. 'N') then

c Search for columns with wave, y and witdh:

            CALL TBCSER(tiddata2,':center',colx,status)
            CALL TBCSER(tiddata2,':ymin',coly,status)
            CALL TBCSER(tiddata2,':b',colwidth,status)
            write(*,800) tabname2

         else

            CALL TBCSER(tiddata2,':X_AXIS',colx,status)
            CALL TBCSER(tiddata2,':Y_AXIS',coly,status)
            CALL TBCSRT(tiddata2,1,colx,1,status)
            write(*,850) tabname2

         endif

      endif

800   FORMAT('*****>>>N.B.: Center and b held fixed. Table: ',a20/)
850   FORMAT('*****>>>N.B.: Start position taken from table: ',a20/)

c Open table with output parameters:
         CALL TBTINI(tabname3,0,1,5,15,tiddata3,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':center',
     +               colcen,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':errorc',
     +               colerc,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':ymin',
     +               colymin,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':b',
     +               colb,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':W',
     +               colW,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':errorW',
     +               colerW,status)
         write(*,900) tabname3
900   FORMAT('*****>>>N.B.: Output data being wrote to table: ',a20/)

c Display for parameters:

      write(*,1000)
1000  FORMAT('LINE   CENTER       ERR       b       W      ERRW    
     +NITER    CHI/DEG'/'-------------------------------------
     +----------------------------------')



c Compute error for searching x1 and x2 in table:
      CALL TBERDR(tiddata1,1,colwave,xfirst,lonull,status)
      CALL TBERDR(tiddata1,numrow,colwave,xlast,lonull,status)
      if (xlast .LT. xfirst) then
         CALL TBCSRT(tiddata1,1,colwave,1,status)
      endif
      error = ABS(xlast - xfirst)/(numrow*1.5)
c Open graphic device:
      CALL PTOPEN(' ','none',ACCESS,PLMODE)

c Get cursor values x1 and x2:

   10 CONTINUE
c      CALL PTGCUR(XCUR,YCUR,KEY,STATUS)
c      IF (STATUS.EQ.1) THEN
c         GOTO 10
c      ELSE IF (KEY.EQ.32) THEN
c         CALL TBTCLO(tiddata1,STATUS)
c         CALL PTCLOS()
c         CALL STSEPI
c      ELSE
c         LTYPE = 0
c         BIN   = 0
c         CALL PTDATA(STYPE,LTYPE,BIN,XCUR,YCUR,YOFF,1)
c         X1    = XCUR
c      END IF
C
   20 CONTINUE
c      CALL PTGCUR(XCUR,YCUR,KEY,STATUS)
c      IF (STATUS.EQ.1) THEN
c         GOTO 20
c      ELSE IF (KEY.EQ.32) THEN
c         CALL TBTCLO(tiddata2,STATUS)
c         CALL PTCLOS()
c         CALL STSEPI
c      ELSE
c         LTYPE = 0
c         CALL PTDATA(STYPE,LTYPE,BIN,XCUR,YCUR,YOFF,1)
c         X2    = XCUR
c      ENDIF


c Search for row numbers of x1 and x2:
      CALL TBESRR(tiddata1,colwave,x1,error,1,i1,status)
      CALL TBESRR(tiddata1,colwave,x2,error,1,i2,status)

c Read table data betwwen x1 and x2 (only selected rows are considered):
      NDATA=0
      do 30 i = i1,i2
         CALL TBSGET(tiddata1,i,selflag,status)
         IF (.NOT.selflag) GOTO 30
         NDATA=NDATA+1
         CALL TBERDR(tiddata1,i,colwave,X(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,colflux,Y(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,colsigma,SIG(NDATA),lonull,status)
         Y(NDATA) = 1. - Y(NDATA)
30    continue

*****************************************************************************

c FITTING WITH 3*NGAUSS FREE PARAMETERS: ************************************

      if (option .EQ. 'N') then      

c Parameters for MARQUARDT:
      MA   = 3*ngauss
      MFIT = MA
      NCA  = 60

c Compute start values:

      if (NDATA .LT. 2*npix+1) then
         write(*,1005) 
c         write(*,*) NDATA
         goto 10
      endif
1005  FORMAT('Too few pixels')
      CALL STARTGAUSS(X,Y,NDATA,2*npix+1,xstart,ystart,bstart)
      do 35 i = 1,MFIT-1,3
         LISTA(i) = i
         LISTA(i+1) = i+1
         LISTA(i+2) = i+2
35    continue
      if (ngauss .EQ. 1) then      
         A(1) = ystart
         A(2) = xstart
         A(3) = bstart 
      else
         do 37 i = 1,ngauss

            CALL TBERDR(tiddata2,i,colx,xpos,lonull,status)
            CALL TBESRR(tiddata1,colwave,xpos,error,1,i3,status)
            NDATAAUX=0
            do 36 k = i3-npix,i3+npix
               NDATAAUX = NDATAAUX + 1
               CALL TBERDR(tiddata1,k,colwave,XAUX(NDATAAUX),
     +                     lonull,status)
               CALL TBERDR(tiddata1,k,colflux,YAUX(NDATAAUX),
     +                     lonull,status)
               YAUX(NDATAAUX) = 1. - YAUX(NDATAAUX)
36          continue
            CALL STARTGAUSS(XAUX,YAUX,NDATAAUX,2*npix+1,xstart,
     +                      ystart,bstart)         
            j = 1 + (i-1)*3
            A(j) = ystart
            A(j+1) = xstart
            A(j+2) = bstart
37       continue
      endif

1060  FORMAT(i3/F16.10/F16.14/F16.14)    

****************************************************************************

c  FITTING WITH FIXED CENTER AND B:***************************************

      else
         MA   = 3*ngauss
         MFIT = ngauss
         NCA  = 60
c   Only first MFIT parameters in array LISTA will be fitted.

         do 38 i = 1,ngauss
            j = 1 + (i-1)*3            
            CALL TBERDR(tiddata2,i,coly,A(j),lonull,status)
            A(j)=1.-A(j)
            CALL TBERDR(tiddata2,i,colx,A(j+1),lonull,status)
            CALL TBERDR(tiddata2,i,colwidth,A(j+2),lonull,status)

            LISTA(i)=j
            LISTA(i+ngauss)=j+1
            LISTA(i+2*ngauss)=j+2
            
38       continue
         
      endif

****************************************************************************

c Loop for MARQUARDT:
      ALAMBDA  = -1.
      DO 40 N = 1,50

         ALAOLD = ABS(ALAMBDA)

         CALL MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,COVAR,
     +               ALPHA,NCA,CHISQ,FGAUSS,ALAMBDA)


         IF (ALAMBDA .GE. ALAOLD) GOTO 50

40    CONTINUE
50    CONTINUE 

c  Compute parameter errors:
      ALAMBDA=0.
      CALL MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,COVAR,
     +            ALPHA,NCA,CHISQ,FGAUSS,ALAMBDA)



c      do 55 i = 1,MFIT-1,3
      do 55 i = 1,MA-1,3
         ERR(i+1) = SQRT(COVAR(i+1,i+1))
         ERR(i) = SQRT(COVAR(i,i))
         ERR(i+2) = SQRT(COVAR(i+2,i+2))
55    continue


c Compute fitted profile:
      SIG2=0.
      Wprof=0.
      DO 60 i = 1,NDATA
         CALL FGAUSS(X(i),A,YFIT(i),DYDA,MA)
         SIG2=SIG2+SIG(i)**2
         Wprof=Wprof+Y(i)*pixsize

         YFIT(i) = 1. - YFIT(i)
         CALL TBEWRR(tiddata1,i+i1-1,colfit,YFIT(i),lonull,status)         
60    CONTINUE

c Display fitted profile:
      LTYPE = 1
      CALL PTDATA(STYPE,LTYPE,BIN,X,YFIT,YOFF,NDATA)

c Display parameters:
      j = 0
      STYPE = 13
      LTYPE = 0
c      do 65 i = 1,MFIT-1,3

      do 65 i = 1,MA-1,3
         j = j + 1
         CALL PTDATA(STYPE,LTYPE,BIN,A(i+1),1.,YOFF,1)
         FWHM = 1.66510922*A(i+2)
         W = 1.77245385*A(i)*A(i+2) 



         b = A(i+2)
         errorW = 1.77245385*SQRT(ERR(i)**2*A(i+2)**2 + ERR(i+2)**2*
     +                            A(i)**2)
         A(i) = 1.- A(i)
         write(*,1010) j,A(i+1),ERR(i+1),b,W,errorW,N,
     +                 CHISQ/(NDATA-MFIT)

c         write(*,*) "errb=",ERR(i+2)

         CALL TBEWRR(tiddata3,j,colcen,A(i+1),lonull,status)         
         CALL TBEWRR(tiddata3,j,colerc,ERR(i+1),lonull,status)         
         CALL TBEWRR(tiddata3,j,colymin,A(i),lonull,status)         
         CALL TBEWRR(tiddata3,j,colb,b,lonull,status)         
         CALL TBEWRR(tiddata3,j,colW,W,lonull,status)         
         CALL TBEWRR(tiddata3,j,colerW,errorW,lonull,status)         
65    continue
      write(*,1020) NDATA,Wprof,SIG2
c      write(*,*) W/errorW
      write(*,*)
      STYPE = 1
1010  FORMAT(i2,4x,F9.3,4x,F5.3,3x,3(F6.3,3x),i2,5x,F8.4)
1020  FORMAT(2x,'Pixels: ',i3,4x,'W profile: ',F6.2,4x,
     +'variance profile: ',F6.2)

c      goto 10

      end


********************* Numerical Recipies **************************************

      SUBROUTINE MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,
     *    COVAR,ALPHA,NCA,CHISQ,FUNCS,ALAMDA)
      PARAMETER (MMAX=60)
      DIMENSION X(NDATA),Y(NDATA),SIG(NDATA),A(MA),LISTA(MA),
     *  COVAR(NCA,NCA),ALPHA(NCA,NCA),ATRY(MMAX),BETA(MMAX),DA(MMAX)
      IF(ALAMDA.LT.0.)THEN
        KK=MFIT+1
        DO 12 J=1,MA
          IHIT=0
          DO 11 K=1,MFIT
            IF(LISTA(K).EQ.J)IHIT=IHIT+1
11        CONTINUE
          IF (IHIT.EQ.0) THEN
            LISTA(KK)=J
            KK=KK+1
          ELSE IF (IHIT.GT.1) THEN
            PAUSE 'Improper permutation in LISTA'
          ENDIF
12      CONTINUE
        IF (KK.NE.(MA+1)) PAUSE 'Improper permutation in LISTA'
        ALAMDA=0.001
        CALL MRQCOF(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,ALPHA,BETA,NCA,CHISQ,F
     *UNCS)
        OCHISQ=CHISQ
        DO 13 J=1,MA
          ATRY(J)=A(J)
13      CONTINUE
      ENDIF
      DO 15 J=1,MFIT
        DO 14 K=1,MFIT
          COVAR(J,K)=ALPHA(J,K)
14      CONTINUE
        COVAR(J,J)=ALPHA(J,J)*(1.+ALAMDA)
        DA(J)=BETA(J)
15    CONTINUE
      CALL GAUSSJ(COVAR,MFIT,NCA,DA,1,1)
      IF(ALAMDA.EQ.0.)THEN
        CALL COVSRT(COVAR,NCA,MA,LISTA,MFIT)
        RETURN
      ENDIF
      DO 16 J=1,MFIT
        ATRY(LISTA(J))=A(LISTA(J))+DA(J)
16    CONTINUE
      CALL MRQCOF(X,Y,SIG,NDATA,ATRY,MA,LISTA,MFIT,COVAR,DA,NCA,CHISQ,FU
     *NCS)
      IF(CHISQ.LT.OCHISQ)THEN
        ALAMDA=0.1*ALAMDA
        OCHISQ=CHISQ
        DO 18 J=1,MFIT
          DO 17 K=1,MFIT
            ALPHA(J,K)=COVAR(J,K)
17        CONTINUE
          BETA(J)=DA(J)
          A(LISTA(J))=ATRY(LISTA(J))
18      CONTINUE
      ELSE
        ALAMDA=10.*ALAMDA
        CHISQ=OCHISQ
      ENDIF
      RETURN
      END

*******************************************************************************

      SUBROUTINE MRQCOF(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,ALPHA,BETA,NALP,
     *CHISQ,FUNCS)
      PARAMETER (MMAX=60)
      DIMENSION X(NDATA),Y(NDATA),SIG(NDATA),ALPHA(NALP,NALP),BETA(MA),
     *    DYDA(MMAX),LISTA(MFIT),A(MA)
      DO 12 J=1,MFIT
        DO 11 K=1,J
          ALPHA(J,K)=0.
11      CONTINUE
        BETA(J)=0.
12    CONTINUE
      CHISQ=0.
      DO 15 I=1,NDATA
        CALL FUNCS(X(I),A,YMOD,DYDA,MA)
        SIG2I=1./(SIG(I)*SIG(I))
        DY=Y(I)-YMOD
        DO 14 J=1,MFIT
          WT=DYDA(LISTA(J))*SIG2I
          DO 13 K=1,J
            ALPHA(J,K)=ALPHA(J,K)+WT*DYDA(LISTA(K))
13        CONTINUE
          BETA(J)=BETA(J)+DY*WT
14      CONTINUE
        CHISQ=CHISQ+DY*DY*SIG2I
15    CONTINUE
      DO 17 J=2,MFIT
        DO 16 K=1,J-1
          ALPHA(K,J)=ALPHA(J,K)
16      CONTINUE
17    CONTINUE
      RETURN
      END


******************************************************************************

      SUBROUTINE COVSRT(COVAR,NCVM,MA,LISTA,MFIT)
      DIMENSION COVAR(NCVM,NCVM),LISTA(MFIT)
      DO 12 J=1,MA-1
        DO 11 I=J+1,MA
          COVAR(I,J)=0.
11      CONTINUE
12    CONTINUE
      DO 14 I=1,MFIT-1
        DO 13 J=I+1,MFIT
          IF(LISTA(J).GT.LISTA(I)) THEN
            COVAR(LISTA(J),LISTA(I))=COVAR(I,J)
          ELSE
            COVAR(LISTA(I),LISTA(J))=COVAR(I,J)
          ENDIF
13      CONTINUE
14    CONTINUE
      SWAP=COVAR(1,1)
      DO 15 J=1,MA
        COVAR(1,J)=COVAR(J,J)
        COVAR(J,J)=0.
15    CONTINUE
      COVAR(LISTA(1),LISTA(1))=SWAP
      DO 16 J=2,MFIT
        COVAR(LISTA(J),LISTA(J))=COVAR(1,J)
16    CONTINUE
      DO 18 J=2,MA
        DO 17 I=1,J-1
          COVAR(I,J)=COVAR(J,I)
17      CONTINUE
18    CONTINUE
      RETURN
      END

****************************************************************************

      SUBROUTINE GAUSSJ(A,N,NP,B,M,MP)
      PARAMETER (NMAX=60)
      DIMENSION A(NP,NP),B(NP,MP),IPIV(NMAX),INDXR(NMAX),INDXC(NMAX)
      DO 11 J=1,N
        IPIV(J)=0
11    CONTINUE
      DO 22 I=1,N
        BIG=0.
        DO 13 J=1,N
          IF(IPIV(J).NE.1)THEN
            DO 12 K=1,N
              IF (IPIV(K).EQ.0) THEN
                IF (ABS(A(J,K)).GE.BIG)THEN
                  BIG=ABS(A(J,K))
                  IROW=J
                  ICOL=K
                ENDIF
              ELSE IF (IPIV(K).GT.1) THEN
                write(*,*) 'Singular matrix'
              ENDIF
12          CONTINUE
          ENDIF
13      CONTINUE
        IPIV(ICOL)=IPIV(ICOL)+1
        IF (IROW.NE.ICOL) THEN
          DO 14 L=1,N
            DUM=A(IROW,L)
            A(IROW,L)=A(ICOL,L)
            A(ICOL,L)=DUM
14        CONTINUE
          DO 15 L=1,M
            DUM=B(IROW,L)
            B(IROW,L)=B(ICOL,L)
            B(ICOL,L)=DUM
15        CONTINUE
        ENDIF
        INDXR(I)=IROW
        INDXC(I)=ICOL
        IF (A(ICOL,ICOL).EQ.0.) write(*,*) 'Singular matrix.'
        PIVINV=1./A(ICOL,ICOL)
        A(ICOL,ICOL)=1.
        DO 16 L=1,N
          A(ICOL,L)=A(ICOL,L)*PIVINV
16      CONTINUE
        DO 17 L=1,M
          B(ICOL,L)=B(ICOL,L)*PIVINV
17      CONTINUE
        DO 21 LL=1,N
          IF(LL.NE.ICOL)THEN
            DUM=A(LL,ICOL)
            A(LL,ICOL)=0.
            DO 18 L=1,N
              A(LL,L)=A(LL,L)-A(ICOL,L)*DUM
18          CONTINUE
            DO 19 L=1,M
              B(LL,L)=B(LL,L)-B(ICOL,L)*DUM
19          CONTINUE
          ENDIF
21      CONTINUE
22    CONTINUE
      DO 24 L=N,1,-1
        IF(INDXR(L).NE.INDXC(L))THEN
          DO 23 K=1,N
            DUM=A(K,INDXR(L))
            A(K,INDXR(L))=A(K,INDXC(L))
            A(K,INDXC(L))=DUM
23        CONTINUE
        ENDIF
24    CONTINUE
      RETURN
      END

****************************************************************************

      SUBROUTINE FGAUSS(X,A,Y,DYDA,NA)
      DIMENSION A(NA),DYDA(NA)      
      Y=0.
      EPS=1.E-15
      DO 11 I=1,NA-1,3
c        write(*,*) X,ARG,EX,A(I+2)
        ARG=(X-A(I+1))/(A(I+2)+EPS)
        EX=EXP(-ARG**2)
        FAC=A(I)*EX*2.*ARG
        Y=Y+A(I)*EX
        DYDA(I)=EX
        DYDA(I+1)=FAC/A(I+2)
        DYDA(I+2)=FAC*ARG/A(I+2)
11    CONTINUE
c      PAUSE
      RETURN
      END

*****************************************************************************

      SUBROUTINE STARTGAUSS(X,Y,NDATA,N,XS,YS,BS)
c
c Returns start values for position, amplitude and width (xs,ys,bs)
c for L-M fitting of gaussian. Method:
c ys = y  minimum.
c xs = sumx*y/sumy
c bs = sqrt(2)*stdv
c
      REAL X(2000),Y(2000)
      REAL XSORT(2000),YSORT(2000)
      REAL*8 xs,ys,bs,sumx,sumy,sumxy,sumx2y
      INTEGER NDATA,i,N
      do 5 i = 1,NDATA
         XSORT(i) = X(i)
         YSORT(i) = Y(i)
5     continue

c Sort data:
      CALL SORT2(NDATA,YSORT,XSORT)

c Find xs:
      sumx = 0.
      sumy = 0.
      sumxy = 0.
      sumx2y = 0.
      do 10 i = NDATA, NDATA-N+1,-1
         sumx = sumx + XSORT(i)
         sumy = sumy + YSORT(i)
         sumxy = sumxy + XSORT(i)*YSORT(i)
         sumx2y = sumx2y + XSORT(i)*XSORT(i)*YSORT(i)
10    continue
      xs = sumxy/sumy
      ys = YSORT(NDATA)
      bs = 1.414213*sqrt(abs(sumx2y/sumy - xs*xs))

      return
      end

****************************************************************************
      SUBROUTINE SORT2(N,RA,RB)
      DIMENSION RA(N),RB(N)
      L=N/2+1
      IR=N
10    CONTINUE
        IF(L.GT.1)THEN
          L=L-1
          RRA=RA(L)
          RRB=RB(L)
        ELSE
          RRA=RA(IR)
          RRB=RB(IR)
          RA(IR)=RA(1)
          RB(IR)=RB(1)
          IR=IR-1
          IF(IR.EQ.1)THEN
            RA(1)=RRA
            RB(1)=RRB
            RETURN
          ENDIF
        ENDIF
        I=L
        J=L+L
20      IF(J.LE.IR)THEN
          IF(J.LT.IR)THEN
            IF(RA(J).LT.RA(J+1))J=J+1
          ENDIF
          IF(RRA.LT.RA(J))THEN
            RA(I)=RA(J)
            RB(I)=RB(J)
            I=J
            J=J+J
          ELSE
            J=IR+1
          ENDIF
        GO TO 20
        ENDIF
        RA(I)=RRA
        RB(I)=RRB
      GO TO 10
      END






























































































