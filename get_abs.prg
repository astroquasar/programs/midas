
write/key z/r/1/1       1.0  ! 1 + redshift
set/format F7.5
get/gcurs table

!goto MgII
goto CIV


MgII:

comp/key z = {table.tbl,:x_axis,@1}/2796.3521 
comp/tab /home/slopez/musica2/prg_linux/abslines2 :wave_test=:wave*{z}
overplot/ident /home/slopez/musica2/prg_linux/abslines2 :wave_test :id
write/out "1+z= {z}
goto there


CIV:

comp/key z = {table.tbl,:x_axis,@1}/1548.1949 
comp/tab /home/slopez/musica2/prg_linux/abslines2 :wave_test=:wave*{z}
overplot/ident /home/slopez/musica2/prg_linux/abslines2 :wave_test :id
write/out "1+z= {z}
goto there


there:
