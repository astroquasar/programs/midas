!assign/graph g,0
assign/graph postscript.p

define/local tableinA/c/1/41 1355A_f.tbl
define/local tableinB/c/1/41 1355B_f.tbl

define/local fluxcolA/c/1/41 :normflux
!define/local sigmacolA/c/1/41 :stdev
define/local fluxcolB/c/1/41 :normflux
!define/local sigmacolB/c/1/41 :stdev
define/local z/r/1/1        0.4799
define/local lambdas/r/1/8  2796.3521,2803.531,2382.7649,2600.1729,2344.2141,2586.6499,2374.4612,2852.9641
define/local QSOname/c/1/41 Q1355-2257


define/local vi/r/1/1 500 
define/local ylimits/r/1/2 -.1,1.5
define/local axelimits/r/1/4 -60,-28,20,220 !-50,-13,20,250
define/local x/r/1/1 3
define/local y/r/1/1 3
define/local N/r/1/1 2

set/graph xformat=none font=1
set/format f8.4
!set/graph xformat=none

!-----------------------------------------------------------------------

COMP/TABL {tableinA} :v = (:wave/('lambdas(7)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(7)'*('z'+1))-1)*299790
set/graph color=1
plot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
label/graph "{QSOname}" 700,2.5 0 'N'
label/graph "z(lens)= 0.702 +/- 0.001" 700,2.2 0,1.5,0
label/graph "z='z'" -300,1.8 0 'N'
label/graph "d=7.35 kpc" 100,1.8 0 'N'
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2374" -300,0.2 0,1.5,0


comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'


COMP/TABL {tableinA} :v = (:wave/('lambdas(6)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(6)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2586" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'

COMP/TABL {tableinA} :v = (:wave/('lambdas(5)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(5)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2344" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'

COMP/TABL {tableinA} :v = (:wave/('lambdas(4)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(4)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2600" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'


COMP/TABL {tableinA} :v = (:wave/('lambdas(3)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(3)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2382" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'


COMP/TABL {tableinA} :v = (:wave/('lambdas(2)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(2)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Mg II  2803" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'



COMP/TABL {tableinA} :v = (:wave/('lambdas(1)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(1)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Mg II  2796" -300,0.2 0,1.5,0

set/graph xformat= font=1
comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'

COMP/TABL {tableinA} :v = (:wave/('lambdas(8)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(8)'*('z'+1))-1)*299790
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tablein} {vi} {lambdas(2)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1

label/graph "Mg I 2852" -300,0.2  0,1.5,0


!-----------------------------------------------------------------------

!--------------------------------------

comp/key axelimits(4) = 'axelimits(4)' -7*'axelimits(2)' 
comp/key axelimits(3) = 'axelimits(3)' + 30*'x'

set/graph xformat=none font=1
set/format f8.4
!set/graph xformat=none
comp/key z = 0.7022
!-----------------------------------------------------------------------


COMP/TABL {tableinA} :v = (:wave/('lambdas(7)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(7)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
!label/graph "{QSOname}" 700,2.5 0 'N'
label/graph "z='z'" -300,1.8 0 'N'
label/graph "d=8.75 kpc" 100,1.8 0 'N'
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2374" -300,0.2 0,1.5,0


comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'


COMP/TABL {tableinA} :v = (:wave/('lambdas(6)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(6)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2586" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'

COMP/TABL {tableinA} :v = (:wave/('lambdas(5)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(5)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2344" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'

COMP/TABL {tableinA} :v = (:wave/('lambdas(4)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(4)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2600" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'


COMP/TABL {tableinA} :v = (:wave/('lambdas(3)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(3)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Fe II 2382" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'


COMP/TABL {tableinA} :v = (:wave/('lambdas(2)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(2)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Mg II  2803" -300,0.2 0,1.5,0

comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'



COMP/TABL {tableinA} :v = (:wave/('lambdas(1)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(1)'*('z'+1))-1)*299790
set/graph color=1
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tableinA} {vi} {lambdas(1)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1
label/graph "Mg II  2796" -300,0.2 0,1.5,0

set/graph xformat= font=1
comp/key axelimits(4) = 'axelimits(4)' + 'axelimits(2)'

COMP/TABL {tableinA} :v = (:wave/('lambdas(8)'*('z'+1))-1)*299790
COMP/TABL {tableinB} :v = (:wave/('lambdas(8)'*('z'+1))-1)*299790
overplot/axes -{vi},{vi} {ylimits(1)},{ylimits(2)},1,0.2 'axelimits(1)','axelimits(2)','axelimits(3)','axelimits(4)'   !UVES
!@@ plot_1.prg {tablein} {vi} {lambdas(2)}


@@ ../programas/plot.prg {tableinA} {vi} {fluxcolA} 
set/graph color=2
@@ ../programas/plot.prg {tableinB} {vi} {fluxcolB}
set/graph color=1

label/graph "Mg I 2852" -300,0.2  0,1.5,0



$ mv postscript.ps {QSOname}.ps
$ evince {QSOname}.ps
