!
!PROCEDURE ECHELLEVAR.  (C) SEBASTIAN LOPEZ JUNE 95
!
!Creates variance frame "var" from input frame. Original units are
!photoelectrons. Corresponding frame "b" (bkg subtracted) must exit.
!
!OUTLIERS: negative values in "b" ----> "infinity" in "var"
!          cosmics in input       ----> "infinity" in "var"
!OTHERWISE: var = input frame
!
!An input value which lies THRES times over the 3*3 area median value
!is taken as cosmic.
!
!
DEFINE/PARAMETER P1 ? I "Enter input frame:"
!
DEFINE/LOCAL THRES/I/1/1 2           ! Cosmics threshold
!
WRITE/OUT "Creating frame maske = {P1} / median"
!
FILTER/MEDIAN {P1} median 3,3,0.87 NR !7x7 neighbour, replace only if 50% diff.
!FILTER/MEDIAN {P1} median 3,3,0.65 NR !7x7 neighbour, replace only if 50% diff.
                                     !FOR 2x2 binning UVES 580

!FILTER/MEDIAN {P1} median 3,3,0.3 NR !7x7 neighbour, replace only if 70% diff.

COMP/IMA maske = {P1} / median
!REPLACE/IMAGE {P1} var maske/1.8,>=1.E8
REPLACE/IMAGE {P1} var maske/3.0,>=1.E8
!                            2 for EMMI
!





