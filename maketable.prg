
!  Procedure maketable.prg                   (c) SVCLM May 1995
!
!  Creates table "startopt.tbl" with start values for 
!  optimal extraction of EMMI echelle orders and "fitopt.tbl" which
!  will contain the fit parameters after running "@@ echelleopt".
!-------------------------------------------------------
!
!goto there1
SET/FORMAT I1
!
DEFINE/PAR P1 ? IMAGE    "Enter input image:"
DEFINE/PAR P2 ? NUMBER   "Enter sky position (pixels):"
DEFINE/PAR P3 ? NUMBER   "Enter degree of polynom:"
DEFINE/PAR P4 ? NUMBER   "Enter pos1,delta,seeing (FWHM in pixels):"
DEFINE/PAR P5 ? CHA      "Create table startopt.tbl again? (Y/N):"
!
!
WRITE/KEY PARAM/R/1/3 {P4}
DEFINE/LOCAL NUMCOL/I/1/1 10           ! N. of columns
DEFINE/LOCAL NUMROW/I/1/1 0           ! N. of rows
DEFINE/LOCAL N/I/1/1 0                ! Loop variable for orders
DEFINE/LOCAL NX/I/1/1 0               ! Loop variable for abcsissa
DEFINE/LOCAL FIRST/I/1/1 0            ! First row in order loop
DEFINE/LOCAL LAST/I/1/1 0             ! Last row in order loop
DEFINE/LOCAL OPTION/C/1/1 {P5}        ! option
DEFINE/LOCAL OFF/R/1/1 0.             ! offset
! 
!
comp/key param(3) = 'param(3)'/2.355   !Conversion to sigma of Gaussian
comp/key param(3) = 'param(3)'*1.414   !Conversion to sigma of L-M method
!
COMP/KEY NUMCOL = NUMCOL + {P3} + 1    ! Number of columns
COMP/KEY NUMROW = NUMROW + {order.tbl,NBORDI}*{{P1},NPIX(1)} ! N. of rows
!
IF OPTION .EQ. "N" GOTO THERE
!
WRITE/OUT {order.tbl,NBORDI} orders.
WRITE/OUT  Creating table "startopt.tbl" 
WRITE/OUT  with 'NUMROW' rows and 'NUMCOL' columns:
CREATE/TAB startopt NUMCOL NUMROW NULL  ! Creates table "startopt"

WRITE/DESC startopt.tbl NPIXX/I/1/1 {{P1},NPIX(1)}
WRITE/DESC startopt.tbl NPIXY/I/1/1 {order.tbl,NBORDI}
WRITE/OUT Wrote descriptors NPIXX and NPIXY in table startopt.tbl 

!
! Columns in table startopt and table elements:
!
CREATE/COL startopt :order ? I2    ! Order Number
CREATE/COL startopt :x  ? I4       ! Absissa (pixels)
WRITE/TAB startopt :x @1..'NUMROW' 0.
WRITE/OUT :x  

CREATE/COL startopt :pos1  ? F7.3  ! Pos. of obj 1 with respect to order
WRITE/TAB startopt :pos1 @1..'NUMROW' 'PARAM(1)'
WRITE/OUT :pos1
CREATE/COL startopt :delta  ? F7.3  ! Pos of obj 1 - pos of obj2
WRITE/TAB startopt :delta @1..'NUMROW' 'PARAM(2)'
WRITE/OUT :delta
CREATE/COL startopt :sigma  ? F7.3 ! Common witdh of seeing profile
WRITE/TAB startopt :sigma @1..'NUMROW' 'PARAM(3)'
WRITE/OUT :sigma
CREATE/COL startopt :int1  ? F7.3 ! Intensity of profile 1
WRITE/TAB startopt :int1 @1..'NUMROW' 0.
WRITE/OUT :int1
CREATE/COL startopt :error1 ? F7.3 ! sigma error in int1
WRITE/TAB startopt :error1 @1..'NUMROW' 0.
WRITE/OUT :error1
CREATE/COL startopt :int2  ? F7.3  ! Intensity of profile 2
WRITE/TAB startopt :int2 @1..'NUMROW' 0.
WRITE/OUT :int2
CREATE/COL startopt :error2  ? F7.3 ! sigma error in int2
WRITE/TAB startopt :error2 @1..'NUMROW' 0.
WRITE/OUT :error2
CREATE/COL startopt :pix  ? I3 ! Num. of pixels with infinity variances
WRITE/TAB startopt :pix @1..'NUMROW' 0
WRITE/OUT :pix
!
! Table elements for column :order :
!
WRITE/OUT :order
!
DO N = 1 {order.tbl,NBORDI}   ! Order loop 
  COMP/KEY FIRST = ( N - 1 ) * {{P1},NPIX(1)} + 1
  COMP/KEY LAST = N * {{P1},NPIX(1)} 
  WRITE/TAB startopt :order @'FIRST'..'LAST' 'N'
ENDDO
!
THERE:
!
! Columns with parameters of sky polynom:
!
WRITE/DESC startopt.tbl SKYPOS/R/1/1  {P2}
WRITE/DESC startopt.tbl POLYDEG/I/1/1 {P3}
WRITE/OUT Wrote descriptors SKYPOS and POLYDEG in table startopt.tbl 
DO N = 0 {startopt.tbl,POLYDEG}               !Sky column loop
  WRITE/OUT :sky{N}
  CREATE/COL startopt :sky{N} ? F7.3
  WRITE/TAB startopt :sky{N} @1..'NUMROW' 0.
ENDDO
!
! Compute sky with SLIT=1 in input frame and write it as
! start value in column :sky0
!
WRITE/KEY IN_A/C/1/60    {P1}
WRITE/KEY OUT_A/C/1/60   OUTIMA
WRITE/KEY INPUTR/R/1/3   1.,0.,{startopt.tbl,SKYPOS}
WRITE/KEY IN_C/C/1/7     AVERAGE
WRITE/KEY IN_B/C/1/60    {ORDTAB}
WRITE/KEY INPUTC/C/1/60  COEFF
WRITE/KEY INPUTI/I/1/2   1,{NBORDI}
WRITE/KEY IN_COL/C/1/60  sky0

WRITE/OUT "computing sky0..."
RUN /musica2/prg_linux/maketable
!
! Compute int1 and int2 with echelle parameters SLIT=1 OFFSET=pos1
! and OFFSET=pos1-delta:
!
COMP/KEY OFF = 'PARAM(1)'
WRITE/KEY INPUTR/R/1/3   1.,0.,'OFF'
WRITE/KEY IN_COL/C/1/60  int1
WRITE/OUT "computing int1..."
RUN /musica2/prg_linux/maketable
!
COMP/KEY OFF = 'PARAM(1)'-'PARAM(2)'
WRITE/KEY INPUTR/R/1/3   1.,0.,'OFF'
WRITE/KEY IN_COL/C/1/60  int2
WRITE/OUT "computing int2..."
RUN /musica2/prg_linux/maketable
!
! Filter sky0,int1 and int2:
!
there1:

@@ /musica2/prg_linux/median startopt test :x :sky0 5
copy/tt test :median startopt :sky0
@@ /musica2/prg_linux/median startopt test :x :int1 5
copy/tt test :median startopt :int1
@@ /musica2/prg_linux/median startopt test :x :int2 5
copy/tt test :median startopt :int2
!
comp/tab startopt :int1 = :int1 - :sky0
comp/tab startopt :int2 = :int2 - :sky0
!

!
