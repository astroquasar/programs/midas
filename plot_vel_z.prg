
    WRITE/KEY aux/r/1/1  1. 

plot/axes 1.55,2.35.0.2,0.2 0,100 -200,-60,30,20 "z" "RMS \Delta v"
select/tab testHIA :redshift.ge.2.2.and.:redshift.le.2.3.and.:redshiftB.ne.0
stat/tab testHIA :minvHI2
comp/key aux=M$SQRT({OUTPUTR(3)}) 
overplot/symbol 19 2.25,{aux}
