! Fit polynom to tilt angles
!
WRITE/KEY in_tab/C/1/60   line_out   !table with line pairs
WRITE/KEY out_tab/C/1/60   out1      !modified startopt with tilts
WRITE/KEY order/i/1/1   1
WRITE/KEY xpix/i/1/1   2000          !pixels in x to displey

copy/tab startopt test1
COMPUTE/tab  test1 :tanangle_fit = 1.
del/row test1 @1..

set/graph bin=off color=1
!plot/axes 0,{xpix}  30,35  -300,-120,20,20 "x" "tan angle"
plot/axes 0,{xpix}  0,{order.tbl,nbordi}  ? "x" "tan angle"
comp/tab {in_tab} :tanangle=((:x_2-:x_1)/(:ynew_1-:ynew_2))

do order = 1 {order.tbl,nbordi}

set/graph stype=4  color=1
comp/tab {in_tab} :y_aux = :tanangle + {order}
select/tab {in_tab} :y_1.eq.{order}
overplot/tab {in_tab} :x_1 :y_aux

!FIT:
regression/poly {in_tab} :tanangle :x_1 2
SAVE/REGRESSION {in_tab} COEF
COMPUTE/REGRESSION  {in_tab} :tanangle_fit = COEF

!!!!evaluate at x in table startopt for each order:
select/tab startopt :order.eq.{order}
copy/tab startopt test_ord
copy/dd {in_tab}.tbl COEF* test_ord.tbl 
COMPUTE/REGRESSION  test_ord :tanangle_fit = COEF(:x)
merge/tab test1 test_ord test2
copy/tab test2 test1

set/graph stype=0 color=2
comp/tab {in_tab} :y_aux = :tanangle_fit + {order}
select/tab {in_tab} :y_1.eq.{order}
overplot/tab {in_tab} :x_1 :y_aux

enddo

select/tab {in_tab} all
select/tab startopt all
copy/tab test1 {out_tab} 
set/graph bin=off color=1