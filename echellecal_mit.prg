!
!PROCEDURE ECHELLECAL.  (C) SEBASTIAN LOPEZ APR.95
!Wavelength Calibration in EMMI Echelle Frames. Session is already init.
!(SAVE/ECHELLE Sesion name afterwards!)
!
!
define/parameter p1 ? I "Enter frame with Th-Ar spectrum:"
!
!
copy/tab order_mit order
set/echelle nbordi=13 slit=10 offset=-2

!!!! 580:
!set/echelle wlcmtd=pair wlc={p1} width2=2.8 thres2=30  tol=0.2 dc=4 seamtd=gravity

!!!! 860:
set/echelle wlcmtd=restart wlc={p1} width2=2.3 thres2=60  tol=0.2 dc=4 seamtd=gravity
set/echelle WLCITER=1,0.2,20,5.0,4.0  WLCNITER = 5,10 LINCAT=thar.tbl

!
cal/echelle done
!
!
!LINES USED 580 (MIT):  Frames divided by 150-200)
!
!
!6531.341   order 93
!!!!6752.832  order 90
!!!!!5891.451  order 103
!6077.105  order 100


!LINES USED 860 (MIT):  
!
	!!!!!9608.936   order 63
	!!!!!9608.486   order 63
	!!!!!9582.813   order 63
	!9317.729   order 65
	!9045.353   order 67
	!!!!!8784.142   order 69

!With option "angle":

!10133.566   order 60
! 9855.742   order 62
! 8969.867   order 68
! 8749.169   order 70

!60
!10133.566
!9855.742   
!9505.393
!9270.150
!8969.867
!8749.169 



!** Identification loop **
! Initial accuracy          : 1.0 pixel
! Next neighbour parameter  : 0.1
! Maximal error             : 1.0 pixel
! Initial error             : 2.0 pixel
! Iteration error           : 1.0 std dev.

!** Rejection loop **
! Tolerance                 : 0.5


