!
! MIDAS Procedure ECHELLEFITNEW (SANTIAGO)                      (C) SVCLM
!      for HE 2243                                        March 1996
! Fits polynoms to fitted parameters of optimal extraction
! of Echelle orders (echelleopt.prg)

! Method: parameters POS1 and SIGMA in each order are fitted with
! third order polynoms. 

DEFINE/PARAMETER P1 ?      C   "Enter input table:"
DEFINE/PARAMETER P2 ?      C   "Enter output table:"
DEFINE/PARAMETER P3 ?      C   "Enter poly degree:"
DEFINE/LOCAL N/I/1/1 0                ! Loop variable for orders
DEFINE/LOCAL I/I/1/1 0                ! Loop variable for regression
DEFINE/LOCAL IMAX/I/1/1 6            ! max Zahl der Iterationen
DEFINE/LOCAL KAPPA/I/1/1 3
DEFINE/LOCAL SIGMA/R/1/1 0
DEFINE/LOCAL SELROW/I/1/1 0
DEFINE/LOCAL sigstart/R/1/1 2.2
DEFINE/LOCAL posstart/R/1/1 1.1
DEFINE/LOCAL XLIM/R/1/2 200.,1200.  ! x-region to consider in fits


copy/tab {P1} test1

@@ /crater/prg_linux/median test1 testa :x :pos1 5
copy/tt testa :median test1 :pos1
@@ /crater/prg_linux/median test1 testa :x :delta 5
copy/tt testa :median test1 :delta
@@ /crater/prg_linux/median test1 testa :x :sigma 5
copy/tt testa :median test1 :sigma

SELECT/TAB test1 :order .EQ. 1
COPY/TAB test1 {P2}
DELETE/ROW {P2} 1..
SELECT/TAB test1 ALL

DO N = 1 {order.tbl,NBORDI}
!DO N = 1 18

!selektion der orders
WRITE/OUT "Order" {N}
SELECT/TAB test1 (sequence .LE. {N}*{test1.tbl,NPIXX}).AND.(sequence .GT. ({N}-1)*{test1.tbl,NPIXX})
!Fit der Position, Schleife
COPY/TAB test1 testa
COPY/TAB test1 testb
SELECT/TABLE testa :pos1 .ne. 'posstart' 
select/tab testa SELECT .AND. ((:x .GE. 'XLIM(1)') .AND. (:x .LE. 'XLIM(2)'))

stat/tab testa :pos1
COMPUTE/KEY sigma = M$ABS(outputr(4))
@@ /crater/prg_linux/median testa aux :x :pos1 1
!                                 skip outliers:
COMPUTE/KEY sigma = kappa * sigma
READ/KEY  sigma
SELECT/TAB testa SELECT .AND. ABS(:pos1 - {aux.tbl,MEDIAN}) .LE. 'sigma'

DO I = 1 IMAX
COMPUTE/KEY selrow = outputi(1)

REGRESSION/POLYNOMIAL testa  :pos1 :x {P3} 
SAVE/REGRESSION testa COEF
COMPUTE/REGRESSION testa :fit = COEF
COMPUTE/TABLE testa :diff = :pos1 - :fit
SELECT/TABLE testa :pos1 .ne. 'posstart'

stat/tab testa :pos1
COMPUTE/KEY sigma = M$ABS(outputr(4))
@@ /crater/prg_linux/median testa aux :x :pos1 1
!                                 skip outliers:
COMPUTE/KEY sigma = kappa * sigma
READ/KEY  sigma

SELECT/TAB testa :pos1 .ne. 'posstart' .AND. ABS(:diff) .LE. 'sigma'
select/tab testa SELECT .AND. ((:x .GE. 'XLIM(1)') .AND. (:x .LE. 'XLIM(2)'))
IF OUTPUTI(1) .EQ. selrow .OR. I .EQ. IMAX THEN
REGRESSION/POLYNOMIAL testa :pos1 :x {P3}
SAVE/REGRESSION testa COEF
SELECT/TABLE testa all
COMPUTE/REGRESSION testa :fit = COEF
COPY/TT testa :fit testb :pos1
WRITE/OUT Positionsfit fertig
GOTO fitsigma
ENDIF
ENDDO
fitsigma:
SELECT/TABLE testa ABS(:sigma - 'sigstart') .ge. 0.001
select/tab testa SELECT .AND. ((:x .GE. 'XLIM(1)') .AND. (:x .LE. 'XLIM(2)'))

stat/tab testa :sigma
COMPUTE/KEY sigma = M$ABS(outputr(4))
@@ /crater/prg_linux/median testa aux :x :sigma 1
!                                 skip outliers:
COMPUTE/KEY sigma = kappa * sigma
READ/KEY  sigma
SELECT/TAB testa SELECT .AND. ABS(:sigma - {aux.tbl,MEDIAN}) .LE. 'sigma'

DO I = 1 IMAX
COMPUTE/KEY selrow = outputi(1)
REGRESSION/POLYNOMIAL testa  :sigma :x {P3} 
SAVE/REGRESSION testa COEF
COMPUTE/REGRESSION testa :fit = COEF
COMPUTE/TABLE testa :diff = :sigma - :fit
SELECT/TABLE testa ABS(:sigma - 'sigstart') .ge. 0.001

stat/tab testa :sigma
COMPUTE/KEY sigma = M$ABS(outputr(4))
@@ /crater/prg_linux/median testa aux :x :sigma 1
!                                 skip outliers:
COMPUTE/KEY sigma = kappa * sigma
READ/KEY  sigma

SELECT/TAB testa ABS(:sigma - 'sigstart') .ge. 0.001 .AND. ABS(:diff) .LE. 'sigma'
select/tab testa SELECT .AND. ((:x .GE. 'XLIM(1)') .AND. (:x .LE. 'XLIM(2)'))
IF OUTPUTI(1) .EQ. selrow .OR. I .EQ. IMAX THEN
REGRESSION/POLYNOMIAL testa :sigma :x {P3}
SAVE/REGRESSION testa COEF
SELECT/TABLE testa all
COMPUTE/REGRESSION testa :fit = COEF
COPY/TT testa :fit testb :sigma
WRITE/OUT sigmafit fertig

@@ /crater/prg_linux/median testa testc :x :delta 1
WRITE/TAB testb :delta @1.. {testc.tbl,MEDIAN}  

!
MERGE/TAB {P2} testb testout
COPY/TAB testout {P2}
SELECT/TABLE {P2} all
GOTO nextorder
ENDIF
ENDDO
nextorder:

!set/graph stype=1
!plot/tab {P1} :x :sigma
!plot/tab {P1} :x :pos1
!set/graph stype=0
!overplot/tab {P2} :x :sigma
!overplot/tab {P2} :x :pos1

ENDDO

SELECT/TABLE {P2} all
SELECT/TABLE test1 all





