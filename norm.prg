!  Procedure norm.prg                   (c) wt 2006
!
!proceso que normaliza el flujo y sigma

set/format i1

DEFINE/PARAMETER P1 ?	      C   "tabla de datos"
DEFINE/LOCAL T/c/1/100 {P1}

comp/tab {T} :normflux=:flux/:fit
comp/tab {T} :stdev=:sigma/:fit

FIN: