!
! MIDAS procedure  FIT_CONT2		                29.07.01 
!								(C)SVCLM
! Compare A continuum with B.
!
WRITE/OUT Procedure FIT_CONT
DEFINE/PARAMETER P1 ?      C   "Enter order:"
!
define/local N/i/1/1 {P1}
define/local ROOT/c/1/11 nothing
define/local ff/r/1/1 10000.
define/local ratio/r/1/1 0.001
set/format I4
!
!goto there

copy/tab r{N} test
copy/tab tableA_r{N} tabletest
write/out "click A"
plot/tab  test :wave :fitA
get/gcurs aux
comp/key ff = {aux.tbl,:y_axis,@1} 

write/out "click B"
!set/graph yaxis=0,0.05
plot/tab test :wave :fluxB
get/gcurs aux1
comp/key ratio = {aux1.tbl,:y_axis,@1}/{ff}
comp/tab test :fitB = :fitA* {ratio}
comp/tab tabletest :y_axis=:y_axis*{ratio}
set/graph color=2
overplot/tab test :wave :fitB
set/graph color=4 stype=5
overplot/tab tabletest :x_axis :y_axis
set/graph stype=0 color=3
overplot/ident tabletest :x_axis :seq 
set/graph color=1
comp/tab test :normfluxB=:fluxB/:fitB
