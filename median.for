*                 PROGRAMM MEDIAN.FOR                                 25.05.94
*                                                                        SVCLM

*                 Smooths column in tabelle with median filter.
*                 No selected values are skipped.
*
        implicit none

        real flux(100000),array(100000)
        real wave,median
        integer i,j,k,colwavei,colwaveo,colfluxi,colfluxo
        integer colmedian,radius,n,numselrow
        character*50 tabin,tabout,colind,coldep,colno,dsc

        integer kun,knul,stat,acts,numrow,numcol,nsc,acol,arow,status
        integer tidin,tidout,lonull,dun

        logical selflag         

        INCLUDE 'MID_INCLUDE:ST_DEF.INC'
        INCLUDE 'MID_INCLUDE:ST_DAT.INC'

        CALL STSPRO('MEDIAN')
*                  Tabelle zu gl"atten:
        CALL STKRDC('tabin',1,1,30,acts,tabin,kun,knul,stat)
*                  Tabelle mit gefilterte Daten:
        CALL STKRDC('tabout',1,1,50,acts,tabout,kun,knul,stat)
        CALL STKRDC('colind',1,1,30,acts,colind,kun,knul,stat)
        CALL STKRDC('coldep',1,1,30,acts,coldep,kun,knul,stat)
*                  Radius:
        CALL STKRDI('radius',1,1,acts,radius,kun,knul,stat) 
*                   OEffnung Input Tabelle:
        CALL TBTOPN(tabin,2,tidin,status)
        CALL TBCSER(tidin,colind,colwavei,status)
        CALL TBCSER(tidin,coldep,colfluxi,status)
        CALL TBIGET(tidin,numcol,numrow,nsc,acol,arow,status)

*OEffnung Output Tabelle:
        CALL TBTINI(tabout,0,1,3,numrow,tidout,status)

        CALL TBCINI(tidout,D_R4_FORMAT,1,'F8.2',' ',colind,
     +              colwaveo,status)
        CALL TBCINI(tidout,D_R4_FORMAT,1,'E11.2',' ',coldep,
     +              colfluxo,status)
        CALL TBCINI(tidout,D_R4_FORMAT,1,'E11.2',' ','median',
     +              colmedian,status)
*Ein- und Ausgabe von x- und y-columns: 
*Only selected rows are considered
        j=0
        do 5 i=1,numrow
                CALL TBSGET(tidin,i,selflag,status)
                IF (.NOT.selflag) GOTO 5
                j=j+1
                CALL TBERDR(tidin,i,colwavei,wave,lonull,status)
                CALL TBERDR(tidin,i,colfluxi,flux(j),lonull,status)
                CALL TBEWRR(tidout,j,colwaveo,wave,status)
                CALL TBEWRR(tidout,j,colfluxo,flux(j),status)
5       continue
        numselrow=j
*Radius als Descriptor in Output Tabelle:
        CALL STDWRI(tidout,'RADIUS',radius,1,1,dun,status)
*Copy all descriptors (resolution,wavestep)
        CALL STDCOP(tidin,tidout,3,dsc,status)
*Doppel loop for filtering:
        do 20 i=1,numselrow
                k=1
*Fenster an den Raenden wird kleiner:
                do 10 j=max(i-radius,1),min(i+radius,numrow)
*Elements zu filtern:
                        array(k)=flux(j)
                        k=k+1
10              continue

                CALL MDIAN1(array,k-1,median)
*Ausgabe gefilterter column:
                CALL TBEWRR(tidout,i,colmedian,median,status)
20      continue

30      continue

*Last call to compute median value of all rows:

        CALL MDIAN1(flux,numselrow,median)
        CALL STDWRR(tidout,'MEDIAN',median,1,1,dun,status)

        CALL TBTCLO(tidin,status)	
        CALL TBTCLO(tidout,status)	
        CALL STSEPI

        end

*-------------------NUMREC------------------------------------------------

      SUBROUTINE MDIAN1(X,N,XMED)
      DIMENSION X(N)
      CALL SORT(N,X)
      N2=N/2
      IF(2*N2.EQ.N)THEN
        XMED=0.5*(X(N2)+X(N2+1))
      ELSE
        XMED=X(N2+1)
      ENDIF
      RETURN
      END

      SUBROUTINE SORT(N,RA)
      DIMENSION RA(N)
      L=N/2+1
      IR=N
10    CONTINUE
        IF(L.GT.1)THEN
          L=L-1
          RRA=RA(L)
        ELSE
          RRA=RA(IR)
          RA(IR)=RA(1)
          IR=IR-1
          IF(IR.EQ.1)THEN
            RA(1)=RRA
            RETURN
          ENDIF
        ENDIF
        I=L
        J=L+L
20      IF(J.LE.IR)THEN
          IF(J.LT.IR)THEN
            IF(RA(J).LT.RA(J+1))J=J+1
          ENDIF
          IF(RRA.LT.RA(J))THEN
            RA(I)=RA(J)
            I=J
            J=J+J
          ELSE
            J=IR+1
          ENDIF
        GO TO 20
        ENDIF
        RA(I)=RRA
      GO TO 10
      END
























