
*PROGRAMM FITSPLINE1    		22.01.95
*------------------ 		(C)	SVCLM
*
*Fit cubic splines to the data in table TABCURSOR 
*and
*interpolate at step in table TABDATA creating a new
*column :CONT
*
      implicit none
*
*ARRAYS: wave and flux in table TABCURSOR -- deri(1) and deri(2): first
*derivatives at first and last points -- Y2: second derivatives
*of interpolated function.

      real*4 flux(5000),wave(5000),deri(2),Y2(5000),valx,valy,tol
      integer i,i1,i2,rad,numrowdata,numrowcursor,sigma
      integer numcol,nsc,arow,lonull,acol,NDATA

      integer tiddata,tidcursor,acts,kun,knul,status,colwave,colflux
      integer colwavedata,colcont,nc
      character*50 tabdata,tabcursor,col1,col2
      logical selflag

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'

      CALL STSPRO('fitspline1')

*Read parameters from keywords:



      CALL STKRDC('TABDATA',1,1,30,acts,tabdata,kun,knul,status)
      CALL STKRDC('TABCUR',1,1,30,acts,tabcursor,kun,knul,status)
      CALL STKRDC('indcol',1,1,30,acts,col1,kun,knul,status)
      CALL STKRDC('depcol',1,1,30,acts,col2,kun,knul,status)
      CALL STKRDR('DERIV',1,2,acts,deri,kun,knul,status)
      CALL STKRDR('tolerance',1,1,acts,tol,kun,knul,status)

      CALL TBTOPN(tabdata,2,tiddata,status)
      CALL TBTOPN(tabcursor,2,tidcursor,status)
      CALL TBCSER(tidcursor,col1,colwave,status)
      CALL TBCSER(tidcursor,col2,colflux,status)
      CALL TBCSER(tiddata,':wave',colwavedata,status)

      CALL TBCSER(tiddata,':fit',colcont,status)
      CALL TBIGET(tiddata,numcol,numrowdata,nsc,acol,arow,status)
      CALL TBIGET(tidcursor,numcol,numrowcursor,nsc,acol,arow,status)
c     write(*,*) "hola",tiddata,numrowdata
      if (colcont .le. 0) then

           CALL TBCINI(tiddata,D_R4_FORMAT,1,'F7.4',' ',':fit',
     +     colcont,status)
      endif







*Sort data in table TABCURSOR:

      CALL TBCSRT(tidcursor,1,colwave,1,status)

*Read data from table TABCURSOR:
*Only selected rows are considered
      NDATA=0
      do 5 i=1,numrowcursor

         CALL TBSGET(tidcursor,i,selflag,status)
         IF (.NOT.selflag) GOTO 5

         NDATA=NDATA+1
         CALL TBERDR(tidcursor,i,colwave,wave(NDATA),lonull,status)
         CALL TBERDR(tidcursor,i,colflux,flux(NDATA),lonull,status)

5     continue

*Search for first and last element in table TABDATA:

      CALL TBESRR(tiddata,colwavedata,wave(1),tol,1,i1,status)
      CALL TBESRR(tiddata,colwavedata,wave(NDATA),tol,1,
     +  i2,status)

c      write(*,*) wave(1),wave(NDATA),i1,i2,NDATA,tol

c      write(*,*) i1,i2

*Find spline coefficients:

      CALL SPLINE(wave,flux,NDATA,deri(1),deri(2),Y2)

*Find values for intermediate points along table TABDATA and
*write them in column :CONT considering this time all points
*between wave(1) and wave(NDATA)

c      write(*,*) i1,i2
      do 10 i=i1,i2

c         CALL TBSGET(tiddata,i,selflag,status)
c         IF (.NOT.selflag) GOTO 10

         CALL TBERDR(tiddata,i,colwavedata,valx,lonull,status)
         CALL SPLINT(wave,flux,Y2,NDATA,valx,valy)
         CALL TBEWRR(tiddata,i,colcont,valy,lonull,status)
10    continue

      CALL TBTCLO(tiddata,status)
      CALL TBTCLO(tidcursor,status)
      CALL STSEPI
      end

******************************************************************************
* 	SUBROUTINES:

      SUBROUTINE SPLINE(X,Y,N,YP1,YPN,Y2)
      PARAMETER (NMAX=5000)
      DIMENSION X(N),Y(N),Y2(N),U(NMAX)
      IF (YP1.GT..99E30) THEN
        Y2(1)=0.
        U(1)=0.
      ELSE
        Y2(1)=-0.5
        U(1)=(3./(X(2)-X(1)))*((Y(2)-Y(1))/(X(2)-X(1))-YP1)
      ENDIF
      DO 11 I=2,N-1
        SIG=(X(I)-X(I-1))/(X(I+1)-X(I-1))
        P=SIG*Y2(I-1)+2.
        Y2(I)=(SIG-1.)/P
        U(I)=(6.*((Y(I+1)-Y(I))/(X(I+1)-X(I))-(Y(I)-Y(I-1))
     *      /(X(I)-X(I-1)))/(X(I+1)-X(I-1))-SIG*U(I-1))/P
11    CONTINUE
      IF (YPN.GT..99E30) THEN
        QN=0.
        UN=0.
      ELSE
        QN=0.5
        UN=(3./(X(N)-X(N-1)))*(YPN-(Y(N)-Y(N-1))/(X(N)-X(N-1)))
      ENDIF
      Y2(N)=(UN-QN*U(N-1))/(QN*Y2(N-1)+1.)
      DO 12 K=N-1,1,-1
        Y2(K)=Y2(K)*Y2(K+1)+U(K)
12    CONTINUE
      RETURN
      END

******************************

      SUBROUTINE SPLINT(XA,YA,Y2A,N,X,Y)
      DIMENSION XA(N),YA(N),Y2A(N)
      KLO=1
      KHI=N
1     IF (KHI-KLO.GT.1) THEN
        K=(KHI+KLO)/2
        IF(XA(K).GT.X)THEN
          KHI=K
        ELSE
          KLO=K
        ENDIF
      GOTO 1
      ENDIF
      H=XA(KHI)-XA(KLO)
      IF (H.EQ.0.) PAUSE 'Bad XA input.'
      A=(XA(KHI)-X)/H
      B=(X-XA(KLO))/H
      Y=A*YA(KLO)+B*YA(KHI)+
     *      ((A**3-A)*Y2A(KLO)+(B**3-B)*Y2A(KHI))*(H**2)/6.
      RETURN
      END

