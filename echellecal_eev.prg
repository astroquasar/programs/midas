!
!PROCEDURE ECHELLECAL.  (C) SEBASTIAN LOPEZ APR.95
!Wavelength Calibration in EMMI Echelle Frames. Session is already init.
!(SAVE/ECHELLE Sesion name afterwards!)
!
!
define/parameter p1 ? I "Enter frame with Th-Ar spectrum:"
!
!
copy/tab order_eev order
set/echelle nbordi=20 slit=3 offset=-1
set/echelle wlcmtd=restart   wlc={p1} width2=2.5 thres2=400  tol=0.2 dc=4 seamtd=gravity
set/echelle WLCITER=1,0.3,20,5.0,4.0  WLCNITER = 5,10 LINCAT=thar.tbl

!
cal/echelle done
!
!
!LINES USED (EEV):  (frames divided by 3-5)
!
!5720.183  order 106
!5067.973  order 120

!for 860: 7 lines  used (method angle):
!72
!8416.727
!8252.393
!8085.219 
!7891.075
!7817.770
!7270.660
!6787.736



!** Identification loop **
! Initial accuracy          : 1.0 pixel
! Next neighbour parameter  : 0.1
! Maximal error             : 1.0 pixel
! Initial error             : 2.0 pixel
! Iteration error           : 1.0 std dev.

!** Rejection loop **
! Tolerance                 : 0.5
