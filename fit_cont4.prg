!
! MIDAS procedure  FIT_CONT3		                29.07.01 
!								(C)SVCLM
! Correct tableA_000{N} to fit B.
!
WRITE/OUT Procedure FIT_CONT
DEFINE/PARAMETER P1 ?      C   "Enter order:"
!
define/local N/i/1/1 {P1}
define/local N1/i/1/1 {tableA_{N}.tbl,TBLCONTR(4)}
define/local I/i/1/1 1
define/local wave/r/1/1 1.
define/local wave1/r/1/1 1.
define/local wave2/r/1/1 1.
define/local y/r/1/1 1.
set/format I4
!
!goto there

sort/tab tabletest :x_axis
comp/tab tabletest :seq=seq
copy/tab tabletest tableB_r{N}

@@ /home/slopez/he1104/prg/fitspline1 r{N} tableB_r{N} ? ? 0,0
comp/tab r{N} :fitB = :fit
comp/tab r{N} :normfluxB = :fluxB / :fitB
comp/tab r{N} :stdevB = :sigmaB / :fitB

plot/tab r{N} :wave :fluxB
set/graph color=2 lwidth=3
overplot/tab r{N} :wave :fitB
set/graph color=4 stype=5 lwidth=1
overplot/tab tableB_r{N} :x_axis :y_axis
set/graph color=3
overplot/ident tableB_r{N} :x_axis :seq 
set/graph color=1 stype=0