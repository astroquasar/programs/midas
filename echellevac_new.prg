!
! MIDAS procedure  ECHELLEVAC_NEW		 	        24.10.00 
!								(C)SVCLM
! Convert to vacuum AND heliocentric wavelengths
!
!
WRITE/OUT Procedure ECHELLEVAC_NEW. Column ":wave" to ":wave_hvac"
!
DEFINE/PARAMETER P1 ?      C   "Enter root name:"
DEFINE/PARAMETER P2 ?      C   "Enter order1,order2:"
DEFINE/PARAMETER P3 ?      C   "Enter radial velocity:"
!
define/local N/i/1/1 1
define/local ORDER/i/1/2 {P2}
set/format I4
!
DO N = {ORDER(1)} {ORDER(2)}
!
WRITE/OUT Converting to vacuum

!comp/tab order{N} :flux = :flux/1.67
!comp/tab order{N} :sigma = :sigma*2.7
copy/tab  {P1}{N} test
comp/tab test :n_air = 1 + 6432.8E-8 + 2949810/(146E8-(1/:wave**2)*1.E16) + 25540/(41E8-(1/:wave**2)*1.E16)
comp/tab test :wave_vac=:wave*:n_air

WRITE/OUT Converting to heliocentric

comp/tab test :wave_hvac=:wave_vac*(1+{P3}/299790.)
copy/tt test :wave_hvac {P1}{N} :wave_hvac
!
ENDDO
