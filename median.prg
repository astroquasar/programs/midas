! MIDAS procedure  MEDIAN		 			26.05.94 
!								(C)SVCLM
! Median-Filter "uber column in Tabelle.
! Median of all values in dep. column is stored
! in descriptor "MEDIAN". 
! No selected values are skipped.
!
WRITE/OUT Procedure Median
DEFINE/PARAMETER P1 ?      C   "Enter input table:"
DEFINE/PARAMETER P2 ?      C   "Enter output table:"
DEFINE/PARAMETER P3 :wave  C   "Enter ind. column:"
DEFINE/PARAMETER P4 :flux  C   "Enter dep. column:"
DEFINE/PARAMETER P5 ?      C   "Enter radius in pixels:"
!
write/key tabin/c/1/50       {P1}
write/key tabout/c/1/50      {P2}
write/key colind/c/1/50      {P3}
write/key coldep/c/1/50      {P4}
write/key radius/i/1/1       {P5}
RUN /Users/vincent/prog/midas/median.exe

