!
! Procedure ECHELLEREB                                      (C) SVCLM
!                                                           July 1995
! Rebin COLUMN in input TABLE.                              
!
!
DEFINE/PAR P1     ?     TABLE   "Enter input table"
DEFINE/PAR P2     ?     IMAGE   "Enter output image"
DEFINE/PAR P6     ?       C     "Column to rebin"
DEFINE/PAR P3  {SAMPLE}   ?     "Sample or reference image:"
DEFINE/PAR P4  {REBMTD}   C     "Method: LINEAR, NONLINEAR"
DEFINE/PAR P5  {LINTAB}  TABLE  "Auxiliary table"
!
WRITE/OUT rebinning column {P6}
!
! Case unsensitivity and check method
!
DEFINE/LOCAL METHOD/C/1/12  "YYYYYYYYYYYY"
IF P4(1:1) .EQ. "L"   THEN
   WRITE/KEY METHOD/C/1/12 "LINEAR      "
   WRITE/OUT "REBIN/ECHELLE: Sorry, method LINEAR is not supported yet"
   RETURN/EXIT
ENDIF
IF P4(1:1) .EQ. "N"   WRITE/KEY METHOD/C/1/12 "NONLINEAR   "
IF METHOD(1:1) .EQ. "Y" THEN
   WRITE/OUT "REBIN/ECHELLE: unknown method {P4}"
   RETURN/EXIT
ENDIF
!
IF P3(1:1) .EQ. "+"  ERROR/ECHELLE  REBIN/ECHELLE  SAMPLE
!
!SET/ECH  REBMTD={METHOD}  SAMPLE={P3}
!
!
WRITE/KEY   IN_A/C/1/60      {P1}
WRITE/KEY   IN_COL/C/1/60    {P6}
WRITE/KEY   IN_B           {P5}  ! Auxiliary table
WRITE/KEY   OUT_A          {P2}
WRITE/KEY   P4/C/1/12      {METHOD}
!
RUN /musica2/prg_linux/echellereb_new
!
! Descriptors transfer is performed in the program. New descriptors 
! WSTART and NPTOT are created.
!
RETURN







