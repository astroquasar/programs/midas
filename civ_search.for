*PROGRAMM CIV_SEARCH		14.06.99
*------------------ 		(C)	SVCLM
*
*Search civ and compute chi_min in Keck HIRES spectra (method from Diploma) 
*
      implicit none
*
      real*4 wave,lambda,TOL,P,z,PMIN,wave0,dv,dv1,redshift,sum1,sum2
      real*4 deltaz(2),error,sigma,g_i,l1,l2,dz,alpha,flux,stdev
      real*4 chiquad,eqw_1548
      integer i,i1,i2,i_l1,i_l2rad,numrowdata,numrowcursor,row
      integer numcol,nsc,arow,lonull,acol,NDATA,N

      integer tiddata,tidcursor,acts,kun,knul,status,colalpha
      integer colwavedata,colcont,nc,col(2),colflux,colsigma,colwave,aux
      integer colz,colchiquad
      character*20 tabdata,tabname3
      character*10 ID
      logical selflag

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'


      CALL STSPRO('civ_search')

*Read parameters from keywords:

      CALL STKRDC('TABLE',1,1,20,acts,tabdata,kun,knul,status)
      CALL STKRDR('deltaz',1,2,acts,deltaz,kun,knul,status)
      CALL STKRDC('TABLEOUT',1,1,20,acts,tabname3,kun,knul,status)

c      write(*,*) tabdata,deltaz(1),deltaz(2),tabname3

c Open table with input parameters:

      CALL TBTOPN(tabdata,3,tiddata,status)
      CALL TBCSER(tiddata,':normflux',colflux,status)
      CALL TBCSER(tiddata,':stdev',colsigma,status)
      CALL TBCSER(tiddata,':wave',colwave,status)

      CALL TBIGET(tiddata,numcol,numrowdata,nsc,acol,arow,status)

c Open table with output parameters:
         CALL TBTINI(tabname3,0,1,2,10000,tiddata3,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':z',
     +               colz,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':alpha',
     +               colalpha,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':eqw',
     +               coleqw,status)
         CALL TBCINI(tiddata3,D_R4_FORMAT,1,'F9.4',' ',':chiquad',
     +               colchiquad,status)

c Data: error to find boundaries; sigma line in pixels,dz: interval in z
      error=0.06        
      sigma=3       
      dz=0.00005
c      dz=0.00003

c********
c      do 200 alpha=0,3,0.01
c*******

      i_z=1
      do 100 z=deltaz(1),deltaz(2),dz
c search boundaries:
      x1=(1+deltaz(1))*1548.195
      x2=(1+deltaz(2))*1548.195
      CALL TBESRR(tiddata,colwave,x1,error,1,i1,status)
      CALL TBESRR(tiddata,colwave,x2,error,1,i2,status)
c      write(*,*) x1,x2,i1,i2

c Position of doublet in AA and in pixels:
      l1=(1+z)*1548.1949
      l2=(1+z)*1550.7700
      CALL TBESRR(tiddata,colwave,l1,error,1,i_l1,status)
      CALL TBESRR(tiddata,colwave,l2,error,1,i_l2,status)
c      write(*,*) i_l1,i_l2

      sum1=0.
      sum2=0.
      N=1
      do 50 i=i_l1-30,i_l2+30
c c_iv doublet line profile
      g_i=-0.19080*exp(-(i-i_l1)**2/(2*sigma**2)) + 
     + -0.09522*exp(-(i-i_l2)**2/(2*sigma**2))
c Read data values
         CALL TBERDR(tiddata,i,colflux,flux,lonull,status)
         CALL TBERDR(tiddata,i,colsigma,stdev,lonull,status)
      sum1=sum1+(flux-1)*g_i/stdev**2
      sum2=sum2+(g_i/stdev)**2
      N=N+1
 50   continue         

c*************************
c best alpha for a z:
      alpha=sum1/sum2
      eqw_1548=2.506628*alpha*0.19080*sigma*0.04

c       alpha=5
c*************************

      chiquad=0.            
      do 60 i=i_l1-30,i_l2+30
         CALL TBERDR(tiddata,i,colflux,flux,lonull,status)
         CALL TBERDR(tiddata,i,colsigma,stdev,lonull,status)
      g_i=-0.19080*exp(-(i-i_l1)**2/(2*sigma**2)) + 
     + -0.09522*exp(-(i-i_l2)**2/(2*sigma**2))
         chiquad=chiquad+((flux-1-alpha*g_i)/stdev)**2
c      write(*,*) i,g_i
60     continue
c      write(*,*) z,alpha,chiquad

         CALL TBEWRR(tiddata3,i_z,colz,z,lonull,status)    
         CALL TBEWRR(tiddata3,i_z,colalpha,alpha,lonull,status)    
         CALL TBEWRR(tiddata3,i_z,coleqw,eqw_1548,lonull,status)    
         CALL TBEWRR(tiddata3,i_z,colchiquad,chiquad,lonull,status)    
      i_z=i_z+1

100    continue    

200    continue

      CALL TBTCLO(tiddata,status)
      CALL STSEPI

      end

