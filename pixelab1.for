
*PROGRAMM PixelAB		21.09.03
*------------------ 		(C)	SVCLM
*
* Use POD Method to recover optical depth of HI and metals.
*
*

       PROGRAM PIXELAB
       IMPLICIT NONE

c For data:
      real X(300000),Y(300000),YSIG(300000),Y1(300000),YSIG1(300000)

c General:
      integer numcol,numrow,nsc,arow,lonull,acol,nlines
      integer tiddata1,tiddata2,acts,kun,knul,i,j,k,colx
      integer coly,colysig,coly1,colysig1
      integer colredshift,coltHI,coltmetal
      integer colzlya,colz1548,coltHIB,coltmetalB
      integer i_l1,i_l2,lim1,lim2,numz
      character*40 tabname1,tabname2,tabname3,col1,col2
      logical selflag
      real z,tau_HI,tau_j,tau,tau_metal,l1,l2,N_sigma
      real factor(8),lambda(8)
c For graphic:
      integer access,plmode,key,status,maxvals,actvals,lines(20)
      integer stype,ltype,bin,yoff,ssize,other
      real xcur,ycur,x1,x2

c codes:
      real  wave(7000),range(2),tol,liney(2),xaxis(4),yaxis(4)
      integer ipos,yc,xc,bmode

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'


      DATA      access/1/
      DATA      maxvals/20/
      DATA      plmode/-1/
c      DATA      bin/1/
      DATA      STYPE/0/
      DATA      YOFF/0.0/
      DATA      npix/2/      
      DATA      pixsize/0.04/      

                          
      CALL STSPRO('pixel')
c Read keywords:
      CALL STKRDC('tableaux',1,1,40,acts,tabname1,kun,knul,status)
      CALL STKRDC('tableout',1,1,40,acts,tabname2,kun,knul,status)
      CALL STKRDR('range',1,2,acts,range,kun,knul,status)
      CALL STKRDR('tol',1,1,acts,tol,kun,knul,status)
      CALL STKRDR('N_sigma',1,1,acts,N_sigma,kun,knul,status)


c Open table with input data:
      CALL TBTOPN(tabname1,2,tiddata1,status)
      CALL TBIGET(tiddata1,numcol,numrow,nsc,acol,arow,status)

c Search for columns with data 
      CALL TBCSER(tiddata1,':wave',colx,status)
      CALL TBCSER(tiddata1,':normfluxA',coly,status)
      CALL TBCSER(tiddata1,':stdevA',colysig,status)
      CALL TBCSER(tiddata1,':normfluxB',coly1,status)
      CALL TBCSER(tiddata1,':stdevB',colysig1,status)

c Open table with output parameters:
      CALL TBTOPN(tabname2,2,tiddata2,status)
c Search for columns redshift, tau1 and tau2:
      CALL TBCSER(tiddata2,':redshift',colredshift,status)
      CALL TBCSER(tiddata2,':tau_HI_A',coltHI,status)
      CALL TBCSER(tiddata2,':tau_metal_A',coltmetal,status)
      CALL TBCSER(tiddata2,':tau_HI_B',coltHIB,status)
      CALL TBCSER(tiddata2,':tau_metal_B',coltmetalB,status)

c Read table data betwwen 1 and numrow (only selected rows are considered):
c the selection will discard automatically pixels with tau<0 and pixels
c with flux<0
      NDATA=0
      do 30 i = 1,numrow
         CALL TBSGET(tiddata1,i,selflag,status)
         IF (.NOT.selflag) GOTO 30
         NDATA=NDATA+1
         CALL TBERDR(tiddata1,i,colx,X(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,coly,Y(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,colysig,YSIG(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,coly1,Y1(NDATA),lonull,status)
         CALL TBERDR(tiddata1,i,colysig1,YSIG1(NDATA),lonull,status)
30    continue

c Get limits on z_lya:
      l1 = 1215.6700*(range(1)+1.)
      l2 = 1215.6700*(range(2)+1.)
      CALL TBESRR(tiddata1,colx,l1,tol,1,lim1,status)
      CALL TBESRR(tiddata1,colx,l2,tol,1,lim2,status)

c Auxiliary data
      numz = lim2 - lim1 + 1
      NDATA=lim1
      lambda(1)=1215.6700
      factor(1)=1.
      lambda(2)=1025.7223
      factor(2)=1215.6700*0.4162/(1025.7223*0.0791)
      lambda(3)=972.5368
      factor(3)=1215.6700*0.4162/(972.5368*0.02899)
      lambda(4)=949.7430
      factor(4)=1215.6700*0.4162/(949.7430*0.01394)


c MAIN LOOP OVER ALL REDSHIFTS:
      do 40 i = 1,numz
c     Redshift and tau from Ly-a:
         z= X(NDATA)/1215.67 - 1.
         CALL TBEWRR(tiddata2,i,colredshift,z,lonull,status)
c Spectrum A
         tau_HI = 100000.
c        LOOP OVER LYMAN SERIES LINES:
         do 35 j = 1,4
           l1=(1+z)*lambda(j)
           CALL TBESRR(tiddata1,colx,l1,tol,1,i_l1,status)
           tau_j =  -LOG(Y(i_l1))*factor(j)
           write(*,*) z,lambda(j),Y(i_l1),YSIG(i_l1),tau_j
           IF (((Y(i_l1).ge.(N_sigma*YSIG(i_l1))).and.(Y(i_l1).le.
     +        (1-N_sigma*YSIG(i_l1)))).and.(tau_j.lt.tau_HI))  THEN 
              tau_HI=tau_j
           ENDIF
35       continue      
c        Discard pixel if all series pixels were poorly constrained
c         write(*,*) tau_HI
         IF (tau_HI.lt.100000.)  CALL TBEWRR(tiddata2,i,coltHI,
     +                                       tau_HI,lonull,status)

c
c Get position of doublet in AA and in pixels:
         l1=(1+z)*1548.1949
         l2=(1+z)*1550.7700
         CALL TBESRR(tiddata1,colx,l1,tol,1,i_l1,status)
         CALL TBESRR(tiddata1,colx,l2,tol,1,i_l2,status)
c Compute tau's
         tau_1548 =  -LOG(Y(i_l1))
         tau_1550 =  -LOG(Y(i_l2))
c negative tau in 1548:
         IF (Y(i_l1).gt.(1.-N_sigma*YSIG(i_l1))) tau_1548 =  100000.  
         IF (Y(i_l2).lt.(1.-N_sigma*YSIG(i_l2))) THEN
            tau_metal = min(tau_1548,tau_1550*2)
         ELSE 
            tau_metal = tau_1548 
         ENDIF
         IF (tau_metal.eq.100000.) GOTO 37
         write(*,*) z,1548,i_l1,Y(i_l1),YSIG(i_l1),tau_1548
         write(*,*) z,1550,i_l2,Y(i_l2),YSIG(i_l2),tau_1550*2
ctau_metal = 0.00001
c
c
c Write data on tiddata2
c         write(*,*) tau_metal
         CALL TBEWRR(tiddata2,i,coltmetal,tau_metal,lonull,status)
c
37       continue     
c        spectrum B: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         tau_HI = 100000.
c        LOOP OVER LYMAN SERIES LINES:
         do 38 j = 1,4
           l1=(1+z)*lambda(j)
           CALL TBESRR(tiddata1,colx,l1,tol,1,i_l1,status)
           tau_j =  -LOG(Y1(i_l1))*factor(j)
c           write(*,*) z,lambda(j),Y1(i_l1),YSIG1(i_l1),tau_j
           IF (((Y1(i_l1).ge.(N_sigma*YSIG1(i_l1))).and.(Y1(i_l1).le.
     +        (1-N_sigma*YSIG1(i_l1)))).and.(tau_j.lt.tau_HI))  THEN 
              tau_HI=tau_j
           ENDIF
38       continue      
c        Discard pixel if all series pixels were poorly constrained
c         write(*,*) tau_HI
         IF (tau_HI.lt.100000.) CALL TBEWRR(tiddata2,i,coltHIB,tau_HI,
     +                                      lonull,status)

c
c Get position of doublet in AA and in pixels:
         l1=(1+z)*1548.1949
         l2=(1+z)*1550.7700
         CALL TBESRR(tiddata1,colx,l1,tol,1,i_l1,status)
         CALL TBESRR(tiddata1,colx,l2,tol,1,i_l2,status)
c Compute tau's
         tau_1548 =  -LOG(Y1(i_l1))
         tau_1550 =  -LOG(Y1(i_l2))
c negative tau in 1548:
         IF (Y1(i_l1).gt.(1.-N_sigma*YSIG1(i_l1))) tau_1548 =  100000.  
         IF (Y1(i_l2).lt.(1.-N_sigma*YSIG1(i_l2))) THEN
            tau_metal = min(tau_1548,tau_1550*2)
         ELSE 
            tau_metal = tau_1548 
         ENDIF
         IF (tau_metal.eq.100000.) GOTO 39
c         write(*,*) z,1548,i_l1,Y1(i_l1),YSIG1(i_l1),tau_1548
c         write(*,*) z,1550,i_l2,Y1(i_l2),YSIG1(i_l2),tau_1550*2
ctau_metal = 0.00001
c
c
c Write data on tiddata2
         CALL TBEWRR(tiddata2,i,coltmetalB,tau_metal,lonull,status)
39    continue      
         NDATA=NDATA+1
40    continue   

c      write(*,*)  "hola"

c Compute new flux and plot in velocity:
c      LTYPE = 1

c      DO 65 j = 1,nlines
c         write(*,*) lines(j),wave(lines(j))
c         DO 60 i = 1,numrow
c            IF (wave(lines(j)).EQ.0) GOTO 65
c            V(i)=(X(i)/(wave(lines(j))*(1.+z))-1.)*299790.
c            YNEW(i)=Y(i)/nlines  + (j-1.)/nlines
60       CONTINUE

c plot data:
c         CALL PTDATA(STYPE,LTYPE,BIN,V,YNEW,YOFF,NDATA)

c         CALL PTDATA(STYPE,LTYPE,other,V,YNEW,YOFF,NDATA)
c plot lines:
c         liney(1)=1./nlines  + (j-1.)/nlines
c         liney(2)=1./nlines  + (j-1.)/nlines
c         CALL PTDATA(STYPE,3,OFF,range,liney,YOFF,2)
c         write(text,1000) ion(lines(j)),wave(lines(j)) 
1000     format(a6,F6.1)
c         CALL PTTEXT(text,range(1)*.98,(j+0.15),0.,1.2,1) 
c         CALL PTTEXT(text,range(1)*.98,(j+0.05),0.,1.2,1) 


65    CONTINUE
      CALL TBTCLO(tiddata1,status)
      CALL TBTCLO(tiddata2,status)
c      CALL PTKDEF
c      CALL PTCLOS()
        CALL STSEPI
      end











