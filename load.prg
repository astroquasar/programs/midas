!Procedure LOAD     Loads shifted echelle orders
!                                                 (c) SEBASTIAN LOPEZ 30.03.95
clear/channel over 
comp/tab order :YOFFSET = :YFIT + {OFFSET}
load/tab order.tbl :X :YOFFSET :ORDER  1 1 2
