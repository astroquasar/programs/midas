DEFINE/LOCAL N/I/1/2 1
SET/FORMAT I4

do N = 1 13

!blue

copy/tab Q1355A_1_{N} ref_tab
rebin/tt ref_tab :wave,:flux Q1355A_1_{N} :wave_hvac,:flux LIN 0.,1. PIX
rebin/tt ref_tab :wave,:sigma Q1355A_1_{N} :wave_hvac,:sigma LIN 0.,1. PIX
copy/tab ref_tab Q1355A_1_{N}
!

copy/tab Q1355A_2_{N} ref_tab
rebin/tt ref_tab :wave,:flux Q1355A_2_{N} :wave_hvac,:flux LIN 0.,1. PIX
rebin/tt ref_tab :wave,:sigma Q1355A_2_{N} :wave_hvac,:sigma LIN 0.,1. PIX
copy/tab ref_tab Q1355A_2_{N}
!

copy/tab Q1355A_3_{N} ref_tab
rebin/tt ref_tab :wave,:flux Q1355A_3_{N} :wave_hvac,:flux LIN 0.,1. PIX
rebin/tt ref_tab :wave,:sigma Q1355A_3_{N} :wave_hvac,:sigma LIN 0.,1. PIX
copy/tab ref_tab Q1355A_3_{N}
!



!copy/tab 1335A_4_{N} ref_tab
!rebin/tt ref_tab :wave,:flux 1335A_4_{N} :wave_hvac,:flux LIN 0.,1. PIX
!rebin/tt ref_tab :wave,:sigma 1335A_4_{N} :wave_hvac,:sigma LIN 0.,1. PIX
!copy/tab ref_tab 1335A_4_{N}


enddo
