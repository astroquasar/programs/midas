! MIDAS procedure  CIV_SEARCH		 			17.08.98 
!								(C)SVCLM
!
! Create random number and store it in keyword RAN
!
DEFINE/PARAMETER P1 ?           C   "Enter IDUM:"

!
write/key idum/r/1/1    'P1'
write/key RAN/r/1/1    1.
!
run /crater/prg_linux/create_random

write/out 'RAN'
