!
! MIDAS procedure  COMBI1_3		 	                03.11.00 
!								(C)SVCLM
! same as combi_2243 but for 7 spectra instead of 4
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1_3, adding 3 exposures
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter input table 3:"
DEFINE/PARAMETER P4 ?      C   "Enter input table 4:"
DEFINE/PARAMETER P5 ?      C   "Enter input table 5:"
DEFINE/PARAMETER P6 ?      C   "Enter input table 6:"
DEFINE/PARAMETER P7 ?      C   "Enter input table 7:"
DEFINE/PARAMETER P8 ?      C   "Enter output table:"
!
!         !!!!!!!!!!!  A:          
!
copy/tab {P1} {P8}
comp/tab {P4} :flux1 = :fluxA 
comp/tab {P4} :sigma1 = :sigmaA 
!
copy/tt {P2} :fluxA {P8} :flux2
copy/tt {P2} :sigmaA {P8} :sigma2
!
copy/tt {P3} :fluxA {P8} :flux3
copy/tt {P3} :sigmaA {P8} :sigma3
!
copy/tt {P4} :fluxA {P8} :flux4
copy/tt {P4} :sigmaA {P8} :sigma4
!
copy/tt {P5} :fluxA {P8} :flux5
copy/tt {P5} :sigmaA {P8} :sigma5
!
copy/tt {P6} :fluxA {P8} :flux6
copy/tt {P6} :sigmaA {P8} :sigma6
!
copy/tt {P7} :fluxA {P8} :flux7
copy/tt {P7} :sigmaA {P8} :sigma7
!
!                         Coadd flux and sky:
!
comp/tab {P8} :sigmaA = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2+1/:sigma4**2+1/:sigma5**2+1/:sigma6**2+1/:sigma7**2))

comp/tab {P8} :fluxA = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2+:flux4/:sigma4**2+:flux5/:sigma5**2+:flux6/:sigma6**2+:flux7/:sigma7**2)*:sigmaA**2

!         !!!!!!!!!!!  B:
          
comp/tab {P8} :flux1 = :fluxB
comp/tab {P8} :sigma1 = :sigmaB 
!
copy/tt {P2} :fluxB {P8} :flux2
copy/tt {P2} :sigmaB {P8} :sigma2
!
copy/tt {P3} :fluxB {P8} :flux3
copy/tt {P3} :sigmaB {P8} :sigma3
!
copy/tt {P4} :fluxB {P8} :flux4
copy/tt {P4} :sigmaB {P8} :sigma4
!
copy/tt {P5} :fluxB {P8} :flux5
copy/tt {P5} :sigmaB {P8} :sigma5
!
copy/tt {P6} :fluxB {P8} :flux6
copy/tt {P6} :sigmaB {P8} :sigma6
!
copy/tt {P7} :fluxB {P8} :flux7
copy/tt {P7} :sigmaB {P8} :sigma7
!
!                         Coadd flux and sky:
!
comp/tab {P8} :sigmaB = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2+1/:sigma4**2+1/:sigma5**2+1/:sigma6**2+1/:sigma7**2))

comp/tab {P8} :fluxB = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2+:flux4/:sigma4**2+:flux5/:sigma5**2+:flux6/:sigma6**2+:flux7/:sigma7**2)*:sigmaB**2



del/col {P8} :flux1,:flux2,:flux3,:flux4,:flux5,:flux6,:flux7,:sigma1,:sigma2,:sigma3,:sigma4,:sigma5,:sigma6,:sigma7
