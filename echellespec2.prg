!
!  Procedure ECHELLESPEC2                                 (C) SVCLM
!                                                         Oct 1995
!  Rebin columns in INPUT table for all orders
!  and copy them to OUTPUT table.
!
DEFINE/PAR P1     ?     TABLE   "Enter input table"
DEFINE/PAR P2     ?     TABLE   "Enter output table's root name"
DEFINE/PAR P3     ?     I       "Enter order1,order2"
DEFINE/LOCAL ORDER/I/1/2 {P3}
DEFINE/LOCAL N/I/1/2 1
!                        
!                                       
!                                  amplitude int1 (int2):
SET/FORMAT I4
@@ /musica2/prg_linux/echellereb {P1} reb reb_reference ? ? :flux
merge/echelle reb test {ORDER(1)},{ORDER(2)} noappend
do N = {ORDER(1)} {ORDER(2)}
copy/it test{N} {P2}{N} :wave
copy/dd test{N} step {P2}{N}.tbl step
name/column {P2}{N} :LAB002 :fluxA
enddo
!                                  stddev error1 (error2):
@@ /musica2/prg_linux/echellereb {P1} reb reb_reference ? ? :stdev
merge/echelle reb test {ORDER(1)},{ORDER(2)} noappend
do N = {ORDER(1)} {ORDER(2)}
copy/it test{N} test :wave
copy/tt test :LAB002 {P2}{N} :sigmaA
enddo

goto there

!                                  stddev error1 (error2):
@@ /musica2/prg_linux/echellereb {P1} reb reb_reference ? ? :fluxB
merge/echelle reb test {ORDER(1)},{ORDER(2)} noappend
do N = {ORDER(1)} {ORDER(2)}
copy/it test{N} test :wave
copy/tt test :LAB002 {P2}{N} :fluxB
enddo

!                                  stddev error1 (error2):
@@ /musica2/prg_linux/echellereb {P1} reb reb_reference ? ? :stdevB
merge/echelle reb test {ORDER(1)},{ORDER(2)} noappend
do N = {ORDER(1)} {ORDER(2)}
copy/it test{N} test :wave
copy/tt test :LAB002 {P2}{N} :sigmaB
enddo




there: