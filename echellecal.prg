! wavelength calibration MIKE May 2004


set/echelle offset=3 slit=1 extmtd=average !! importante aqui el offset!

set/echelle wlcmtd=pair   wlc={p1} width2=4.0 thres2=90  tol=0.9 dc=3 seamtd=gravity
set/echelle WLCITER=3,0.1,20,1.0,6.0  WLCNITER = 80,100 LINCAT=MagE_thar.tbl

!** Identification loop **
! Initial accuracy          : 3.0 pixel   WLCITER (1)
! Next neighbour parameter  : 0.9         WLCITER (2)  important
! Maximal error             : 20.0 pixel  WLCITER (3)
! Initial error             : 5.0 pixel   WLCITER (4)
! Iteration error           : 4.0 std dev. WLCITER (5)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
cal/echelle done
!
!
!LINES USED:
!
!obj 2 blue:
!Enter absolute order number of first pointed line (square mark) : 76
!Sequence no. 0001, Order no. 0076. Enter wavelength : 4657.9012
!Sequence no. 0003, Order no. 0091. Enter wavelength : 3895.4192
!!!!---------->(Problems with order 28 in obj_1_b)

!obj 1 blue March:
!Enter absolute order number of first pointed line (square mark) : 76
!Sequence no. 0001, Order no. 0077. Enter wavelength : 4598.7627
!Sequence no. 0003, Order no. 0091. Enter wavelength : 3895.4192

!obj 1 blue Sept:
!Enter absolute order number of first pointed line (square mark) : 77
!Sequence no. 0001, Order no. 0077. Enter wavelength : 4598.7627
!Sequence no. 0003, Order no. 0091. Enter wavelength : 3886.9159


!obj 1 red (2x2):
!Enter absolute order number of first pointed line (square mark) : 52
!Sequence no. 0001, Order no. 0052. Enter wavelength : 6538.1120
! OR Sequence no. 0001, Order no. 0049. Enter wavelength : 6937.6642
!Sequence no. 0003, Order no. 0066. Enter wavelength : 5158.6041
! OR Sequence no. 0003, Order no. 0066. Enter wavelength : 5343.5812
! OR Sequence no. 0003, Order no. 0063. Enter wavelength : 5499.2554

!52
!6538.1120
!5158.6041
!y

!UM425:
!77
!4598.7627
!3886.9159
!y


!77
!4598.7627
!3886.9159
!y


!! red:
!51
!6666.3588
!5650.7043


!51
!6666.3588
!5002.0581

