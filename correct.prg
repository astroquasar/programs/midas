!TEST
set/grap colour=1
set/grap xaxis=3310,3320 yaxis=0.9,1.05
compute/lyman test_param test_syn fitspec_dummy
plot/tab test_syn #1 #2


!print file to ascii
assign/prin FILE fitspec_dummy.ascii
sel/tab fitspec_dummy ALL
print/tab fitspec_dummy ? ? format_fitspec N

!read back file from ascii
crea/tab fitspec_dummy2 * * fitspec_dummy.ascii format_fitspec
compute/lyman test_param test_syn2 fitspec_dummy2
set/grap colour=2
overplot/tab test_syn2 #1 #2



