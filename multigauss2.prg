!
!  Procedure MULTIGAUSS2.PRG                      09.09.96
!                                               (c) SVCLM
!
!  Fit Gaussian(s) to displayed absorption profile.
!  Results are wrote to table "dataout"
!
DEFINE/PAR P1 logout TABLE "Enter Table with output data: "
DEFINE/PAR P2 SINGLE  TABLE "Enter Table with input data: "
DEFINE/PAR P3 N  C "Fix wave and witdth (Y/N) ?: "
!
WRITE/KEY NGAUSS/i/1/1  1   
WRITE/KEY tableout/c/1/40  {P1}
WRITE/KEY tablein/c/1/40  "none"
WRITE/KEY option/c/1/1  {P3}

!   
IF P2(1:6) .NE. "SINGLE" THEN	
   WRITE/KEY tablein/c/1/40  {P2}
   WRITE/KEY NGAUSS/i/1/1 {{P2}.tbl,TBLCONTR(4)}  
ENDIF
run /musica2/prg_linux/multigauss2

