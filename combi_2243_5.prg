!
! MIDAS procedure  COMBI_2243		 	                03.11.00 
!								(C)SVCLM
!
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter input table 3:"
DEFINE/PARAMETER P4 ?      C   "Enter input table 4 :"
DEFINE/PARAMETER P5 ?      C   "Enter input table 5 :"
DEFINE/PARAMETER P6 ?      C   "Enter output table:"
!
!                    Scale by median:
!
copy/tab {P1} {P6}
comp/tab {P6} :flux1 = :flux 
comp/tab {P6} :sigma1 = :sigma 
!
copy/tt {P2} :flux {P6} :flux2
copy/tt {P2} :sigma {P6} :sigma2
!
copy/tt {P3} :flux {P6} :flux3
copy/tt {P3} :sigma {P6} :sigma3
!
copy/tt {P4} :flux {P6} :flux4
copy/tt {P4} :sigma {P6} :sigma4
!
copy/tt {P5} :flux {P6} :flux5
copy/tt {P5} :sigma {P6} :sigma5

!
!                         Coadd flux and sky:
!
comp/tab {P6} :sigma = sqrt(1/(1/:sigma1**2+1/:sigma2**2+1/:sigma3**2+1/:sigma4**2+1/:sigma5**2))
comp/tab {P6} :flux = (:flux1/:sigma1**2+:flux2/:sigma2**2+:flux3/:sigma3**2+:flux4/:sigma4**2+:flux5/:sigma5**2)*:sigma**2


