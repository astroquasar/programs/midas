!
! MIDAS procedure  COMBI1_2		 	                03.11.00 
!								(C)SVCLM
! same as combi_2243 but for 2 spectra instead of 4
! Combine spectra of the same order. 
! Rectified flux is weighted with noise.
!
WRITE/OUT Procedure COMBI1_2, adding 2 exposures
DEFINE/PARAMETER P1 ?      C   "Enter input table 1:"
DEFINE/PARAMETER P2 ?      C   "Enter input table 2:"
DEFINE/PARAMETER P3 ?      C   "Enter output table:"
!
!         !!!!!!!!!!!  A:          
!
copy/tab {P1} {P3}
comp/tab {P3} :flux1 = :fluxA 
comp/tab {P3} :sigma1 = :sigmaA 
!
copy/tt {P2} :fluxA {P3} :flux2
copy/tt {P2} :sigmaA {P3} :sigma2
!
!                         Coadd flux and sky:
!
comp/tab {P3} :sigmaA = sqrt(1/(1/:sigma1**2+1/:sigma2**2))
comp/tab {P3} :fluxA = (:flux1/:sigma1**2+:flux2/:sigma2**2)*:sigmaA**2

!         !!!!!!!!!!!  B:
          
comp/tab {P3} :flux1 = :fluxB
comp/tab {P3} :sigma1 = :sigmaB 
!
copy/tt {P2} :fluxB {P3} :flux2
copy/tt {P2} :sigmaB {P3} :sigma2
!
!                         Coadd flux and sky:
!
comp/tab {P3} :sigmaB = sqrt(1/(1/:sigma1**2+1/:sigma2**2))
comp/tab {P3} :fluxB = (:flux1/:sigma1**2+:flux2/:sigma2**2)*:sigmaB**2
